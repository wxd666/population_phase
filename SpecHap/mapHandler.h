//
// Created by yangshuo on 5/7/2021.
//

#ifndef SPECHAP_MAPHANDLER_H
#define SPECHAP_MAPHANDLER_H

#include "vcfHandler.h"

class MapHandler : boost::noncopyable {

public:
    static std::vector<double> parse_map(std::vector<std::pair<int, int> > &chrBps,
                                         const std::string &geneticMapFile);

    MapHandler();

    ~MapHandler();
};

#endif //SPECHAP_MAPHANDLER_H
