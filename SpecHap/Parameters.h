//
// Created by yangshuo on 25/10/2021.
//

#ifndef SPECHAP_PARAMETERS_H
#define SPECHAP_PARAMETERS_H

#endif //SPECHAP_PARAMETERS_H

class Parameters {
private:
public:
    explicit Parameters();

    ~Parameters();

    std::map<int, std::map<std::string, int>> significance_map;
    std::map<std::string, int> countRateMap;
    int comp_pair_count;
    double reduce_weight_rate;
};
