//
// Created by yangshuo on 5/7/2021.
//

#ifndef SPECHAP_GENOBITS_H
#define SPECHAP_GENOBITS_H

#include "vcfHandler.h"
#include "memoryUtils.h"

class GenoBitsbuilder : boost::noncopyable {
private:
    uint64 Mseg64; // number of <=64-SNP chunks
    std::vector<std::vector<double>> seg64cMvecs;
    uint64_masks *genoBits;
    uint64_masks *haploBits;
    std::vector<std::vector<bool>> effectiveSNPSites;
    std::vector<int> blockPaddingCounts;

    void to_geno_bits(const std::vector<bool> &hapsRef, const std::vector<uchar> &genosTarget,
                      const std::vector<double> &cMs, double cMmax, uint64 M, uint64 Nref, uint64 Ntarget);

public:
    GenoBitsbuilder(const std::vector<bool> &hapsRef, const std::vector<uchar> &genosTarget,
                    const std::vector<double> &cMs, double cMmax, uint64 M, uint64 Nref, uint64 Ntarget);

    ~GenoBitsbuilder();


    uint64 getMseg64(void) const;

    const uint64_masks *getGenoBits(void) const;

    const uint64_masks *getHaploBits(void) const;

    std::vector<std::vector<double>> getSeg64cMvecs(void) const;

    std::vector<std::vector<bool>> getEffectiveSNPSites(void) const;

    std::vector<int> getBlockPaddingCount() const;

};

#endif //SPECHAP_GENOBITS_H
