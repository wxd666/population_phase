//
// Created by yangshuo on 5/7/2021.
//

#include <fstream>
#include "haploBitsArrays.h"

HaploBitsArrays::HaploBitsT::HaploBitsT() = default;

HaploBitsArrays::HaploBitsT::HaploBitsT(const uint64_masks *refBits, const uint64_masks *tarBits,
                                        const std::vector<std::vector<bool>> &effectiveSnpSites, uint64 nref,
                                        uint64 ntarget, uint64 m, uint64 mseg64,
                                        const std::vector<std::vector<double>> &seg64CMvecs,
                                        const std::vector<double> &cMs, const std::vector<int> &blockPaddingCounts) {
    this->ref_bits = refBits;
    this->tar_bits = tarBits;
    this->effectiveSNPSites = effectiveSnpSites;
    Nref = nref;
    Ntarget = ntarget;
    M = m;
    Mseg64 = mseg64;
    seg64cMvecs = seg64CMvecs;
    this->cms = cMs;
    this->block_padding_counts = blockPaddingCounts;
    cms64j.resize(Mseg64 * 64 + 1);
    double cmLast = 0;
    for (uint64 m64 = 0; m64 < Mseg64; m64++) {
        for (uint64 i = 0; i < seg64CMvecs[m64].size(); ++i) {
            cms64j[m64 * 64 + i] = cmLast = seg64CMvecs[m64][i];
        }
        for (uint64 i = seg64CMvecs[m64].size(); i < 64; ++i) {
            cms64j[m64 * 64 + i] = cmLast;
        }
    }
    cms64j[Mseg64 * 64] = cmLast;
}

HaploBitsArrays::HaploBitsT::~HaploBitsT() {
    delete ref_bits;
    delete tar_bits;
}

void HaploBitsArrays::CompressedHaploBitsT::setBit(uint64 n, uint64 m) {
    haploBitsT[n * NSite64 + (m >> 6)] |= 1ULL << (m & 63);
}

HaploBitsArrays::CompressedHaploBitsT::CompressedHaploBitsT(const uint64_masks *ref_bits, uint64 Mseg64, uint64 Nref,
                                                            const std::vector<uint64> &splits64j,
                                                            const std::vector<uchar> &splitGenos,
                                                            const std::vector<uint64_masks> &tgtGenoBits,
                                                            const std::vector<uint> &bestHaps,
                                                            const std::vector<double> &cMcoords) {
    this->Nref = Nref;
    bestK = bestHaps;
    NHaps = bestHaps.size();
    /// only preserve the het sites
    NSites = splits64j.size() + 2;
    NSite64 = (NSites + 63) / 64;
//    std::cout << "NSites = " << NSites << ", NSite64 = " << NSite64 << std::endl;
    haploBitsT = ALIGNED_MALLOC_UINT64S(NHaps * NSite64);
    memset(haploBitsT, 0, NHaps * NSite64 * sizeof(haploBitsT[0]));
    this->cMcoords = cMcoords;
    uint64 hapSeq;

//    std::ofstream bestK_compressed_hap("bestK_compressed_hap.txt", std::ios::out);
//    std::ofstream bestK_withHom_compressed_hap("bestK_withHom_compressed_hap.txt", std::ios::out);
//    bestK_withHom_compressed_hap.close();
    for (uint k = 0; k < bestHaps.size(); k++) {
        uint64 n = bestHaps[k];
//        bestK_compressed_hap << "0";
        /// in this turn, each vector in haploBitsTWithHom would have 1 padding 0s at the beginning and 2 padding 0s at the end
        haploBitsTWithHom[bestHaps[k]].resize(2 * (splits64j.size() + 2));
        for (uint64 h = 0; h <= splits64j.size(); h++) {
            // set hom
            uint64 homStart = (h == 0 ? 0 : splits64j[h - 1] + 1);
            uint64 homStop = (h == splits64j.size() ? Mseg64 * 64 : std::max(splits64j[h], 1ULL)) - 1;
            int numErrs = 0;
            for (uint64 m64 = (homStart >> 6); m64 <= (homStop >> 6); ++m64) {
                uint64 mask = -1ULL;
                if (m64 == (homStart >> 6))
                    mask &= (-1ULL >> (homStart & 63)) << (homStart & 63);
                if (m64 == (homStop >> 6))
                    mask &= (-1ULL >> (63 - (homStop & 63)));
                if (n & 1) {
                    /**
                     * splits64j[h] / 64 * Nref
                     * splits64j[h]/64 = Mseg64
                     */
                    hapSeq = ref_bits[m64 * Nref + n / 2].is2;
                } else {
                    hapSeq = ref_bits[m64 * Nref + n / 2].is0;
                }
                numErrs += popCount64(mask & ((tgtGenoBits[m64].is0 & hapSeq) |
                                              (tgtGenoBits[m64].is2 & ~hapSeq)));
                haploBitsTWithHom[bestHaps[k]][2 * h + 1] = numErrs;
            }
            // set bit at split
            if (h < splits64j.size()) {
                if (n & 1) {
                    /**
                     * splits64j[h] / 64 * Nref
                     * splits64j[h]/64 = Mseg64
                     */
                    hapSeq = ref_bits[(splits64j[h] >> 6) * Nref + n / 2].is2;
                } else {
                    hapSeq = ref_bits[(splits64j[h] >> 6) * Nref + n / 2].is0;
                }
                if ((hapSeq >> (splits64j[h] & 63)) & 1) {
                    setBit(k, h + 1);
                    haploBitsTWithHom[bestHaps[k]][2 * (h + 1)] = 1;
//                    bestK_compressed_hap << "1";
                } else {
//                    bestK_compressed_hap << "0";
                }
            }
        }
//        bestK_compressed_hap << "\nk = " << k << std::endl;
    }
    for (int i = 0; i < Nref * 2; ++i) {
        // todo: try to add a check for double-IBD here
        std::string curRefHap;
        std::string curRefHapWithHom;
        for (uint64 h = 0; h <= splits64j.size(); h++) {
            // set hom
            uint64 homStart = (h == 0 ? 0 : splits64j[h - 1] + 1);
            uint64 homStop = (h == splits64j.size() ? Mseg64 * 64 : std::max(splits64j[h], 1ULL)) - 1;
            int numErrs = 0;
            for (uint64 m64 = (homStart >> 6); m64 <= (homStop >> 6); ++m64) {
                uint64 mask = -1ULL;
                if (m64 == (homStart >> 6))
                    mask &= (-1ULL >> (homStart & 63)) << (homStart & 63);
                if (m64 == (homStop >> 6))
                    mask &= (-1ULL >> (63 - (homStop & 63)));
                if (i & 1) {
                    hapSeq = ref_bits[m64 * Nref + i / 2].is2;
                } else {
                    hapSeq = ref_bits[m64 * Nref + i / 2].is0;
                }
                numErrs += popcount64_01(mask & ((tgtGenoBits[m64].is0 & hapSeq) |
                                                 (tgtGenoBits[m64].is2 & ~hapSeq)));
                if (numErrs) {
                    break;
                }
            }
            if (numErrs) {
                curRefHapWithHom += '1';
            } else {
                curRefHapWithHom += '0';
            }
            // set bit at split
            if (h < splits64j.size()) {
                if (i & 1) {
                    /**
                     * splits64j[h] / 64 * Nref
                     * splits64j[h]/64 = Mseg64
                     */
                    hapSeq = ref_bits[(splits64j[h] >> 6) * Nref + i / 2].is2;
                } else {
                    hapSeq = ref_bits[(splits64j[h] >> 6) * Nref + i / 2].is0;
                }
                if ((hapSeq >> (splits64j[h] & 63)) & 1) {
                    curRefHap += '1';
                    curRefHapWithHom += '1';
                } else {
                    curRefHap += '0';
                    curRefHapWithHom += '0';
                }
            }
        }
        all_haps.emplace_back(curRefHap);
        all_haps_with_compressed_hom.emplace_back(curRefHapWithHom);
    }
//     todo: this loop is only for fixing the double-IBD, and if no use, remember to remove this loop
//    for (uint k = 0; k < bestHaps.size(); ++k) {
//        uint64 n = bestHaps[k];
//        std::string curRefHapWithHomA, curRefHapWithHomB;
//        uint64 hapSeqA, hapSeqB;
//        if (k + 1 < bestHaps.size() && bestHaps[k + 1] == n + 1) {
//            uint64 hLastDiff = 0;
//            uint64 hLastErrA = 0, hLastErrB = 0;
//            for (uint64 h = 0; h <= splits64j.size(); ++h) {
//                int numErrA = 0, numErrB = 0;
//                uint64 homStart = (h == 0 ? 0 : splits64j[h - 1] + 1);
//                uint64 homStop = (h == splits64j.size() ? Mseg64 * 64 : std::max(splits64j[h], 1ULL)) - 1;
//                for (uint64 m64 = (homStart >> 6); m64 < (homStop >> 6); ++m64) {
//                    uint64 mask = -1ULL;
//                    if (m64 == (homStart >> 6))
//                        mask &= (-1ULL >> (homStart & 63)) << (homStart & 63);
//                    if (m64 == (homStop >> 6))
//                        mask &= (-1ULL >> (63 - (homStop & 63)));
//                    hapSeqA = ref_bits[m64 * Nref + n / 2].is0;
//                    hapSeqB = ref_bits[m64 * Nref + n / 2].is2;
//                    numErrA += popcount64_01(mask & ((tgtGenoBits[m64].is0 & hapSeqA) |
//                                                     (tgtGenoBits[m64].is2 & ~hapSeqA)));
//                    numErrB += popcount64_01(mask & ((tgtGenoBits[m64].is0 & hapSeqB) |
//                                                     (tgtGenoBits[m64].is2 & ~hapSeqB)));
//                    if (numErrA && numErrB)
//                        break;
//                }
//                if (numErrA) {
//                    curRefHapWithHomA += '1';
//                } else {
//                    curRefHapWithHomA += '0';
//                }
//                if (numErrB) {
//                    curRefHapWithHomB += '1';
//                } else {
//                    curRefHapWithHomB += '0';
//                }
//                int splitA = 0, splitB = 0, splitGeno = 0;
//                if (h < splits64j.size()) {
//                    hapSeqA = ref_bits[(splits64j[h] >> 6) * Nref + n / 2].is0;
//                    hapSeqB = ref_bits[(splits64j[h] >> 6) * Nref + n / 2].is2;
//                    splitA = (hapSeqA >> (splits64j[h] & 63)) & 1;
//                    splitB = (hapSeqB >> (splits64j[h] & 63)) & 1;
//                    splitGeno = splitGenos[h];
//                    if (splitA) {
//                        curRefHapWithHomA += '1';
//                    } else {
//                        curRefHapWithHomA += '0';
//                    }
//                    if (splitB) {
//                        curRefHapWithHomB += '1';
//                    } else {
//                        curRefHapWithHomB += '0';
//                    }
//                }
//                /// check for double-IBD
//                if (numErrA || numErrB || splitA + splitB != splitGeno || h == splits64j.size()) {
//                    if (h > hLastDiff + 20) {
//                        for (uint h2 = hLastDiff + 1; h2 < h; ++h2) {
//                            curRefHapWithHomA.replace(2 * h2, 1, "1");
//                            curRefHapWithHomB.replace(2 * h2, 1, "1");
//                        }
//                    } else if (h > hLastDiff + 10) {
//                        for (uint h2 = hLastDiff + 1; h2 < h; h2++) {
//                            if (hLastErrA >= hLastErrB) {
//                                curRefHapWithHomA.replace(2 * h2, 1, "1");
//                            } else {
//                                curRefHapWithHomB.replace(2 * h2, 1, "1");
//                            }
//                        }
//                    }
//                    hLastDiff = h;
//                }
//                if (numErrA) hLastErrA = h;
//                if (numErrB) hLastErrB = h;
//            }
//            std::string tarA = all_haps_with_compressed_hom[n];
//            std::string tarB = all_haps_with_compressed_hom[n + 1];
//            std::replace(all_haps_with_compressed_hom.begin() + n, all_haps_with_compressed_hom.begin() + n + 1, tarA,
//                         curRefHapWithHomA);
//            std::replace(all_haps_with_compressed_hom.begin() + n + 1, all_haps_with_compressed_hom.begin() + n + 2,
//                         tarB, curRefHapWithHomB);
//            k++;
//        } else {
//            continue;
//        }
//    }
//    initAlleleIs0Array(1, NSites - 1);
}

int HaploBitsArrays::CompressedHaploBitsT::getBit(uint64 n, uint64 m) const {
    return haploBitsT[n * NSite64 + (m >> 6)] >> (m & 63) & 1;
}

uint64 HaploBitsArrays::CompressedHaploBitsT::getBits64(uint64 n, uint64 m64) const {
    return haploBitsT[n * NSite64 + m64];
}

int HaploBitsArrays::CompressedHaploBitsT::getNhaps() const {
    return NHaps;
}

int HaploBitsArrays::CompressedHaploBitsT::getM() const {
    return NSites;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByPBWT(uint start, uint end) const {
    int N = getNhaps();
    static double res[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    uint64 *curBits64 = new uint64[N];
    uchar *curBits8 = new uchar[N]; // N-byte buffer hopefully fits in L1 cache for random access
    SortDiv *ad = new SortDiv[N], *ad1 = new SortDiv[N]; // N+1 for convenience below
    std::set<int> start_set0;
    uint64 shift;
    for (int n = 0; n < N; n++) { // N+1 for convenience below
        ad[n].a = n;
        ad[n].d = start + 1;
    }

    for (int n = 0; n < N; n++) {
        curBits64[n] = this->getBits64(n, (start + 1) >> 6);
        curBits8[n] = (uchar) (curBits64[n] >> shift);
//        std::cout << "n ======= " << n << std::endl;
//        for (int i = 0; i < 2; ++i) {
//            std::cout << ";";
//            for (int j = 0; j < 64; ++j) {
//                if ((this->getBits64(n, i) >> j) & 1) {
//                    std::cout << "1";
//                } else {
//                    std::cout << "0";
//                }
//            }
//        }
//        std::cout << "\n";
    }
    for (int i = start + 1; i <= end + 1; ++i) {
        if ((i & 63) == 0 || i == 0) {
            for (int n = 0; n < N; n++)
                curBits64[n] = this->getBits64(n, i >> 6);
        }
        if (shift != ((i & 63) & ~7)) { // move current 8-bit m-block for each sample to curBits8
            shift = (i & 63) & ~7;
            for (int n = 0; n < N; n++)
                curBits8[n] = (uchar) (curBits64[n] >> shift);
        }
        uchar curBit8 = 1 << (i & 7);
        int u = 0, v = 0, p = i + 1, q = i + 1;
        for (int n = 0; n < N; n++) {
            int a_n = ad[n].a;
            int a_d = ad[n].d;
            if (a_d > p) {
                p = a_d;
            }
            if (a_d > q) {
                q = a_d;
            }
            if (curBits8[a_n] & curBit8) {
                ad1[v].a = a_n;
                ad1[v].d = q;
                if (i == end + 1) {
                    if (start_set0.count(ad1[v].a) > 0) {
                        res[1]++;
                    } else {
                        res[3]++;
                    }
                }
                v++;
            } else {
                ad[u].a = a_n;
                ad[u].d = p;
                if (i == end + 1) {
                    if (start_set0.count(ad[u].a) > 0) {
                        res[0]++;
                    } else {
                        res[2]++;
                    }
                }
                u++;
            }
        }
        if (i == start + 1) {
            for (int j = 0; j < u; ++j) {
                start_set0.insert(ad[j].a);
            }
            std::cout << "size ==== " << start_set0.size() << std::endl;
        }
        std::cout << "\n start = " << start << " end = " << end << " idx = " << i << " ppa = ";
        for (int j = 0; j < u; ++j) {
            std::cout << ad[j].a << ";";
        }
        std::cout << "\n start = " << start << " end = " << end << " idx = " << i << " ppa1 = ";
        for (int j = 0; j < v; ++j) {
            std::cout << ad1[j].a << ";";
        }
        memcpy(ad + u, ad1, v * sizeof(ad1[0]));
    }
    return res;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByRecombination(uint start, uint end, uint mapIdx0,
                                                                        uint mapIdx1) const {
    double cm = cms[mapIdx1] - cms[mapIdx0];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    int N = getNhaps();
    double nHaps, nSites, start_set0_size;
    double nRef = 2503;
    static double res[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    uint64 *curBits64 = new uint64[N];
    uchar *curBits8 = new uchar[N]; // N-byte buffer hopefully fits in L1 cache for random access
    SortDiv *ad = new SortDiv[N], *ad1 = new SortDiv[N]; // N+1 for convenience below
    std::set<int> start_set0;
    uint64 shift;
    for (int n = 0; n < N; n++) { // N+1 for convenience below
        ad[n].a = n;
        ad[n].d = start + 1;
    }

    for (int n = 0; n < N; n++) {
        curBits64[n] = this->getBits64(n, (start + 1) >> 6);
        curBits8[n] = (uchar) (curBits64[n] >> shift);
    }
    for (int i = start + 1; i <= end + 1; i += (end - start)) {
        for (int n = 0; n < N; n++)
            curBits64[n] = this->getBits64(n, i >> 6);
        shift = (i & 63) & ~7;
        for (int n = 0; n < N; n++)
            curBits8[n] = (uchar) (curBits64[n] >> shift);
        uchar curBit8 = 1 << (i & 7);
        int u = 0, v = 0, p = i + 1, q = i + 1;
        for (int n = 0; n < N; n++) {
            int a_n = ad[n].a;
            int a_d = ad[n].d;
            if (a_d > p) {
                p = a_d;
            }
            if (a_d > q) {
                q = a_d;
            }
            if (curBits8[a_n] & curBit8) {
                ad1[v].a = a_n;
                ad1[v].d = q;
                /// as calculating here, we only care about the recombination rate, but not the mutation rate.
                if (i == end + 1) {
                    if (start_set0.count(ad1[v].a) > 0) {
                        // 0 -> 1
                        res[1] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[3] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[0] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                    } else {
                        // 1 -> 1
                        res[3] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[1] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[2] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));

                    }
                }
                v++;
            } else {
                ad[u].a = a_n;
                ad[u].d = p;
                if (i == end + 1) {
                    if (start_set0.count(ad[u].a) > 0) {
                        // 0 -> 0
                        res[0] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[2] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[1] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                    } else {
                        // 1 -> 0
                        res[2] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[0] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        res[3] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                    }
                }
                u++;
            }
        }
        if (i == start + 1) {
            for (int j = 0; j < u; ++j) {
                start_set0.insert(ad[j].a);
            }
            nHaps = (double) NHaps;
//            nSites = (double) NSites;
//            start_set0_size = (double) start_set0.size();
//            std::cout << "size ==== " << start_set0.size() << std::endl;
        }
//        std::cout << "\n start = " << start << " end = " << end << " idx = " << i << " ppa = ";
//        for (int j = 0; j < u; ++j) {
//            std::cout << ad[j].a << ";";
//        }
//        std::cout << "\n start = " << start << " end = " << end << " idx = " << i << " ppa1 = ";
//        for (int j = 0; j < v; ++j) {
//            std::cout << ad1[j].a << ";";
//        }
        memcpy(ad + u, ad1, v * sizeof(ad1[0]));
    }
    double maxRes = 0;
    for (int i = 0; i < 4; ++i) {
        res[i] = 1 / res[i];
        if (maxRes < res[i] && isinf(res[i]) != 1) {
            maxRes = res[i];
        }
    }
    for (int i = 0; i < 4; ++i) {
        if (isinf(res[i]) == 1) {
            res[i] = maxRes + 0.1;
        }
        res[i] /= (end - start);
    }
//    for (int i = 0; i < 4; ++i) {
//        if (isnan(res[i]) == 1) {
//            std::cout << "edge weight is nan , start = " << start << " , end = " << end << ", mapIdx0 = " << mapIdx0
//                      << ", mapIdx1 = " << mapIdx1 << ", cm = " << cm << std::endl;
//        } else if (start > 3000) {
//            std::cout << "edge weight is : " << res[i] << ", start = " << start << ", end = " << end << ", mapIdx0 = "
//                      << mapIdx0 << ", mapIdx1 = " << mapIdx1 << ", cm = " << cm << std::endl;
//        }
//    }
    return res;
}

double *
HaploBitsArrays::CompressedHaploBitsT::getWeightByRecombinationV2(uint start, uint end, uint mapIdx0, uint mapIdx1) {
    double cm = cms[mapIdx1] - cms[mapIdx0];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    int N = getNhaps();
    double nHaps, nSites, start_set0_size;
    double nRef = 2503;
    static double res[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    for (int n = 0; n < N; ++n) {
        if (alleleIs0Array[n * snpCnt + end - blkStart]) {
            if (alleleIs0Array[n * snpCnt + start - blkStart]) {
                // 0 -> 0
                res[0] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                               (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                res[2] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                res[1] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
            } else {
                // 1 -> 0
                res[2] += -log(exp(-4.0 * nRef * cm / nHaps) +
                               (1 - exp(-4.0 * nRef * cm / nHaps)) * (1.0 / nHaps));
                res[0] += -log((1 - exp(-4.0 * nRef * cm / nHaps)) * (1.0 / nHaps));
                res[3] += -log((1 - exp(-4.0 * nRef * cm / nHaps)) * (1.0 / nHaps));
            }
        } else {
            if (alleleIs0Array[n * snpCnt + start - blkStart]) {
                // 0 -> 1
                res[1] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                               (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                res[3] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                res[0] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
            } else {
                // 1 -> 1
                res[3] += -log(exp(-4.0 * nHaps * cm / nHaps) +
                               (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                res[1] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                res[2] += -log((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
            }
        }
    }
    for (int i = 0; i < 4; ++i) {
        if (res[i] == 0) {
            continue;
        }
        res[i] = 1 / res[i];
    }
    return res;
}

void HaploBitsArrays::CompressedHaploBitsT::setCms(std::vector<double> cMs) {
    cms = std::move(cMs);
}

void HaploBitsArrays::CompressedHaploBitsT::initAlleleIs0Array(int blk_start, int blk_end) {
    int N = getNhaps();
    blkStart = blk_start;
    blkEnd = blk_end;
    snpCnt = blk_end - blk_start + 1;
    alleleIs0Array = new bool[N * snpCnt];
    uint64 *curBits64 = new uint64[N];
    uchar *curBits8 = new uchar[N];
    uint64 shift;
    for (int i = blk_start; i <= blk_end; ++i) {
        if ((i & 63) == 0 || i == 0) {
            for (int n = 0; n < N; n++)
                curBits64[n] = this->getBits64(n, i >> 6);
        }
        if (shift != ((i & 63) & ~7)) { // move current 8-bit m-block for each sample to curBits8
            shift = (i & 63) & ~7;
            for (int n = 0; n < N; n++)
                curBits8[n] = (uchar) (curBits64[n] >> shift);
        }
        uchar curBit8 = 1 << (i & 7);
        for (int n = 0; n < N; n++) {
            if (curBits8[n] & curBit8) {
                continue;
            } else {
                alleleIs0Array[n * snpCnt + i] = true;
            }
        }
    }
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByRecombinationV3(uint start, uint end, uint mapIdx0,
                                                                          uint mapIdx1) const {
    double cm = cms[mapIdx1] - cms[mapIdx0];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    int N = getNhaps();
    double nHaps, nSites, start_set0_size;
    double nRef = 2503;
    static double res[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    uint64 *curBits64 = new uint64[N];
    uchar *curBits8 = new uchar[N]; // N-byte buffer hopefully fits in L1 cache for random access
    SortDiv *ad = new SortDiv[N], *ad1 = new SortDiv[N]; // N+1 for convenience below
    std::set<int> start_set0;
    uint64 shift;
    for (int n = 0; n < N; n++) { // N+1 for convenience below
        ad[n].a = n;
        ad[n].d = start + 1;
    }

    for (int n = 0; n < N; n++) {
        curBits64[n] = this->getBits64(n, (start + 1) >> 6);
        curBits8[n] = (uchar) (curBits64[n] >> shift);
    }
    for (int i = start + 1; i <= end + 1; i += (end - start)) {
        for (int n = 0; n < N; n++)
            curBits64[n] = this->getBits64(n, i >> 6);
        shift = (i & 63) & ~7;
        for (int n = 0; n < N; n++)
            curBits8[n] = (uchar) (curBits64[n] >> shift);
        uchar curBit8 = 1 << (i & 7);
        int u = 0, v = 0, p = i + 1, q = i + 1;
        for (int n = 0; n < N; n++) {
            int a_n = ad[n].a;
            int a_d = ad[n].d;
            if (a_d > p) {
                p = a_d;
            }
            if (a_d > q) {
                q = a_d;
            }
            if (curBits8[a_n] & curBit8) {
                ad1[v].a = a_n;
                ad1[v].d = q;
                /// as calculating here, we only care about the recombination rate, but not the mutation rate.
                if (i == end + 1) {
                    auto *tmp = new double[4];
                    if (start_set0.count(ad1[v].a) > 0) {
                        // 0 -> 1
                        tmp[1] = log10(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[3] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[0] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[2] = 0;
                    } else {
                        // 1 -> 1
                        tmp[3] = log10(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[1] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[2] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[0] = 0;
                    }
                    res[0] += cal_score(tmp[0], tmp[3]);
                    res[3] += cal_score(tmp[0], tmp[3]);
                    res[1] += cal_score(tmp[1], tmp[2]);
                    res[2] += cal_score(tmp[1], tmp[2]);
                }
                v++;
            } else {
                ad[u].a = a_n;
                ad[u].d = p;
                if (i == end + 1) {
                    auto *tmp = new double[4];
                    if (start_set0.count(ad[u].a) > 0) {
                        // 0 -> 0
                        tmp[0] = log10(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[2] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[1] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[3] = 0;
                    } else {
                        // 1 -> 0
                        tmp[2] = log10(exp(-4.0 * nHaps * cm / nHaps) +
                                       (1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[0] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[3] = log10((1 - exp(-4.0 * nHaps * cm / nHaps)) * (1.0 / nHaps));
                        tmp[1] = 0;
                    }
                    res[0] += cal_score(tmp[0], tmp[3]);
                    res[3] += cal_score(tmp[0], tmp[3]);
                    res[1] += cal_score(tmp[1], tmp[2]);
                    res[2] += cal_score(tmp[1], tmp[2]);
                }
                u++;
            }
        }
        if (i == start + 1) {
            for (int j = 0; j < u; ++j) {
                start_set0.insert(ad[j].a);
            }
            nHaps = (double) NHaps;
            nSites = (double) NSites;
            start_set0_size = (double) start_set0.size();
        }
        memcpy(ad + u, ad1, v * sizeof(ad1[0]));
    }
//    for (int i = 0; i < 4; ++i) {
//        if (isnan(res[i]) == 1) {
//            std::cout << "edge weight is nan , start = " << start << " , end = " << end << ", mapIdx0 = " << mapIdx0
//                      << ", mapIdx1 = " << mapIdx1 << ", cm = " << cm << std::endl;
//        } else if (cm >= 2.5) {
//            std::cout << "edge weight is : " << res[i] << ", start = " << start << ", end = " << end << ", mapIdx0 = "
//                      << mapIdx0 << ", mapIdx1 = " << mapIdx1 << ", cm = " << cm << std::endl;
//        }
//    }
    return res;
}

const std::vector<uint> &HaploBitsArrays::CompressedHaploBitsT::getBestK() const {
    return bestK;
}

const std::vector<bool> &HaploBitsArrays::CompressedHaploBitsT::getHapsRef() const {
    return hapsRef;
}

void HaploBitsArrays::CompressedHaploBitsT::setHapsRef(const std::vector<bool> &hapsRef) {
    CompressedHaploBitsT::hapsRef = hapsRef;
}

double *
HaploBitsArrays::CompressedHaploBitsT::getWeightByBlocks(const Block &block1, const Block &block2) {
    /**
     * 1, separating the haplotypes by het dif counts
     * 2, loading the haplotypes from 0 dif, 1 dif and increasing
     */
    auto *res = new double[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    double cm = cms[block2.snp_idx.front()] - cms[block1.snp_idx.back()];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    double nHaps = (double) NHaps;
//        double Ne = 100000;
//        double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
    std::set<int> start_set0;
    /// het dif count map
    std::map<int, std::vector<std::pair<double, int>>> error_rate_rank_h1_map;
    std::map<int, std::vector<std::pair<double, int>>> error_rate_rank_h2_map;
    std::map<int, std::vector<std::pair<double, int>>> complementary_rate_rank_h1_map;
    std::map<int, std::vector<std::pair<double, int>>> complementary_rate_rank_h2_map;
    // recombinationProbs: the first value in pair indicates copy, while the second value in pair indicates recombination
    std::vector<std::vector<std::pair<double, double>>> recombinationProbs;
    recombinationProbs.resize(bestK.size());
    /// topK means the filtered k haplotypes which would be used to construct the weight edges
    int topK = bestK.size() <= 10 ? bestK.size() : 2 * bestK.size() / 100;
    /// a scaling score for different het site
    int het_dif_scale = 100;
//    std::ofstream file;
//    std::ofstream file1;
//    std::ofstream file2;
    std::ofstream file3;
//    std::ofstream file4;
//    file.open("flow_v7.txt", std::ios::app);
//    file1.open("flow_v7_weight.txt", std::ios::app);
//    file2.open("flow_v7_error_rate_rank.txt", std::ios::app);
    file3.open("flow_v7_error.log", std::ios::app);
//    file4.open("flow_v7_res.txt", std::ios::app);
    double alpha = 10.0;
    for (int n = 0; n < bestK.size(); ++n) {
        recombinationProbs[n].resize(4);
        /// calculating the Similarity with hamming distance
        int block1_dif_snp_count_0 = 0, block1_dif_snp_count_1 = 0, block2_dif_snp_count_0 = 0, block2_dif_snp_count_1 = 0;
        int block1_total_count = 0, block2_total_count = 0;
        int block1_dif_het_count_0 = 0, block1_dif_het_count_1 = 0, block2_dif_het_count_0 = 0, block2_dif_het_count_1 = 0;
        /// get hamming distance for block1 -> haps
        block1_total_count = block1.snp_idx.back() - block1.snp_idx.front() + 1;
        block2_total_count = block2.snp_idx.back() - block2.snp_idx.front() + 1;
        //todo: mark here, cause we didn't take the hom snp sites which are not adjacent to the phased het sites into consideration
        for (int i = 0; i < block1.phased_het_idx.size(); ++i) {
            int het_id = block1.phased_het_idx[i];
            block1_dif_snp_count_0 += (block1.pseudo_hap1[i] != this->getBit(n, block1.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            block1_dif_snp_count_1 += (block1.pseudo_hap2[i] != this->getBit(n, block1.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            if ((block1.pseudo_hap1[i] != this->getBit(n, block1.het_idx[het_id] + 1)) ||
                (block1.pseudo_hap2[i] != this->getBit(n, block1.het_idx[het_id] + 1))) {
                block1_total_count += het_dif_scale - 1;
            }
            block1_dif_het_count_0 += (block1.pseudo_hap1[i] != this->getBit(n, block1.het_idx[het_id] + 1)) ? 1 : 0;
            block1_dif_het_count_1 += (block1.pseudo_hap2[i] != this->getBit(n, block1.het_idx[het_id] + 1)) ? 1 : 0;
            if (het_id == 0) {
                continue;
            }
            block1_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[het_id] + 1];
            block1_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[het_id] + 1];
        }
        /// get hamming distance for block2 -> haps
        for (int i = 0; i < block2.phased_het_idx.size(); i++) {
            int het_id = block2.phased_het_idx[i];
            block2_dif_snp_count_0 += (block2.pseudo_hap1[i] != this->getBit(n, block2.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            block2_dif_snp_count_1 += (block2.pseudo_hap2[i] != this->getBit(n, block2.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            if ((block2.pseudo_hap1[i] != this->getBit(n, block2.het_idx[het_id] + 1)) ||
                (block2.pseudo_hap2[i] != this->getBit(n, block2.het_idx[het_id] + 1))) {
                block2_total_count += het_dif_scale - 1;
            }
            block2_dif_het_count_0 += (block2.pseudo_hap1[i] != this->getBit(n, block2.het_idx[het_id] + 1)) ? 1 : 0;
            block2_dif_het_count_1 += (block2.pseudo_hap2[i] != this->getBit(n, block2.het_idx[het_id] + 1)) ? 1 : 0;
            if (het_id == 0) {
                continue;
            }
            block2_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[het_id] + 1];
            block2_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[het_id] + 1];
        }
        /// calculating the similarity of B_1_1 with B_2_1, B_1_1 with B_2_2
        /// find the min error rate of the two link mode, and set these two error rate as distance
        double P_H_B1_0_B2_0 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_0_B2_1 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_0 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_1 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));

        /// calculating the recombination probability by the last snp of block1 and the first snp of block2

        //todo: here we should think about how to change the calculation method from end point to one-one point links
        int block1_het_id = block1.phased_het_idx.back();
        int block2_het_id = block2.phased_het_idx.back();
        int bit1 = this->getBit(n, block1.het_idx[block1_het_id] + 1);
        int bit2 = this->getBit(n, block2.het_idx[block2_het_id] + 1);

        if (bit1 == block1.pseudo_hap1.back()) {
            start_set0.insert(n);
            if (bit2 == block2.pseudo_hap1.front()) {
                recombinationProbs[n][0].first += (1 - ro_m);
                recombinationProbs[n][0].second += ro_m;
                recombinationProbs[n][2].second += ro_m;
            } else {
                recombinationProbs[n][1].first += (1 - ro_m);
                recombinationProbs[n][1].second += ro_m;
                recombinationProbs[n][3].second += ro_m;
            }
        } else {
            if (bit2 == block2.pseudo_hap1.front()) {
                recombinationProbs[n][2].first += (1 - ro_m);
                recombinationProbs[n][2].second += ro_m;
                recombinationProbs[n][0].second += ro_m;
            } else {
                recombinationProbs[n][3].first += (1 - ro_m);
                recombinationProbs[n][3].second += ro_m;
                recombinationProbs[n][1].second += ro_m;
            }
        }
        /// count of different snp sites in block1 combined with block2 compared to ref hap, and error_rate_rank would store the rate of this count with total count
        int blk1_hap1_back = block1.pseudo_hap1.back();
        int blk2_hap1_front = block2.pseudo_hap1.front();
        if (blk1_hap1_back == bit1) {
            if (blk2_hap1_front == bit2) {
                /// block1 hap0 -> block2 hap0
                error_rate_rank_h1_map[block1_dif_het_count_0 + block2_dif_het_count_0].emplace_back(P_H_B1_0_B2_0, n);
            } else {
                /// block1 hap0 -> block2 hap1
                error_rate_rank_h2_map[block1_dif_het_count_0 + block2_dif_het_count_1].emplace_back(P_H_B1_0_B2_1, n);
            }
        } else {
            if (blk2_hap1_front == bit2) {
                /// block1 hap1 -> block2 hap0
                complementary_rate_rank_h2_map[block1_dif_het_count_1 + block2_dif_het_count_0].emplace_back(
                        P_H_B1_1_B2_0, n);
            } else {
                /// block1 hap1 -> block2 hap1
                complementary_rate_rank_h1_map[block1_dif_het_count_1 + block2_dif_het_count_1].emplace_back(
                        P_H_B1_1_B2_1, n);
            }
        }
    }
    unsigned long size = start_set0.size();
    auto size_d = (double) size;
    bool comP_added = false;
    /// find the min different error list
    if (error_rate_rank_h1_map.empty() && error_rate_rank_h2_map.empty()) {
        return res;
    }
    if (complementary_rate_rank_h1_map.empty() && complementary_rate_rank_h2_map.empty()) {
        return res;
    }
    if ((error_rate_rank_h1_map.empty() && complementary_rate_rank_h2_map.empty()) ||
        (error_rate_rank_h2_map.empty() && complementary_rate_rank_h1_map.empty())) {
        return res;
    }
    std::vector<std::pair<double, int>> error_rate_rank_h1_vector;
    std::vector<std::pair<double, int>> error_rate_rank_h2_vector;
    std::vector<std::pair<double, int>> complementary_rate_rank_h1_vector;
    std::vector<std::pair<double, int>> complementary_rate_rank_h2_vector;
    if (!error_rate_rank_h1_map.empty()) {
        error_rate_rank_h1_vector = error_rate_rank_h1_map.begin()->second;
        std::sort(error_rate_rank_h1_vector.begin(), error_rate_rank_h1_vector.end());
    }
    if (!error_rate_rank_h2_map.empty()) {
        error_rate_rank_h2_vector = error_rate_rank_h2_map.begin()->second;
        std::sort(error_rate_rank_h2_vector.begin(), error_rate_rank_h2_vector.end());
    }
    if (!complementary_rate_rank_h1_map.empty()) {
        complementary_rate_rank_h1_vector = complementary_rate_rank_h1_map.begin()->second;
        std::sort(complementary_rate_rank_h1_vector.begin(), complementary_rate_rank_h1_vector.end());
    }
    if (!complementary_rate_rank_h2_map.empty()) {
        complementary_rate_rank_h2_vector = complementary_rate_rank_h2_map.begin()->second;
        std::sort(complementary_rate_rank_h2_vector.begin(), complementary_rate_rank_h2_vector.end());
    }
//    std::cout << "err_rate_rank_vectors" << error_rate_rank_h1_vector.size() << "\t" << error_rate_rank_h2_vector.size()
//              << "\t"
//              << complementary_rate_rank_h1_vector.size() << "\t" << complementary_rate_rank_h2_vector.size()
//              << std::endl;
    if (error_rate_rank_h1_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap1 -> hap1, the least dif count is " << error_rate_rank_h1_map.begin()->first
              << std::endl;
    }
    if (error_rate_rank_h2_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap1 -> hap2, the least dif count is " << error_rate_rank_h2_map.begin()->first
              << std::endl;
    }
    if (complementary_rate_rank_h1_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap2 -> hap2, the least dif count is "
              << complementary_rate_rank_h1_map.begin()->first << std::endl;
    }
    if (complementary_rate_rank_h2_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap2 -> hap1, the least dif count is "
              << complementary_rate_rank_h2_map.begin()->first << std::endl;
    }
    for (int i = 0; i < topK; ++i) {
        if (i < error_rate_rank_h1_vector.size()) {
            int n = error_rate_rank_h1_vector[i].second;
            /// hap1->hap1 copy
            res[0] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][0].first / size_d);
            /// hap1->hap1 recombination provided by hap1 -> hap1 copy
            res[0] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][0].second / nHaps);
            /// hap2 -> hap1 recombination provided by hap1 -> hap1 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][2].second / nHaps);
        }
        if (i < error_rate_rank_h2_vector.size()) {
            int n = error_rate_rank_h2_vector[i].second;
            /// hap1 -> hap2 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][1].first / size_d);
            /// hap1 -> hap2 recombination provided by hap1 -> hap2 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][1].second / nHaps);
            /// hap2-> hap2 recombination provided by hap1 -> hap2;
            res[0] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][3].second / nHaps);
        }
        if (i < complementary_rate_rank_h1_vector.size()) {
            int n = complementary_rate_rank_h1_vector[i].second;
            /// hap2 -> hap2 copy
            res[0] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][3].first /
                      (nHaps - size_d);
            /// hap2 -> hap2 recombination
            res[0] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][3].second /
                      nHaps;
            /// hap1 -> hap2 recombination provided by hap2 -> hap2
            res[1] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][1].second /
                      nHaps;
        }
        if (i < complementary_rate_rank_h2_vector.size()) {
            int n = complementary_rate_rank_h2_vector[i].second;
            /// hap2 -> hap1 copy
            res[1] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][2].first /
                      (nHaps - size_d);
            /// hap2 -> hap1 recombination
            res[1] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][2].second /
                      nHaps;
            /// hap1 -> hap1 recombination provided by hap2 -> hap1
            res[0] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][0].second /
                      nHaps;
        }
    }
//    file2 << "block1 = " << block1.blk_idx << ", block2 = " << block2.blk_idx << std::endl;
//    file2 << "error_rate_rank_h1_vector" << std::endl;
//    for (const auto &item : error_rate_rank_h1_vector) {
//        file2 << item.first << ";" << item.second << std::endl;
//    }
//    file2 << "error_rate_rank_h2_vector" << std::endl;
//    for (const auto &item : error_rate_rank_h2_vector) {
//        file2 << item.first << ";" << item.second << std::endl;
//    }
//    file2 << "complementary_rate_rank_h1_vector" << std::endl;
//    for (const auto &item : complementary_rate_rank_h1_vector) {
//        file2 << item.first << ";" << item.second << std::endl;
//    }
//    file2 << "complementary_rate_rank_h2_vector" << std::endl;
//    for (const auto &item : complementary_rate_rank_h2_vector) {
//        file2 << item.first << ";" << item.second << std::endl;
//    }
//    file4 << "block1 = " << block1.blk_idx << ", block2 = " << block2.blk_idx << ", res = " << res[0] << ";" << res[1]
//          << ";" << res[2] << ";" << res[3] << std::endl;
//    file.close();
//    file1.close();
//    file2.close();
    file3.close();
//    file4.close();
    return res;
}

void HaploBitsArrays::CompressedHaploBitsT::setM(uint64 m) {
    M = m;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByRecombinationV4(uint start, uint end, uint mapIdx0,
                                                                          uint mapIdx1) const {
    double cm = cms[mapIdx1] - cms[mapIdx0];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    int N = getNhaps();
    double nHaps = (double) NHaps;
    double Ne = 100000;
    double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double mu_rate = 0;
    double thelta = 0;
    for (int i = 1; i < N; ++i) {
        thelta += 1.0 / i;
    }
    thelta = 1 / thelta;
    mu_rate = thelta / (N + thelta);
    static double res[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    uint64 *curBits64 = new uint64[N];
    uchar *curBits8 = new uchar[N]; // N-byte buffer hopefully fits in L1 cache for random access
    SortDiv *ad = new SortDiv[N], *ad1 = new SortDiv[N]; // N+1 for convenience below
    std::set<int> start_set0;
    std::vector<int> node_edge_count;
    /// 0: 0-0, 1: 0-1, 2: 1-0, 3: 1-1, 4: 0-null, 5: 1-null, 6: null-0, 7: null-1
    node_edge_count.resize(8);
    uint64 shift;
    for (int n = 0; n < N; n++) { // N+1 for convenience below
        ad[n].a = n;
        ad[n].d = start + 1;
    }

    for (int n = 0; n < N; n++) {
        curBits64[n] = this->getBits64(n, (start + 1) >> 6);
        curBits8[n] = (uchar) (curBits64[n] >> shift);
    }
    for (int i = start + 1; i <= end + 1; i += (end - start)) {
        for (int n = 0; n < N; n++)
            curBits64[n] = this->getBits64(n, i >> 6);
        shift = (i & 63) & ~7;
        for (int n = 0; n < N; n++)
            curBits8[n] = (uchar) (curBits64[n] >> shift);
        uchar curBit8 = 1 << (i & 7);
        int u = 0, v = 0, p = i + 1, q = i + 1;
        for (int n = 0; n < N; n++) {
            int a_n = ad[n].a;
            int a_d = ad[n].d;
            if (a_d > p) {
                p = a_d;
            }
            if (a_d > q) {
                q = a_d;
            }
            if (curBits8[a_n] & curBit8) {
                ad1[v].a = a_n;
                ad1[v].d = q;
                /// as calculating here, we only care about the recombination rate, but not the mutation rate.
                if (i == end + 1) {
                    node_edge_count[7] += 1;
                    if (start_set0.count(ad1[v].a) > 0) {
                        // 0 -> 1
                        node_edge_count[1]++;
                    } else {
                        // 1 -> 1
                        node_edge_count[3]++;
                    }
                }
                v++;
            } else {
                ad[u].a = a_n;
                ad[u].d = p;
                if (i == end + 1) {
                    node_edge_count[6] += 1;
                    if (start_set0.count(ad[u].a) > 0) {
                        // 0 -> 0
                        node_edge_count[0]++;
                    } else {
                        // 1 -> 0
                        node_edge_count[2]++;
                    }
                }
                u++;
            }
        }
        if (i == start + 1) {
            for (int j = 0; j < u; ++j) {
                start_set0.insert(ad[j].a);
            }
            node_edge_count[4] = start_set0.size();
            node_edge_count[5] = NHaps - start_set0.size();
        }
        memcpy(ad + u, ad1, v * sizeof(ad1[0]));
    }
    if (node_edge_count[4] == 0) {
        res[0] = (ro_m * node_edge_count[6] / nHaps);
        res[1] = (ro_m * node_edge_count[7] / nHaps);
    } else {
        res[0] = (1 - ro_m) * (1.0 * node_edge_count[0] / node_edge_count[4]) + (ro_m * node_edge_count[6] / nHaps);
        res[1] = (1 - ro_m) * (1.0 * node_edge_count[1] / node_edge_count[4]) + (ro_m * node_edge_count[7] / nHaps);
    }
    if (node_edge_count[5] == 0) {
        res[2] = (ro_m * node_edge_count[6] / nHaps);
        res[3] = (ro_m * node_edge_count[7] / nHaps);
    } else {
        res[2] = (1 - ro_m) * (1.0 * node_edge_count[2] / node_edge_count[5]) + (ro_m * node_edge_count[6] / nHaps);
        res[3] = (1 - ro_m) * (1.0 * node_edge_count[3] / node_edge_count[5]) + (ro_m * node_edge_count[7] / nHaps);
    }
//    if ((res[0] + res[3]) == (res[1] + res[2])) {
//        std::cout << "edge weight equals , start = " << start << " , end = " << end << ", cm = " << cm
//                  << ", ro_m = " << ro_m << ", weights = " << res[0] << ";" << res[1] << ";" << res[2] << ";"
//                  << res[3] << std::endl;
//        std::cout << "node_edge_counts = " << node_edge_count[0] << ";" << node_edge_count[1] << ";"
//                  << node_edge_count[2] << ";" << node_edge_count[3] << ";" << node_edge_count[4] << ";"
//                  << node_edge_count[5] << ";" << node_edge_count[6] << ";" << node_edge_count[7] << std::endl;
//        res[0] = res[1] = res[2] = res[3] = 0;
//    }
    return res;
}

void HaploBitsArrays::CompressedHaploBitsT::testGetWeight() {
    int n = 0;
    std::cout << std::endl;
    for (int i = 0; i < NSites; ++i) {
        uint64 bits64 = this->getBits64(n, i >> 6);
        int shift = (i & 63) & ~7;
        uint64 curBits8 = bits64 >> shift;
        uchar curBit8 = 1 << (i & 7);
        if (curBits8 & curBit8) {
            std::cout << 1;
        } else {
            std::cout << 0;
        }
//        int bit = this->getBit(n, i);
//        std::cout << bit;
    }
    std::cout << std::endl;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByRecombinationV5(uint start, uint end, uint mapIdx0,
                                                                          uint mapIdx1) const {
    /**
     * the assumed haplotypes are :
     * res[0] -> 0-0
     * res[1] -> 0-1
     * res[2] -> 1-0
     * res[3] -> 1-1
     * 1-ro_m means the copy without recombination mode, and ro_m means the recombination occurs
     * In this turn, the reference panel haplotypes are seemed as separated reads
     * so now, considering the mutation rate on each site
     */
    double cm = cms[mapIdx1] - cms[mapIdx0];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    int N = getNhaps();
    double nHaps = (double) NHaps;
//    double Ne = 100000;
//    double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
//    int cmExpect = 2;
//    if (cmExpect > 0) {
//        double a = cmExpect;
//        double term1 = 1 / sq(1 + (cMcoords[end + 1] - cMcoords[start + 1]) / a);
//        double term2 = (end + 2) == (int) cMcoords.size() ? 0 : 1 / sq(1 + (cMcoords[end + 2] - cMcoords[start + 1]) / a);
//        ro_m = term1 - term2;
//    } else {
//        double a = -cmExpect;
//        double term1 = exp(-(cMcoords[end + 1] - cMcoords[start + 1]) / a);
//        double term2 = (end + 2) == (int) cMcoords.size() ? 0 : exp(-(cMcoords[end + 2] - cMcoords[start + 1]) / a);
//        ro_m = term1 - term2;
//    }
//    const double minRecombP = 0.000001, maxRecombP = 1.0;
//    ro_m = std::max(std::min(ro_m, maxRecombP), minRecombP);
    double mu_rate = 0;
    double thelta = 0;
    for (int i = 1; i < N; ++i) {
        thelta += 1.0 / i;
    }
    thelta = 1 / thelta;
    mu_rate = thelta / (N + thelta);
    static double res[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    if ((end - start) >= 100) {
        return res;
    }
    uint64 *curBits64 = new uint64[N];
    uchar *curBits8 = new uchar[N]; // N-byte buffer hopefully fits in L1 cache for random access
    SortDiv *ad = new SortDiv[N], *ad1 = new SortDiv[N]; // N+1 for convenience below
    std::set<int> start_set0;
    std::vector<int> node_edge_count;
    /// 0: 0-0, 1: 0-1, 2: 1-0, 3: 1-1, 4: 0-null, 5: 1-null, 6: null-0, 7: null-1
    node_edge_count.resize(8);
    uint64 shift;
    for (int n = 0; n < N; n++) { // N+1 for convenience below
        ad[n].a = n;
        ad[n].d = start + 1;
    }

    for (int n = 0; n < N; n++) {
        curBits64[n] = this->getBits64(n, (start + 1) >> 6);
        curBits8[n] = (uchar) (curBits64[n] >> shift);
    }
    for (int i = start + 1; i <= end + 1; i += (end - start)) {
        for (int n = 0; n < N; n++)
            curBits64[n] = this->getBits64(n, i >> 6);
        shift = (i & 63) & ~7;
        for (int n = 0; n < N; n++)
            curBits8[n] = (uchar) (curBits64[n] >> shift);
        uchar curBit8 = 1 << (i & 7);
        int u = 0, v = 0, p = i + 1, q = i + 1;
        for (int n = 0; n < N; n++) {
            int a_n = ad[n].a;
            int a_d = ad[n].d;
            if (a_d > p) {
                p = a_d;
            }
            if (a_d > q) {
                q = a_d;
            }
            if (curBits8[a_n] & curBit8) {
                ad1[v].a = a_n;
                ad1[v].d = q;
                /// as calculating here, we only care about the recombination rate, but not the mutation rate.
                if (i == end + 1) {
                    node_edge_count[7] += 1;
                    if (start_set0.count(ad1[v].a) > 0) {
                        // read is 0 -> 1
                        /// in this turn, it seems like hap 01/10 copy from cur read with 0 or 2 mutations, or recombined from cur read with 1 mutation
                        if (node_edge_count[4] == 0) {
//                            res[1] += (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[1] += (ro_m * (mu_rate - mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[1] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[4]) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
//                            res[1] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[1] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[4]) +
                                      (ro_m * (mu_rate - mu_rate * mu_rate)) / nHaps;
                        }
                        if (node_edge_count[5] == 0) {
//                            res[3] += (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[3] += (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[3] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / node_edge_count[5]) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
//                            res[3] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[3] += ((1 - ro_m) * (mu_rate - mu_rate * mu_rate) / node_edge_count[5]) +
                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                        }
                    } else {
                        // 1 -> 1
                        if (node_edge_count[5] == 0) {
//                            res[3] += (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[3] += (ro_m * (mu_rate - mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[3] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[5]) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
//                            res[3] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[3] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[5]) +
                                      (ro_m * (mu_rate - mu_rate * mu_rate)) / nHaps;
                        }
                        if (node_edge_count[4] == 0) {
//                            res[1] += (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[1] += (ro_m * (1 - 2 * mu_rate + mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[1] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / node_edge_count[4]) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
//                            res[1] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[1] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / node_edge_count[4]) +
                                      (ro_m * (1 - 2 * mu_rate + mu_rate * mu_rate)) / nHaps;
                        }
                    }
                }
                v++;
            } else {
                ad[u].a = a_n;
                ad[u].d = p;
                if (i == end + 1) {
                    node_edge_count[6] += 1;
                    if (start_set0.count(ad[u].a) > 0) {
                        // 0 -> 0
                        if (node_edge_count[4] == 0) {
//                            res[0] += (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[0] += (ro_m * (mu_rate - mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[0] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[4]) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
//                            res[0] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[0] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[4]) +
                                      (ro_m * (mu_rate - mu_rate * mu_rate)) / nHaps;
                        }
                        if (node_edge_count[5] == 0) {
//                            res[2] += (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[2] += (ro_m * (1 - 2 * mu_rate + mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[2] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / node_edge_count[5]) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
//                            res[2] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[2] += (ro_m * (1 - 2 * mu_rate + mu_rate * mu_rate)) / nHaps;
                        }
                    } else {
                        // 1 -> 0
                        if (node_edge_count[4] == 0) {
//                            res[2] += (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[2] += 0;
                        } else {
//                            res[2] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / node_edge_count[4]) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
//                            res[2] += ((1 - ro_m) * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (2 * mu_rate - 2 * mu_rate * mu_rate)) / nHaps;
                            res[2] += ((1 - ro_m) * (1 - 2 * mu_rate + mu_rate * mu_rate) / node_edge_count[4]);
                        }
                        if (node_edge_count[5] == 0) {
//                            res[0] += (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[0] += (ro_m * (1 - 2 * mu_rate + mu_rate * mu_rate)) / nHaps;
                        } else {
//                            res[0] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / node_edge_count[5]) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
//                            res[0] += ((1 - ro_m) * (2 * mu_rate - 2 * mu_rate * mu_rate) / nHaps) +
//                                      (ro_m * (1 - 2 * mu_rate + 2 * mu_rate * mu_rate)) / nHaps;
                            res[0] += ((1 - ro_m) * (mu_rate - mu_rate * mu_rate) / node_edge_count[5]) +
                                      (ro_m * (1 - 2 * mu_rate + mu_rate * mu_rate)) / nHaps;
                        }
                    }
                }
                u++;
            }
        }
        if (i == start + 1) {
            for (int j = 0; j < u; ++j) {
                start_set0.insert(ad[j].a);
            }
            node_edge_count[4] = start_set0.size();
            node_edge_count[5] = NHaps - start_set0.size();
            nHaps = (double) NHaps;
        }
        memcpy(ad + u, ad1, v * sizeof(ad1[0]));
    }
//    for (int i = 0; i < 4; ++i) {
//        if (ro_m >= 1.0) {
//            std::cout << "ro_m >= 1 , start = " << start << " , end = " << end << ", cm = " << cm
//                      << ", ro_m = " << ro_m << ", weights = " << res[0] << ";" << res[1] << ";" << res[2] << ";"
//                      << res[3] << std::endl;
//            std::cout << "node_edge_counts = " << node_edge_count[0] << ";" << node_edge_count[1] << ";"
//                      << node_edge_count[2] << ";" << node_edge_count[3] << ";" << node_edge_count[4] << ";"
//                      << node_edge_count[5] << ";" << node_edge_count[6] << ";" << node_edge_count[7] << std::endl;
//        }
//    }
    if ((res[0] + res[3]) == (res[1] + res[2])) {
//        std::cout << "edge weight equals , start = " << start << " , end = " << end << ", cm = " << cm
//                  << ", ro_m = " << ro_m << ", weights = " << res[0] << ";" << res[1] << ";" << res[2] << ";"
//                  << res[3] << std::endl;
//        std::cout << "node_edge_counts = " << node_edge_count[0] << ";" << node_edge_count[1] << ";"
//                  << node_edge_count[2] << ";" << node_edge_count[3] << ";" << node_edge_count[4] << ";"
//                  << node_edge_count[5] << ";" << node_edge_count[6] << ";" << node_edge_count[7] << std::endl;
        res[0] = res[1] = res[2] = res[3] = 0;
    }
    if ((res[0] + res[3]) == 0.5 && (res[1] + res[2]) == 0.5) {
//        std::cout << "edge weight sum = 0.5 , start = " << start << " , end = " << end << ", cm = " << cm
//                  << ", ro_m = " << ro_m << ", weights = " << res[0] << ";" << res[1] << ";" << res[2] << ";"
//                  << res[3] << std::endl;
//        std::cout << "node_edge_counts = " << node_edge_count[0] << ";" << node_edge_count[1] << ";"
//                  << node_edge_count[2] << ";" << node_edge_count[3] << ";" << node_edge_count[4] << ";"
//                  << node_edge_count[5] << ";" << node_edge_count[6] << ";" << node_edge_count[7] << std::endl;
//        std::cout << "res[03] == res[12] : " << ((res[0] + res[3]) == (res[1] + res[2])) << std::endl;
        res[0] = res[1] = res[2] = res[3] = 0;
//        std::cout << res[0] << res[1] << res[2] << res[3] << std::endl;
    }
//    for (int i = 0; i < 4; ++i) {
//        std::cout << "edge weight log, start = " << start << ", end = " << end << ", weights = " << res[0] << ";"
//                  << res[1] << ";" << res[2] << ";" << res[3] << std::endl;
//        std::cout << "ro_m = " << ro_m << ", cm = " << cm << std::endl;
//        break;
//    }
//    else {
//        for (double &re : res) {
//            re /= (end - start);
//        }
//    }
    return res;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByBlocksV2(const Block &block1, const Block &block2) {
    /**
     * 1, separating the haplotypes by het dif counts
     * 2, loading the haplotypes from 0 dif, 1 dif and increasing
     */
    auto *res = new double[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    double cm = cms[block2.snp_idx.front()] - cms[block1.snp_idx.back()];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    double nHaps = (double) NHaps;
//        double Ne = 100000;
//        double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
    std::set<int> start_set0;
    /// het dif count map
    std::map<int, std::vector<std::pair<double, int>>> error_rate_rank_h1_map;
    std::map<int, std::vector<std::pair<double, int>>> error_rate_rank_h2_map;
    std::map<int, std::vector<std::pair<double, int>>> complementary_rate_rank_h1_map;
    std::map<int, std::vector<std::pair<double, int>>> complementary_rate_rank_h2_map;
    /// recombinationProbs: the first value in pair indicates copy, while the second value in pair indicates recombination
    std::vector<std::vector<std::pair<double, double>>> recombinationProbs;
    recombinationProbs.resize(bestK.size());
    /// topK means the filtered k haplotypes which would be used to construct the weight edges
    int topK = bestK.size() <= 10 ? bestK.size() : 2 * bestK.size() / 5;
    /// a scaling score for different het site
    int het_dif_scale = 100;
    /// het dif count threshold for linking blocks
    int accepted_err_vector_count = 8;
//    std::ofstream file;
//    std::ofstream file1;
//    std::ofstream file2;
    std::ofstream file3;
//    std::ofstream file4;
//    file.open("flow_v7.txt", std::ios::app);
//    file1.open("flow_v7_weight.txt", std::ios::app);
//    file2.open("flow_v7_error_rate_rank.txt", std::ios::app);
    file3.open("flow_v7_error.log", std::ios::app);
//    file4.open("flow_v7_res.txt", std::ios::app);
    double alpha = 10.0;
    for (int n = 0; n < bestK.size(); ++n) {
        recombinationProbs[n].resize(4);
        /// calculating the Similarity with hamming distance
        int block1_dif_snp_count_0 = 0, block1_dif_snp_count_1 = 0, block2_dif_snp_count_0 = 0, block2_dif_snp_count_1 = 0;
        int block1_total_count = 0, block2_total_count = 0;
        int block1_dif_het_count_0 = 0, block1_dif_het_count_1 = 0, block2_dif_het_count_0 = 0, block2_dif_het_count_1 = 0;
        /// get hamming distance for block1 -> haps
        block1_total_count = block1.snp_idx.back() - block1.snp_idx.front() + 1;
        block2_total_count = block2.snp_idx.back() - block2.snp_idx.front() + 1;
        //todo: mark here, cause we didn't take the hom snp sites which are not adjacent to the phased het sites into consideration
        for (int i = 0; i < block1.phased_het_idx.size(); ++i) {
            int het_id = block1.phased_het_idx[i];
            block1_dif_snp_count_0 += (block1.pseudo_hap1[i] != this->getBit(n, block1.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            block1_dif_snp_count_1 += (block1.pseudo_hap2[i] != this->getBit(n, block1.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            if ((block1.pseudo_hap1[i] != this->getBit(n, block1.het_idx[het_id] + 1)) ||
                (block1.pseudo_hap2[i] != this->getBit(n, block1.het_idx[het_id] + 1))) {
                block1_total_count += het_dif_scale - 1;
            }
            block1_dif_het_count_0 += (block1.pseudo_hap1[i] != this->getBit(n, block1.het_idx[het_id] + 1)) ? 1 : 0;
            block1_dif_het_count_1 += (block1.pseudo_hap2[i] != this->getBit(n, block1.het_idx[het_id] + 1)) ? 1 : 0;
            if (het_id == 0) {
                continue;
            }
            block1_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[het_id] + 1];
            block1_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[het_id] + 1];
        }
        /// get hamming distance for block2 -> haps
        for (int i = 0; i < block2.phased_het_idx.size(); i++) {
            int het_id = block2.phased_het_idx[i];
            block2_dif_snp_count_0 += (block2.pseudo_hap1[i] != this->getBit(n, block2.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            block2_dif_snp_count_1 += (block2.pseudo_hap2[i] != this->getBit(n, block2.het_idx[het_id] + 1))
                                      ? het_dif_scale : 0;
            if ((block2.pseudo_hap1[i] != this->getBit(n, block2.het_idx[het_id] + 1)) ||
                (block2.pseudo_hap2[i] != this->getBit(n, block2.het_idx[het_id] + 1))) {
                block2_total_count += het_dif_scale - 1;
            }
            block2_dif_het_count_0 += (block2.pseudo_hap1[i] != this->getBit(n, block2.het_idx[het_id] + 1)) ? 1 : 0;
            block2_dif_het_count_1 += (block2.pseudo_hap2[i] != this->getBit(n, block2.het_idx[het_id] + 1)) ? 1 : 0;
            if (het_id == 0) {
                continue;
            }
            block2_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[het_id] + 1];
            block2_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[het_id] + 1];
        }
        /// calculating the similarity of B_1_1 with B_2_1, B_1_1 with B_2_2
        /// find the min error rate of the two link mode, and set these two error rate as distance
        double P_H_B1_0_B2_0 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_0_B2_1 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_0 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_1 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));

        /// calculating the recombination probability by the last snp of block1 and the first snp of block2
        //todo: here we should think about how to change the calculation method from end point to one-one point links
        int block1_het_id = block1.phased_het_idx.back();
        int block2_het_id = block2.phased_het_idx.back();
        int bit1 = this->getBit(n, block1.het_idx[block1_het_id] + 1);
        int bit2 = this->getBit(n, block2.het_idx[block2_het_id] + 1);
        int blk1_hap1_back = block1.pseudo_hap1.back();
        int blk2_hap1_front = block2.pseudo_hap1.front();

        /// count of different snp sites in block1 combined with block2 compared to ref hap, and error_rate_rank would store the rate of this count with total count
        if (block1_dif_het_count_0 < block1_dif_het_count_1) {
            start_set0.insert(n);
            if (block2_dif_het_count_0 < block2_dif_het_count_1) {
                /// block1 hap0 -> block2 hap0
                recombinationProbs[n][0].first += (1 - ro_m);
                recombinationProbs[n][0].second += ro_m;
                recombinationProbs[n][2].second += ro_m;
                error_rate_rank_h1_map[block1_dif_het_count_0 + block2_dif_het_count_0].emplace_back(P_H_B1_0_B2_0, n);
            } else {
                /// block1 hap0 -> block2 hap1
                recombinationProbs[n][1].first += (1 - ro_m);
                recombinationProbs[n][1].second += ro_m;
                recombinationProbs[n][3].second += ro_m;
                error_rate_rank_h2_map[block1_dif_het_count_0 + block2_dif_het_count_1].emplace_back(P_H_B1_0_B2_1, n);
            }
        } else {
            if (block2_dif_het_count_0 < block2_dif_het_count_1) {
                /// block1 hap1 -> block2 hap0
                recombinationProbs[n][2].first += (1 - ro_m);
                recombinationProbs[n][2].second += ro_m;
                recombinationProbs[n][0].second += ro_m;
                complementary_rate_rank_h2_map[block1_dif_het_count_1 + block2_dif_het_count_0].emplace_back(
                        P_H_B1_1_B2_0, n);
            } else {
                /// block1 hap1 -> block2 hap1
                recombinationProbs[n][3].first += (1 - ro_m);
                recombinationProbs[n][3].second += ro_m;
                recombinationProbs[n][1].second += ro_m;
                complementary_rate_rank_h1_map[block1_dif_het_count_1 + block2_dif_het_count_1].emplace_back(
                        P_H_B1_1_B2_1, n);
            }
        }
    }
    unsigned long size = start_set0.size();
    auto size_d = (double) size;
    bool comP_added = false;
    /// find the min different error list
    if (error_rate_rank_h1_map.empty() && error_rate_rank_h2_map.empty()) {
//        std::cout << "error , h1map = " << error_rate_rank_h1_map.size() << ", h2map = " << error_rate_rank_h2_map.size() << std::endl;
        return res;
    }
    if (complementary_rate_rank_h1_map.empty() && complementary_rate_rank_h2_map.empty()) {
//        std::cout << "error , comph1map = " << complementary_rate_rank_h1_map.size() << ", comph2map = " << complementary_rate_rank_h2_map.size() << std::endl;
        return res;
    }
    if ((error_rate_rank_h1_map.empty() && complementary_rate_rank_h2_map.empty()) ||
        (error_rate_rank_h2_map.empty() && complementary_rate_rank_h1_map.empty())) {
//        std::cout << "error , h1map = " << error_rate_rank_h1_map.size() << ", h2map = " << error_rate_rank_h2_map.size() << ", comph1map = " << complementary_rate_rank_h1_map.size() << ", comph2map = " << complementary_rate_rank_h2_map.size() << std::endl;
        return res;
    }
//    std::cout << "block id = " << block1.blk_idx << "\t" << block2.blk_idx << ", block size = " << block1.phased_het_idx.size() << std::endl;
//    std::cout << "err rate rank h1 dif count = " << std::endl;
    for (auto i = error_rate_rank_h1_map.begin(); i != error_rate_rank_h1_map.end(); ++i) {
//        std::cout << i->first << "\t";
    }
//    std::cout << std::endl;
//    std::cout << "err rate rank h2 dif count = " << std::endl;
    for (auto i = error_rate_rank_h2_map.begin(); i != error_rate_rank_h2_map.end(); ++i) {
//        std::cout << i->first << "\t";
    }
//    std::cout << std::endl;
//    std::cout << "comp h1 dif count = " << std::endl;
    for (auto i = complementary_rate_rank_h1_map.begin(); i != complementary_rate_rank_h1_map.end(); ++i) {
//        std::cout << i->first << "\t";
    }
//    std::cout << std::endl;
//    std::cout << "comp h2 dif count = " << std::endl;
    for (auto i = complementary_rate_rank_h2_map.begin(); i != complementary_rate_rank_h2_map.end(); ++i) {
//        std::cout << i->first << "\t";
    }
//    std::cout << std::endl;
    std::vector<std::pair<double, int>> error_rate_rank_h1_vector;
    std::vector<std::pair<double, int>> error_rate_rank_h2_vector;
    std::vector<std::pair<double, int>> complementary_rate_rank_h1_vector;
    std::vector<std::pair<double, int>> complementary_rate_rank_h2_vector;
    int execute_count = 0;
    if (!error_rate_rank_h1_map.empty()) {
        for (auto i = error_rate_rank_h1_map.begin(); i != error_rate_rank_h1_map.end(); ++i) {
            execute_count++;
            if (execute_count > accepted_err_vector_count) {
                break;
            }
            error_rate_rank_h1_vector.insert(error_rate_rank_h1_vector.end(), i->second.begin(), i->second.end());
        }
        std::sort(error_rate_rank_h1_vector.begin(), error_rate_rank_h1_vector.end());
    }
    if (!error_rate_rank_h2_map.empty()) {
        execute_count = 0;
        for (auto i = error_rate_rank_h2_map.begin(); i != error_rate_rank_h2_map.end(); ++i) {
            execute_count++;
            if (execute_count > accepted_err_vector_count) {
                break;
            }
            error_rate_rank_h2_vector.insert(error_rate_rank_h2_vector.end(), i->second.begin(), i->second.end());
        }
        std::sort(error_rate_rank_h2_vector.begin(), error_rate_rank_h2_vector.end());
    }
    if (!complementary_rate_rank_h1_map.empty()) {
        execute_count = 0;
        for (auto i = complementary_rate_rank_h1_map.begin(); i != complementary_rate_rank_h1_map.end(); ++i) {
            execute_count++;
            if (execute_count > accepted_err_vector_count) {
                break;
            }
            complementary_rate_rank_h1_vector.insert(complementary_rate_rank_h1_vector.end(), i->second.begin(),
                                                     i->second.end());
        }
        std::sort(complementary_rate_rank_h1_vector.begin(), complementary_rate_rank_h1_vector.end());
    }
    if (!complementary_rate_rank_h2_map.empty()) {
        execute_count = 0;
        for (auto i = complementary_rate_rank_h2_map.begin(); i != complementary_rate_rank_h2_map.end(); ++i) {
            execute_count++;
            if (execute_count > accepted_err_vector_count) {
                break;
            }
            complementary_rate_rank_h2_vector.insert(complementary_rate_rank_h2_vector.end(), i->second.begin(),
                                                     i->second.end());
        }
        std::sort(complementary_rate_rank_h2_vector.begin(), complementary_rate_rank_h2_vector.end());
    }
//    std::cout << "err_rate_rank_vectors size = " << error_rate_rank_h1_vector.size() << "\t"
//              << error_rate_rank_h2_vector.size()
//              << "\t"
//              << complementary_rate_rank_h1_vector.size() << "\t" << complementary_rate_rank_h2_vector.size()
//              << std::endl;
    if (error_rate_rank_h1_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap1 -> hap1, the least dif count is " << error_rate_rank_h1_map.begin()->first
              << std::endl;
    }
    if (error_rate_rank_h2_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap1 -> hap2, the least dif count is " << error_rate_rank_h2_map.begin()->first
              << std::endl;
    }
    if (complementary_rate_rank_h1_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap2 -> hap2, the least dif count is "
              << complementary_rate_rank_h1_map.begin()->first << std::endl;
    }
    if (complementary_rate_rank_h2_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap2 -> hap1, the least dif count is "
              << complementary_rate_rank_h2_map.begin()->first << std::endl;
    }
//    int used_ref = 0;
//    while (used_ref < topK) {
//
//    }
    for (int i = 0; i < topK; ++i) {
        if (i < error_rate_rank_h1_vector.size()) {
            int n = error_rate_rank_h1_vector[i].second;
            /// hap1->hap1 copy
            res[0] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][0].first / size_d);
            /// hap1->hap1 recombination provided by hap1 -> hap1 copy
            res[0] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][0].second / nHaps);
            /// hap2 -> hap1 recombination provided by hap1 -> hap1 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][2].second / nHaps);
        }
        if (i < error_rate_rank_h2_vector.size()) {
            int n = error_rate_rank_h2_vector[i].second;
            /// hap1 -> hap2 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][1].first / size_d);
            /// hap1 -> hap2 recombination provided by hap1 -> hap2 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][1].second / nHaps);
            /// hap2-> hap2 recombination provided by hap1 -> hap2;
            res[0] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][3].second / nHaps);
        }
        if (i < complementary_rate_rank_h1_vector.size()) {
            int n = complementary_rate_rank_h1_vector[i].second;
            /// hap2 -> hap2 copy
            res[0] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][3].first /
                      (nHaps - size_d);
            /// hap2 -> hap2 recombination
            res[0] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][3].second /
                      nHaps;
            /// hap1 -> hap2 recombination provided by hap2 -> hap2
            res[1] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][1].second /
                      nHaps;
        }
        if (i < complementary_rate_rank_h2_vector.size()) {
            int n = complementary_rate_rank_h2_vector[i].second;
            /// hap2 -> hap1 copy
            res[1] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][2].first /
                      (nHaps - size_d);
            /// hap2 -> hap1 recombination
            res[1] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][2].second /
                      nHaps;
            /// hap1 -> hap1 recombination provided by hap2 -> hap1
            res[0] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][0].second /
                      nHaps;
        }
    }
    file3.close();
    return res;
}

/**
 * step1, calculating the Similarity for the two blocks with topK ref-haps
 * step2, calculating the copy/recombination probability
 * @param block1
 * @param block2
 * @return
 */
double *HaploBitsArrays::CompressedHaploBitsT::getWeightByBlocksV3(const Block &block1, const Block &block2) {
    /**
     * 1, marking the haps in block1 into the reference panels
     */
    auto *res = new double[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    double cm = cms[block2.snp_idx.front()] - cms[block1.snp_idx.back()];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    double nHaps = (double) NHaps;
//        double Ne = 100000;
//        double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
    std::set<int> start_set0;
    // recombinationProbs: the first value in pair indicates copy, while the second value in pair indicates recombination
    std::vector<std::pair<double, double>> recombinationProbs;
    recombinationProbs.resize(4);
    std::ofstream file;
    file.open("flow_v3.txt", std::ios::app);
    for (int n = 0; n < bestK.size(); ++n) {
        /// calculating the Similarity with hamming distance
        int block1_dif_snp_count_0 = 0, block1_dif_snp_count_1 = 0, block2_dif_snp_count_0 = 0, block2_dif_snp_count_1 = 0;
        int block1_total_count = 0, block2_total_count = 0;
        /// get hamming distance for block1 -> haps
//        block1_total_count = block1.snp_idx.size();
//        block2_total_count = block2.snp_idx.size();
        block1_total_count = block1.snp_idx.back() - block1.snp_idx.front() + 1;
        block2_total_count = block2.snp_idx.back() - block2.snp_idx.front() + 1;
        for (int i = 0; i < block1.het_idx.size(); i++) {
            block1_dif_snp_count_0 += (block1.hap1[i] != this->getBit(n, block1.het_idx[i] + 1)) ? 1 : 0;
            block1_dif_snp_count_1 += (block1.hap2[i] != this->getBit(n, block1.het_idx[i] + 1)) ? 1 : 0;
            if (i == 0) {
                continue;
            }
            block1_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
            block1_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
        }
        /// get hamming distance for block2 -> haps
        for (int i = 0; i < block2.het_idx.size(); i++) {
            block2_dif_snp_count_0 += (block2.hap1[i] != this->getBit(n, block2.het_idx[i] + 1)) ? 1 : 0;
            block2_dif_snp_count_1 += (block2.hap2[i] != this->getBit(n, block2.het_idx[i] + 1)) ? 1 : 0;
            if (i == 0) {
                continue;
            }
            block2_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
            block2_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
        }
        /// calculating the probabilities of B_1_1 with B_2_1, B_1_1 with B_2_2
        double P_H_B1_0_B2_0 = 1 - (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_0) /
                                    (block1_total_count + block2_total_count));
        double P_H_B1_0_B2_1 = 1 - (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_1) /
                                    (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_0 = 1 - (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_0) /
                                    (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_1 = 1 - (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_1) /
                                    (block1_total_count + block2_total_count));
        double P_H1 = std::max(P_H_B1_0_B2_0, P_H_B1_1_B2_1);
        double P_H2 = std::max(P_H_B1_0_B2_1, P_H_B1_1_B2_0);

        /// calculating the recombination probability by the last snp of block1 and the first snp of block2

        int bit1 = this->getBit(n, block1.het_idx.back() + 1);
        int bit2 = this->getBit(n, block2.het_idx.front() + 1);
        if (bit1 == block1.hap1.back()) {
            start_set0.insert(n);
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[0].first += exp(P_H1) * (1 - ro_m);
                recombinationProbs[0].second += exp(P_H1) * ro_m;
                recombinationProbs[2].second += exp(P_H1) * ro_m;
            } else {
                recombinationProbs[1].first += exp(P_H2) * (1 - ro_m);
                recombinationProbs[1].second += exp(P_H2) * ro_m;
                recombinationProbs[3].second += exp(P_H2) * ro_m;
            }
        } else {
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[2].first += exp(P_H2) * (1 - ro_m);
                recombinationProbs[2].second += exp(P_H2) * ro_m;
                recombinationProbs[0].second += exp(P_H2) * ro_m;
            } else {
                recombinationProbs[3].first += exp(P_H1) * (1 - ro_m);
                recombinationProbs[3].second += exp(P_H1) * ro_m;
                recombinationProbs[1].second += exp(P_H1) * ro_m;
            }
        }
        file << "similarity log" << std::endl;
        file << "block1_dif_count_0 = " << block1_dif_snp_count_0 << "\t block1_dif_count_1 = "
             << block1_dif_snp_count_1 << "\t block2_dif_count_0 = " << block2_dif_snp_count_0
             << "\t block2_dif_count_1 = "
             << block2_dif_snp_count_1 <<
             "\t block1_total_count = " << block1_total_count << "\t block2_total_count = " << block2_total_count
             << std::endl;
        file << "P_B1_0_B2_0 = " << P_H_B1_0_B2_0 << "\t P_H_B1_0_B2_1 = " << P_H_B1_0_B2_1 << "\t P_H_B1_1_B2_0 = "
             << P_H_B1_1_B2_0 << "\t P_H_B1_1_B2_1 = " << P_H_B1_1_B2_1 << "\t P_H1 = "
             << P_H1 << "\t P_H2 = " << P_H2 << std::endl;
//        file << "P_H1 = " << P_H1 << ", P_H2 = " << P_H2 << std::endl;
        file << "exp value of P_H1 = " << exp(P_H1) << ", P_H2 = " << exp(P_H2) << std::endl;
        file << "recombinationProbs in n loops" << std::endl;
        file << "cm = " << cm << ", ro_m = " << ro_m << std::endl;
        for (int j = 0; j < 4; ++j) {
            file << "recombinationProbs[" << j << "] = " << recombinationProbs[j].first << "\t"
                 << recombinationProbs[j].second << std::endl;
        }
    }
    unsigned long size = start_set0.size();
    auto size_d = (double) size;
    if (size_d == 0) {
        res[0] += recombinationProbs[0].second / nHaps + recombinationProbs[3].first / (nHaps - size_d) +
                  recombinationProbs[3].second / nHaps;
        res[1] += recombinationProbs[1].second / nHaps + recombinationProbs[2].first / (nHaps - size_d) +
                  recombinationProbs[2].second / nHaps;
    } else if (size_d == nHaps) {
        res[0] += (recombinationProbs[0].first / size_d) + (recombinationProbs[0].second / nHaps) +
                  (recombinationProbs[3].second / nHaps);
        res[1] += (recombinationProbs[1].first / size_d) + (recombinationProbs[1].second / nHaps) +
                  (recombinationProbs[2].second / nHaps);
    } else {
        res[0] += (recombinationProbs[0].first / size_d) + (recombinationProbs[0].second / nHaps) +
                  (recombinationProbs[3].first / (nHaps - size_d)) + (recombinationProbs[3].second / nHaps);
        res[1] += (recombinationProbs[1].first / size_d) + (recombinationProbs[1].second / nHaps) +
                  (recombinationProbs[2].first / (nHaps - size_d)) + (recombinationProbs[2].second / nHaps);
    }
//    file << "edge weight log , start = " << block1.blk_idx << " , end = " << block2.blk_idx
//         << ", weights = " << res[0] << ";" << res[1] << ";" << res[2] << ";" << res[3] << std::endl;
    for (int i = 0; i < 4; ++i) {
        file << "edge weight logging , start = " << block1.blk_idx << " , end = " << block2.blk_idx
             << ", weights = " << res[0] << ";" << res[1] << ", size_d = " << size_d << ", nHaps = " << nHaps
             << std::endl;
//        for (int j = 0; j < 4; ++j) {
//            file << "recombinationProbs[" << j << "] = " << recombinationProbs[j].first << "\t"
//                 << recombinationProbs[j].second << std::endl;
//        }
        break;
    }
    file.close();
    return res;
}

/**
 * step1, calculating the Similarity for the two blocks with topK ref-haps
 * step2, calculating the copy/recombination probability
 * @param block1
 * @param block2
 * @return
 */
double *HaploBitsArrays::CompressedHaploBitsT::getWeightByBlocksV4(const Block &block1, const Block &block2) {
    /**
     * 1, marking the haps in block1 into the reference panels
     */
    auto *res = new double[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    double cm = cms[block2.snp_idx.front()] - cms[block1.snp_idx.back()];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    double nHaps = (double) NHaps;
//        double Ne = 100000;
//        double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
    std::set<int> start_set0;
    // error rate rank list
    std::vector<std::pair<double, int>> error_rate_rank_h1;
    std::vector<std::pair<double, int>> error_rate_rank_h2;
    error_rate_rank_h1.resize(bestK.size());
    error_rate_rank_h2.resize(bestK.size());
    // recombinationProbs: the first value in pair indicates copy, while the second value in pair indicates recombination
    std::vector<std::vector<std::pair<double, double>>> recombinationProbs;
    recombinationProbs.resize(bestK.size());
    /// topK means the filtered k haplotypes which would be used to construct the weight edges
    int topK = bestK.size() <= 10 ? bestK.size() : 4 * bestK.size() / 10;
    std::ofstream file;
    std::ofstream file1;
    std::ofstream file2;
    file.open("flow_v4.txt", std::ios::app);
    file1.open("flow_v4_weight.txt", std::ios::app);
    file2.open("flow_v4_error_rate_rank.txt", std::ios::app);
    double alpha = 10.0;
    for (int n = 0; n < bestK.size(); ++n) {
        recombinationProbs[n].resize(4);
        /// calculating the Similarity with hamming distance
        int block1_dif_snp_count_0 = 0, block1_dif_snp_count_1 = 0, block2_dif_snp_count_0 = 0, block2_dif_snp_count_1 = 0;
        int block1_total_count = 0, block2_total_count = 0;
        /// get hamming distance for block1 -> haps
//        block1_total_count = block1.snp_idx.size();
//        block2_total_count = block2.snp_idx.size();
        block1_total_count = block1.snp_idx.back() - block1.snp_idx.front() + 1;
        block2_total_count = block2.snp_idx.back() - block2.snp_idx.front() + 1;
        for (int i = 0; i < block1.het_idx.size(); i++) {
            block1_dif_snp_count_0 += (block1.hap1[i] != this->getBit(n, block1.het_idx[i] + 1)) ? 1 : 0;
            block1_dif_snp_count_1 += (block1.hap2[i] != this->getBit(n, block1.het_idx[i] + 1)) ? 1 : 0;
            if (i == 0) {
                continue;
            }
            block1_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
            block1_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
        }
        /// get hamming distance for block2 -> haps
        for (int i = 0; i < block2.het_idx.size(); i++) {
            block2_dif_snp_count_0 += (block2.hap1[i] != this->getBit(n, block2.het_idx[i] + 1)) ? 1 : 0;
            block2_dif_snp_count_1 += (block2.hap2[i] != this->getBit(n, block2.het_idx[i] + 1)) ? 1 : 0;
            if (i == 0) {
                continue;
            }
            block2_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
            block2_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
        }
        /// calculating the similarity of B_1_1 with B_2_1, B_1_1 with B_2_2
        /// find the min error rate of the two link mode, and set these two error rate as distance
        double P_H_B1_0_B2_0 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_0_B2_1 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_0 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_1 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        /// here we get the minimal value of the percent for different snp sites in the whole hap
        double P_H1 = std::min(P_H_B1_0_B2_0, P_H_B1_1_B2_1);
        double P_H2 = std::min(P_H_B1_0_B2_1, P_H_B1_1_B2_0);
        error_rate_rank_h1[n].first = P_H1;
        error_rate_rank_h1[n].second = n;
        error_rate_rank_h2[n].first = P_H2;
        error_rate_rank_h2[n].second = n;

        /// calculating the recombination probability by the last snp of block1 and the first snp of block2

        int bit1 = this->getBit(n, block1.het_idx.back() + 1);
        int bit2 = this->getBit(n, block2.het_idx.front() + 1);

        if (bit1 == block1.hap1.back()) {
            start_set0.insert(n);
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[n][0].first += (1 - ro_m);
                recombinationProbs[n][0].second += ro_m;
                recombinationProbs[n][2].second += ro_m;
            } else {
                recombinationProbs[n][1].first += (1 - ro_m);
                recombinationProbs[n][1].second += ro_m;
                recombinationProbs[n][3].second += ro_m;
            }
        } else {
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[n][2].first += (1 - ro_m);
                recombinationProbs[n][2].second += ro_m;
                recombinationProbs[n][0].second += ro_m;
            } else {
                recombinationProbs[n][3].first += (1 - ro_m);
                recombinationProbs[n][3].second += ro_m;
                recombinationProbs[n][1].second += ro_m;
            }
        }
        //todo: we might should add a constraint for topK before this storing operation
        /// count of different snp sites in block1 combined with block2 compared to ref hap, and error_rate_rank would store the rate of this count with total count
//        int blk1_hap1_back = block1.hap1.back();
//        int blk1_hap2_back = block1.hap2.back();
//        int blk2_hap1_front = block2.hap1.front();
//        int blk2_hap2_front = block2.hap2.front();
//        if (blk1_hap1_back == bit1) {
//            if (blk2_hap1_front == bit2) {
//                error_rate_rank_h1[n].first = P_H1;
//                error_rate_rank_h1[n].second = n;
//                error_rate_rank_h2[n].first = 1;
//                error_rate_rank_h2[n].second = n;
//            } else {
//                error_rate_rank_h1[n].first = 1;
//                error_rate_rank_h1[n].second = n;
//                error_rate_rank_h2[n].first = P_H2;
//                error_rate_rank_h2[n].second = n;
//            }
//        } else {
//            if (blk2_hap2_front == bit2) {
//                error_rate_rank_h1[n].first = P_H1;
//                error_rate_rank_h1[n].second = n;
//                error_rate_rank_h2[n].first = 1;
//                error_rate_rank_h2[n].second = n;
//            } else {
//                error_rate_rank_h1[n].first = 1;
//                error_rate_rank_h1[n].second = n;
//                error_rate_rank_h2[n].first = P_H2;
//                error_rate_rank_h2[n].second = n;
//            }
//        }
        file << "similarity log bestK id = " << n << std::endl;
        file << "block1_dif_count_0 = " << block1_dif_snp_count_0 << "\t block1_dif_count_1 = "
             << block1_dif_snp_count_1 << "\t block2_dif_count_0 = " << block2_dif_snp_count_0
             << "\t block2_dif_count_1 = "
             << block2_dif_snp_count_1 <<
             "\t block1_total_count = " << block1_total_count << "\t block2_total_count = " << block2_total_count
             << std::endl;
        file << "P_B1_0_B2_0 = " << P_H_B1_0_B2_0 << "\t P_H_B1_0_B2_1 = " << P_H_B1_0_B2_1 << "\t P_H_B1_1_B2_0 = "
             << P_H_B1_1_B2_0 << "\t P_H_B1_1_B2_1 = " << P_H_B1_1_B2_1 << "\t P_H1 = "
             << P_H1 << "\t P_H2 = " << P_H2 << std::endl;
//        file << "P_H1 = " << P_H1 << ", P_H2 = " << P_H2 << std::endl;
        file << "1/exp(ax) value of P_H1 = " << 1 / exp(alpha * P_H1) << ", P_H2 = " << 1 / exp(alpha * P_H2)
             << std::endl;
        file << "recombinationProbs in n loops" << std::endl;
        file << "cm = " << cm << ", ro_m = " << ro_m << std::endl;
        for (int j = 0; j < 4; ++j) {
            file << "recombinationProbs[" << j << "] = " << recombinationProbs[n][j].first << "\t"
                 << recombinationProbs[n][j].second << std::endl;
        }
    }
    unsigned long size = start_set0.size();
    auto size_d = (double) size;
    std::sort(error_rate_rank_h1.begin(), error_rate_rank_h1.end());
    std::sort(error_rate_rank_h2.begin(), error_rate_rank_h2.end());
    //todo: topK might be larger than the size of error_rate_rank_h1 or error_rate_rank_h2
    for (int k = 0; k < topK; ++k) {
        int n2 = error_rate_rank_h2[k].second;
        int n1 = error_rate_rank_h1[k].second;
        if (size_d == 0) {
            res[0] += ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][0].second / nHaps) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][3].first /
                       (nHaps - size_d)) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][3].second / nHaps);
            res[1] += ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][1].second / nHaps) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][2].first /
                       (nHaps - size_d)) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][2].second / nHaps);
        } else if (size_d == nHaps) {
            res[0] += ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][0].first / size_d) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][0].second / nHaps) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][3].second / nHaps);
            res[1] += ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][1].first / size_d) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][1].second / nHaps) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][2].second / nHaps);
        } else {
            res[0] += ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][0].first / size_d) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][0].second / nHaps) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][3].first /
                       (nHaps - size_d)) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][3].second / nHaps);
            res[1] += ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][1].first / size_d) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][1].second / nHaps) +
                      ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][2].first /
                       (nHaps - size_d)) +
                      ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][2].second / nHaps);
        }
        file1 << "edge weight logging , start = " << block1.blk_idx << " , end = " << block2.blk_idx << ", k = " << k
              << ", bestK id = " << n1 << ";" << n2 << std::endl;
        file1 << "weights = " << res[0] << ";" << res[1] << ", size_d = " << size_d << ", nHaps = " << nHaps
              << std::endl;
        file1 << "k = " << k << ", error_rate_h1 = " << error_rate_rank_h1[k].first << "1/exp(ax) = "
              << 1 / exp(alpha * error_rate_rank_h1[k].first) << ", error_rate_h1_id = "
              << error_rate_rank_h1[k].second << std::endl;
        file1 << "k = " << k << ", error_rate_h2 = " << error_rate_rank_h2[k].first << "1/exp(ax) = "
              << 1 / exp(alpha * error_rate_rank_h2[k].first) << ", error_rate_h2_id = "
              << error_rate_rank_h2[k].second << std::endl;
        for (int i = 0; i < 4; ++i) {
            file1 << "n1 = " << n1 << ", i = " << i << ", recombinationProbs = " << recombinationProbs[n1][i].first
                  << ";"
                  << recombinationProbs[n1][i].second << std::endl;
            file1 << "n2 = " << n2 << ", i = " << i << ", recombinationProbs = " << recombinationProbs[n2][i].first
                  << ";"
                  << recombinationProbs[n2][i].second << std::endl;
        }
    }
    for (int i = 0; i < bestK.size(); ++i) {
        file2 << "n1 = " << error_rate_rank_h1[i].second << ", err_rate = " << error_rate_rank_h1[i].first
              << ", 1/exp(ax) = " << 1 /
                                     exp(alpha * error_rate_rank_h1[i].first) << std::endl;
        file2 << "n2 = " << error_rate_rank_h2[i].second << ", err_rate = " << error_rate_rank_h2[i].first
              << ", 1/exp(ax) = " << 1 /
                                     exp(alpha * error_rate_rank_h2[i].first) << std::endl;
    }
    file1 << "topK value is " << topK << std::endl;
    file.close();
    return res;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByBlocksV5(const Block &block1, const Block &block2) {
    /**
     * 1, marking the haps in block1 into the reference panels
     */
    auto *res = new double[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    double cm = cms[block2.snp_idx.front()] - cms[block1.snp_idx.back()];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    double nHaps = (double) NHaps;
//        double Ne = 100000;
//        double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
    std::set<int> start_set0;
    // error rate rank list
    std::vector<std::pair<double, int>> error_rate_rank_h1;
    std::vector<std::pair<double, int>> complement_rate_rank_h1;
    std::vector<std::pair<double, int>> error_rate_rank_h2;
    std::vector<std::pair<double, int>> complement_rate_rank_h2;
    error_rate_rank_h1.resize(bestK.size());
    error_rate_rank_h2.resize(bestK.size());
    complement_rate_rank_h1.resize(bestK.size());
    complement_rate_rank_h2.resize(bestK.size());
    // recombinationProbs: the first value in pair indicates copy, while the second value in pair indicates recombination
    std::vector<std::vector<std::pair<double, double>>> recombinationProbs;
    recombinationProbs.resize(bestK.size());
    /// topK means the filtered k haplotypes which would be used to construct the weight edges
    int topK = bestK.size() <= 10 ? bestK.size() : 20 * bestK.size() / 100;
    /// a scaling score for different het site
    int het_dif_scale = 30;
    std::ofstream file;
    std::ofstream file1;
    std::ofstream file2;
    file.open("flow_v5.txt", std::ios::app);
    file1.open("flow_v5_weight.txt", std::ios::app);
    file2.open("flow_v5_error_rate_rank.txt", std::ios::app);
    double alpha = 10.0;
    for (int n = 0; n < bestK.size(); ++n) {
        recombinationProbs[n].resize(4);
        /// calculating the Similarity with hamming distance
        int block1_dif_snp_count_0 = 0, block1_dif_snp_count_1 = 0, block2_dif_snp_count_0 = 0, block2_dif_snp_count_1 = 0;
        int block1_total_count = 0, block2_total_count = 0;
        /// get hamming distance for block1 -> haps
        block1_total_count = block1.snp_idx.back() - block1.snp_idx.front() + 1;
        block2_total_count = block2.snp_idx.back() - block2.snp_idx.front() + 1;
        for (int i = 0; i < block1.het_idx.size(); i++) {
            block1_dif_snp_count_0 += (block1.hap1[i] != this->getBit(n, block1.het_idx[i] + 1)) ? het_dif_scale : 0;
            block1_dif_snp_count_1 += (block1.hap2[i] != this->getBit(n, block1.het_idx[i] + 1)) ? het_dif_scale : 0;
            if (i == 0) {
                continue;
            }
            block1_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
            block1_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
        }
        /// get hamming distance for block2 -> haps
        for (int i = 0; i < block2.het_idx.size(); i++) {
            block2_dif_snp_count_0 += (block2.hap1[i] != this->getBit(n, block2.het_idx[i] + 1)) ? het_dif_scale : 0;
            block2_dif_snp_count_1 += (block2.hap2[i] != this->getBit(n, block2.het_idx[i] + 1)) ? het_dif_scale : 0;
            if (i == 0) {
                continue;
            }
            block2_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
            block2_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
        }
        /// calculating the similarity of B_1_1 with B_2_1, B_1_1 with B_2_2
        /// find the min error rate of the two link mode, and set these two error rate as distance
        double P_H_B1_0_B2_0 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_0_B2_1 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_0 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_1 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        /// here we always assume that the mat hap is P_H_B1_0_B2_0 and the pat hap is P_H_B1_1_B2_1
//        error_rate_rank_h1[n].first = P_H_B1_0_B2_0;
//        error_rate_rank_h1[n].second = n;
//        complement_rate_rank_h1[n].first = P_H_B1_1_B2_1;
//        complement_rate_rank_h1[n].second = n;
//        error_rate_rank_h2[n].first = P_H_B1_0_B2_1;
//        error_rate_rank_h2[n].second = n;
//        complement_rate_rank_h2[n].first = P_H_B1_1_B2_0;
//        complement_rate_rank_h2[n].second = n;

        /// calculating the recombination probability by the last snp of block1 and the first snp of block2

        int bit1 = this->getBit(n, block1.het_idx.back() + 1);
        int bit2 = this->getBit(n, block2.het_idx.front() + 1);

        if (bit1 == block1.hap1.back()) {
            start_set0.insert(n);
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[n][0].first += (1 - ro_m);
                recombinationProbs[n][0].second += ro_m;
                recombinationProbs[n][2].second += ro_m;
            } else {
                recombinationProbs[n][1].first += (1 - ro_m);
                recombinationProbs[n][1].second += ro_m;
                recombinationProbs[n][3].second += ro_m;
            }
        } else {
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[n][2].first += (1 - ro_m);
                recombinationProbs[n][2].second += ro_m;
                recombinationProbs[n][0].second += ro_m;
            } else {
                recombinationProbs[n][3].first += (1 - ro_m);
                recombinationProbs[n][3].second += ro_m;
                recombinationProbs[n][1].second += ro_m;
            }
        }
        //todo: we might should add a constraint for topK before this storing operation
        /// count of different snp sites in block1 combined with block2 compared to ref hap, and error_rate_rank would store the rate of this count with total count
        int blk1_hap1_back = block1.hap1.back();
        int blk2_hap1_front = block2.hap1.front();
        if (blk1_hap1_back == bit1) {
            if (blk2_hap1_front == bit2) {
                /// block1 hap0 -> block2 hap0
                error_rate_rank_h1[n].first = P_H_B1_0_B2_0;
                error_rate_rank_h1[n].second = n;
                complement_rate_rank_h1[n].first = 1;
                complement_rate_rank_h1[n].second = n;
                error_rate_rank_h2[n].first = 1;
                error_rate_rank_h2[n].second = n;
                complement_rate_rank_h2[n].first = 1;
                complement_rate_rank_h2[n].second = n;
            } else {
                /// block1 hap0 -> block2 hap1
                error_rate_rank_h1[n].first = 1;
                error_rate_rank_h1[n].second = n;
                complement_rate_rank_h1[n].first = 1;
                complement_rate_rank_h1[n].second = n;
                error_rate_rank_h2[n].first = P_H_B1_0_B2_1;
                error_rate_rank_h2[n].second = n;
                complement_rate_rank_h2[n].first = 1;
                complement_rate_rank_h2[n].second = n;
            }
        } else {
            if (blk2_hap1_front == bit2) {
                /// block1 hap1 -> block2 hap0
                error_rate_rank_h1[n].first = 1;
                error_rate_rank_h1[n].second = n;
                complement_rate_rank_h1[n].first = 1;
                complement_rate_rank_h1[n].second = n;
                error_rate_rank_h2[n].first = 1;
                error_rate_rank_h2[n].second = n;
                complement_rate_rank_h2[n].first = P_H_B1_1_B2_0;
                complement_rate_rank_h2[n].second = n;
            } else {
                /// block1 hap1 -> block2 hap1
                error_rate_rank_h1[n].first = 1;
                error_rate_rank_h1[n].second = n;
                complement_rate_rank_h1[n].first = P_H_B1_1_B2_1;
                complement_rate_rank_h1[n].second = n;
                error_rate_rank_h2[n].first = 1;
                error_rate_rank_h2[n].second = n;
                complement_rate_rank_h2[n].first = 1;
                complement_rate_rank_h2[n].second = n;
            }
        }
        file << "similarity log bestK id = " << n << std::endl;
        file << "block1_dif_count_0 = " << block1_dif_snp_count_0 << "\t block1_dif_count_1 = "
             << block1_dif_snp_count_1 << "\t block2_dif_count_0 = " << block2_dif_snp_count_0
             << "\t block2_dif_count_1 = "
             << block2_dif_snp_count_1 <<
             "\t block1_total_count = " << block1_total_count << "\t block2_total_count = " << block2_total_count
             << std::endl;
        file << "P_B1_0_B2_0 = " << P_H_B1_0_B2_0 << "\t P_H_B1_0_B2_1 = " << P_H_B1_0_B2_1 << "\t P_H_B1_1_B2_0 = "
             << P_H_B1_1_B2_0 << "\t P_H_B1_1_B2_1 = " << P_H_B1_1_B2_1 << std::endl;
//        file << "P_H1 = " << P_H1 << ", P_H2 = " << P_H2 << std::endl;
        file << "1/exp(ax) value of P_B1_0_B2_0 = " << 1 / exp(alpha * P_H_B1_0_B2_0) << ", P_H_B1_0_B2_1 = "
             << 1 / exp(alpha * P_H_B1_0_B2_1) << ", P_H_B1_1_B2_0 = " << 1 / exp(alpha * P_H_B1_1_B2_0)
             << ", P_H_B1_1_B2_1 = " << P_H_B1_1_B2_1 << std::endl;
        file << "recombinationProbs in n loops" << std::endl;
        file << "cm = " << cm << ", ro_m = " << ro_m << std::endl;
        for (int j = 0; j < 4; ++j) {
            file << "recombinationProbs[" << j << "] = " << recombinationProbs[n][j].first << "\t"
                 << recombinationProbs[n][j].second << std::endl;
        }
    }
    unsigned long size = start_set0.size();
    auto size_d = (double) size;
    bool comP_added = false;
    std::sort(error_rate_rank_h1.begin(), error_rate_rank_h1.end());
    std::sort(error_rate_rank_h2.begin(), error_rate_rank_h2.end());
    std::sort(complement_rate_rank_h1.begin(), complement_rate_rank_h1.end());
    std::sort(complement_rate_rank_h2.begin(), complement_rate_rank_h2.end());
    //todo: topK might be larger than the size of error_rate_rank_h1 or error_rate_rank_h2
    for (int k = 0; k < topK; ++k) {
        int n1 = error_rate_rank_h1[k].second;
        int n2 = error_rate_rank_h2[k].second;
        int com_n1 = complement_rate_rank_h1[k].second;
        int com_n2 = complement_rate_rank_h2[k].second;
        ///  hap1 -> hap1 no link and hap1 -> hap2 no link (contain size_d == 0)
        if (error_rate_rank_h1[k].first == 1 && error_rate_rank_h2[k].first == 1) {
            break;
        }
        /// hap2 -> hap2 no link and hap2 -> hap1 no link (contain size_d == nHaps)
        if (complement_rate_rank_h1[0].first == 1 && complement_rate_rank_h2[0].first == 1) {
            break;
        }
        /// hap1 -> hap1 no link and hap2 -> hap1 no link (similar to size_d == 0 on snp Site 2)
        /// or hap1 -> hap2 no link and hap2 -> hap2 no link (similar to size_d == nHaps on snp Site 2)
        if ((error_rate_rank_h1[k].first == 1 && complement_rate_rank_h2[0].first == 1) ||
            (error_rate_rank_h2[k].first == 1 && complement_rate_rank_h1[0].first == 1)) {
            break;
        }
        /// within a single for loop, accumulate all the existed complementary haps
        if (!comP_added) {
            comP_added = true;
            for (int i = 0; i < topK; ++i) {
                if (complement_rate_rank_h1[i].first >= 1 && complement_rate_rank_h2[i].first >= 1) {
                    break;
                }
                /// there are contributed by hap2->hap2
                if (complement_rate_rank_h1[i].first < 1) {
                    res[0] +=
                            (1 / exp(alpha * complement_rate_rank_h1[i].first)) * recombinationProbs[com_n1][3].first /
                            (nHaps - size_d);
                    res[0] +=
                            (1 / exp(alpha * complement_rate_rank_h1[i].first)) * recombinationProbs[com_n1][3].second /
                            nHaps;
                    res[1] +=
                            (1 / exp(alpha * complement_rate_rank_h1[i].first)) * recombinationProbs[com_n1][1].second /
                            nHaps;
                }
                /// these are contributed by hap2->hap1
                if (complement_rate_rank_h2[i].first < 1) {
                    res[1] +=
                            (1 / exp(alpha * complement_rate_rank_h2[i].first)) * recombinationProbs[com_n2][2].first /
                            (nHaps - size_d);
                    res[1] +=
                            (1 / exp(alpha * complement_rate_rank_h2[i].first)) * recombinationProbs[com_n2][2].second /
                            nHaps;
                    res[0] +=
                            (1 / exp(alpha * complement_rate_rank_h2[i].first)) * recombinationProbs[com_n2][0].second /
                            nHaps;
                }
            }
        }
        /// these are contributed by the link mode of hap1 -> hap1
        /// hap1->hap1 copy
        res[0] += ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][0].first / size_d);
        /// hap1->hap1 recombination provided by hap1 -> hap1 copy
        res[0] += ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][0].second / nHaps);
        /// hap2 -> hap1 recombination provided by hap1 -> hap1 copy
        res[1] += ((1 / exp(alpha * error_rate_rank_h1[k].first)) * recombinationProbs[n1][2].second / nHaps);

        /// these are contributed by the link mode of hap1 -> hap2
        /// hap1 -> hap2 copy
        res[1] += ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][1].first / size_d);
        /// hap1 -> hap2 recombination provided by hap1 -> hap2 copy
        res[1] += ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][1].second / nHaps);
        /// hap2-> hap2 recombination provided by hap1 -> hap2;
        res[0] += ((1 / exp(alpha * error_rate_rank_h2[k].first)) * recombinationProbs[n2][3].second / nHaps);

        file1 << "edge weight logging , start = " << block1.blk_idx << " , end = " << block2.blk_idx << ", k = " << k
              << ", bestK id = " << n1 << ";" << n2 << std::endl;
        file1 << "weights = " << res[0] << ";" << res[1] << ", size_d = " << size_d << ", nHaps = " << nHaps
              << std::endl;
        file1 << "k = " << k << ", error_rate_h1 = " << error_rate_rank_h1[k].first << "1/exp(ax) = "
              << 1 / exp(alpha * error_rate_rank_h1[k].first) << ", error_rate_h1_id = "
              << error_rate_rank_h1[k].second << std::endl;
        file1 << "k = " << k << ", error_rate_h2 = " << error_rate_rank_h2[k].first << "1/exp(ax) = "
              << 1 / exp(alpha * error_rate_rank_h2[k].first) << ", error_rate_h2_id = "
              << error_rate_rank_h2[k].second << std::endl;
        for (int i = 0; i < 4; ++i) {
            file1 << "n1 = " << n1 << ", i = " << i << ", recombinationProbs = " << recombinationProbs[n1][i].first
                  << ";"
                  << recombinationProbs[n1][i].second << std::endl;
            file1 << "n2 = " << n2 << ", i = " << i << ", recombinationProbs = " << recombinationProbs[n2][i].first
                  << ";"
                  << recombinationProbs[n2][i].second << std::endl;
        }
    }
    for (int i = 0; i < bestK.size(); ++i) {
        file2 << "n1 = " << error_rate_rank_h1[i].second << ", err_rate = " << error_rate_rank_h1[i].first
              << ", 1/exp(ax) = " << 1 /
                                     exp(alpha * error_rate_rank_h1[i].first) << std::endl;
        file2 << "n2 = " << error_rate_rank_h2[i].second << ", err_rate = " << error_rate_rank_h2[i].first
              << ", 1/exp(ax) = " << 1 /
                                     exp(alpha * error_rate_rank_h2[i].first) << std::endl;
    }
    file1 << "topK value is " << topK << std::endl;
    file.close();
    return res;
}

double *HaploBitsArrays::CompressedHaploBitsT::getWeightByBlocksV6(const Block &block1, const Block &block2) {
    /**
     * 1, separating the haplotypes by het dif counts
     * 2, loading the haplotypes from 0 dif, 1 dif and increasing
     */
    auto *res = new double[4];
    for (int i = 0; i < 4; ++i) {
        res[i] = 0;
    }
    double cm = cms[block2.snp_idx.front()] - cms[block1.snp_idx.back()];
    if (cm == 0) {
        cm = 0.00000000000001;
    }
    double nHaps = (double) NHaps;
//        double Ne = 100000;
//        double ro_m = 1 - exp(-4 * Ne * cm / nHaps);
    double ro_m = 1 - exp(-2 * nHaps * cm / nHaps);
    std::set<int> start_set0;
    /// het dif count map
    std::map<int, std::vector<std::pair<double, int>>> error_rate_rank_h1_map;
    std::map<int, std::vector<std::pair<double, int>>> error_rate_rank_h2_map;
    std::map<int, std::vector<std::pair<double, int>>> complementary_rate_rank_h1_map;
    std::map<int, std::vector<std::pair<double, int>>> complementary_rate_rank_h2_map;
    // recombinationProbs: the first value in pair indicates copy, while the second value in pair indicates recombination
    std::vector<std::vector<std::pair<double, double>>> recombinationProbs;
    recombinationProbs.resize(bestK.size());
    /// topK means the filtered k haplotypes which would be used to construct the weight edges
    int topK = bestK.size() <= 10 ? bestK.size() : 2 * bestK.size() / 100;
    /// a scaling score for different het site
    int het_dif_scale = 100;
    std::ofstream file;
    std::ofstream file1;
    std::ofstream file2;
    std::ofstream file3;
    std::ofstream file4;
    file.open("flow_v6.txt", std::ios::app);
    file1.open("flow_v6_weight.txt", std::ios::app);
    file2.open("flow_v6_error_rate_rank.txt", std::ios::app);
    file3.open("flow_v6_error.log", std::ios::app);
    file4.open("flow_v6_res.txt", std::ios::app);
    double alpha = 10.0;
    for (int n = 0; n < bestK.size(); ++n) {
        recombinationProbs[n].resize(4);
        /// calculating the Similarity with hamming distance
        int block1_dif_snp_count_0 = 0, block1_dif_snp_count_1 = 0, block2_dif_snp_count_0 = 0, block2_dif_snp_count_1 = 0;
        int block1_total_count = 0, block2_total_count = 0;
        int block1_dif_het_count_0 = 0, block1_dif_het_count_1 = 0, block2_dif_het_count_0 = 0, block2_dif_het_count_1 = 0;
        /// get hamming distance for block1 -> haps
        block1_total_count = block1.snp_idx.back() - block1.snp_idx.front() + 1;
        block2_total_count = block2.snp_idx.back() - block2.snp_idx.front() + 1;
        for (int i = 0; i < block1.het_idx.size(); i++) {
            block1_dif_snp_count_0 += (block1.hap1[i] != this->getBit(n, block1.het_idx[i] + 1)) ? het_dif_scale : 0;
            block1_dif_snp_count_1 += (block1.hap2[i] != this->getBit(n, block1.het_idx[i] + 1)) ? het_dif_scale : 0;
            if ((block1.hap1[i] != this->getBit(n, block1.het_idx[i] + 1)) ||
                (block1.hap2[i] != this->getBit(n, block1.het_idx[i] + 1))) {
                block1_total_count += het_dif_scale - 1;
            }
            block1_dif_het_count_0 += (block1.hap1[i] != this->getBit(n, block1.het_idx[i] + 1)) ? 1 : 0;
            block1_dif_het_count_1 += (block1.hap2[i] != this->getBit(n, block1.het_idx[i] + 1)) ? 1 : 0;
            if (i == 0) {
                continue;
            }
            block1_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
            block1_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block1.het_idx[i] + 1];
        }
        /// get hamming distance for block2 -> haps
        for (int i = 0; i < block2.het_idx.size(); i++) {
            block2_dif_snp_count_0 += (block2.hap1[i] != this->getBit(n, block2.het_idx[i] + 1)) ? het_dif_scale : 0;
            block2_dif_snp_count_1 += (block2.hap2[i] != this->getBit(n, block2.het_idx[i] + 1)) ? het_dif_scale : 0;
            if ((block2.hap1[i] != this->getBit(n, block2.het_idx[i] + 1)) ||
                (block2.hap2[i] != this->getBit(n, block2.het_idx[i] + 1))) {
                block2_total_count += het_dif_scale - 1;
            }
            block2_dif_het_count_0 += (block2.hap1[i] != this->getBit(n, block2.het_idx[i] + 1)) ? 1 : 0;
            block2_dif_het_count_1 += (block2.hap2[i] != this->getBit(n, block2.het_idx[i] + 1)) ? 1 : 0;
            if (i == 0) {
                continue;
            }
            block2_dif_snp_count_0 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
            block2_dif_snp_count_1 += haploBitsTWithHom[bestK[n]][2 * block2.het_idx[i] + 1];
        }
        /// calculating the similarity of B_1_1 with B_2_1, B_1_1 with B_2_2
        /// find the min error rate of the two link mode, and set these two error rate as distance
        double P_H_B1_0_B2_0 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_0_B2_1 = (1.0 * (block1_dif_snp_count_0 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_0 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_0) /
                                (block1_total_count + block2_total_count));
        double P_H_B1_1_B2_1 = (1.0 * (block1_dif_snp_count_1 + block2_dif_snp_count_1) /
                                (block1_total_count + block2_total_count));

        /// calculating the recombination probability by the last snp of block1 and the first snp of block2

        int bit1 = this->getBit(n, block1.het_idx.back() + 1);
        int bit2 = this->getBit(n, block2.het_idx.front() + 1);

        if (bit1 == block1.hap1.back()) {
            start_set0.insert(n);
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[n][0].first += (1 - ro_m);
                recombinationProbs[n][0].second += ro_m;
                recombinationProbs[n][2].second += ro_m;
            } else {
                recombinationProbs[n][1].first += (1 - ro_m);
                recombinationProbs[n][1].second += ro_m;
                recombinationProbs[n][3].second += ro_m;
            }
        } else {
            if (bit2 == block2.hap1.front()) {
                recombinationProbs[n][2].first += (1 - ro_m);
                recombinationProbs[n][2].second += ro_m;
                recombinationProbs[n][0].second += ro_m;
            } else {
                recombinationProbs[n][3].first += (1 - ro_m);
                recombinationProbs[n][3].second += ro_m;
                recombinationProbs[n][1].second += ro_m;
            }
        }
        /// count of different snp sites in block1 combined with block2 compared to ref hap, and error_rate_rank would store the rate of this count with total count
        int blk1_hap1_back = block1.hap1.back();
        int blk2_hap1_front = block2.hap1.front();
        if (blk1_hap1_back == bit1) {
            if (blk2_hap1_front == bit2) {
                /// block1 hap0 -> block2 hap0
                error_rate_rank_h1_map[block1_dif_het_count_0 + block2_dif_het_count_0].emplace_back(P_H_B1_0_B2_0, n);
            } else {
                /// block1 hap0 -> block2 hap1
                error_rate_rank_h2_map[block1_dif_het_count_0 + block2_dif_het_count_1].emplace_back(P_H_B1_0_B2_1, n);
            }
        } else {
            if (blk2_hap1_front == bit2) {
                /// block1 hap1 -> block2 hap0
                complementary_rate_rank_h2_map[block1_dif_het_count_1 + block2_dif_het_count_0].emplace_back(
                        P_H_B1_1_B2_0, n);
            } else {
                /// block1 hap1 -> block2 hap1
                complementary_rate_rank_h1_map[block1_dif_het_count_1 + block2_dif_het_count_1].emplace_back(
                        P_H_B1_1_B2_1, n);
            }
        }
    }
    unsigned long size = start_set0.size();
    auto size_d = (double) size;
    bool comP_added = false;
    /// find the min different error list
    if (error_rate_rank_h1_map.empty() && error_rate_rank_h2_map.empty()) {
        return res;
    }
    if (complementary_rate_rank_h1_map.empty() && complementary_rate_rank_h2_map.empty()) {
        return res;
    }
    if ((error_rate_rank_h1_map.empty() && complementary_rate_rank_h2_map.empty()) ||
        (error_rate_rank_h2_map.empty() && complementary_rate_rank_h1_map.empty())) {
        return res;
    }
//    std::cout << "blk1 id = " << block1.blk_idx << "\t" << "block2 id = " << block2.blk_idx << std::endl;
//    std::cout << "error_rate_rank_h1_map size = " << error_rate_rank_h1_map.size() << std::endl;
//    for (const auto &item : error_rate_rank_h1_map) {
//        std::cout << item.second.size() << std::endl;
//    }
//    std::cout << "error_rate_rank_h2_map size = " << error_rate_rank_h2_map.size() << std::endl;
//    for (const auto &item : error_rate_rank_h2_map) {
//        std::cout << item.second.size() << std::endl;
//    }
//    std::cout << "complementary_rate_rank_h1_map size = " << complementary_rate_rank_h1_map.size() << std::endl;
//    for (const auto &item : complementary_rate_rank_h1_map) {
//        std::cout << item.second.size() << std::endl;
//    }
//    std::cout << "complementary_rate_rank_h2_map size = " << complementary_rate_rank_h2_map.size() << std::endl;
//    for (const auto &item : complementary_rate_rank_h2_map) {
//        std::cout << item.second.size() << std::endl;
//    }
    std::vector<std::pair<double, int>> error_rate_rank_h1_vector;
    std::vector<std::pair<double, int>> error_rate_rank_h2_vector;
    std::vector<std::pair<double, int>> complementary_rate_rank_h1_vector;
    std::vector<std::pair<double, int>> complementary_rate_rank_h2_vector;
    if (!error_rate_rank_h1_map.empty()) {
        error_rate_rank_h1_vector = error_rate_rank_h1_map.begin()->second;
        std::sort(error_rate_rank_h1_vector.begin(), error_rate_rank_h1_vector.end());
    }
    if (!error_rate_rank_h2_map.empty()) {
        error_rate_rank_h2_vector = error_rate_rank_h2_map.begin()->second;
        std::sort(error_rate_rank_h2_vector.begin(), error_rate_rank_h2_vector.end());
    }
    if (!complementary_rate_rank_h1_map.empty()) {
        complementary_rate_rank_h1_vector = complementary_rate_rank_h1_map.begin()->second;
        std::sort(complementary_rate_rank_h1_vector.begin(), complementary_rate_rank_h1_vector.end());
    }
    if (!complementary_rate_rank_h2_map.empty()) {
        complementary_rate_rank_h2_vector = complementary_rate_rank_h2_map.begin()->second;
        std::sort(complementary_rate_rank_h2_vector.begin(), complementary_rate_rank_h2_vector.end());
    }
    std::cout << "err_rate_rank_vectors" << error_rate_rank_h1_vector.size() << "\t" << error_rate_rank_h2_vector.size()
              << "\t"
              << complementary_rate_rank_h1_vector.size() << "\t" << complementary_rate_rank_h2_vector.size()
              << std::endl;
    if (error_rate_rank_h1_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap1 -> hap1, the least dif count is " << error_rate_rank_h1_map.begin()->first
              << std::endl;
    }
    if (error_rate_rank_h2_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap1 -> hap2, the least dif count is " << error_rate_rank_h2_map.begin()->first
              << std::endl;
    }
    if (complementary_rate_rank_h1_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap2 -> hap2, the least dif count is "
              << complementary_rate_rank_h1_map.begin()->first << std::endl;
    }
    if (complementary_rate_rank_h2_map.begin()->first != 0) {
        file3 << "no ref supplies mode hap2 -> hap1, the least dif count is "
              << complementary_rate_rank_h2_map.begin()->first << std::endl;
    }
    for (int i = 0; i < topK; ++i) {
        if (i < error_rate_rank_h1_vector.size()) {
            int n = error_rate_rank_h1_vector[i].second;
            /// hap1->hap1 copy
            res[0] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][0].first / size_d);
            /// hap1->hap1 recombination provided by hap1 -> hap1 copy
            res[0] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][0].second / nHaps);
            /// hap2 -> hap1 recombination provided by hap1 -> hap1 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h1_vector[i].first)) * recombinationProbs[n][2].second / nHaps);
        }
        if (i < error_rate_rank_h2_vector.size()) {
            int n = error_rate_rank_h2_vector[i].second;
            /// hap1 -> hap2 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][1].first / size_d);
            /// hap1 -> hap2 recombination provided by hap1 -> hap2 copy
            res[1] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][1].second / nHaps);
            /// hap2-> hap2 recombination provided by hap1 -> hap2;
            res[0] += ((1 / exp(alpha * error_rate_rank_h2_vector[i].first)) * recombinationProbs[n][3].second / nHaps);
        }
        if (i < complementary_rate_rank_h1_vector.size()) {
            int n = complementary_rate_rank_h1_vector[i].second;
            /// hap2 -> hap2 copy
            res[0] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][3].first /
                      (nHaps - size_d);
            /// hap2 -> hap2 recombination
            res[0] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][3].second /
                      nHaps;
            /// hap1 -> hap2 recombination provided by hap2 -> hap2
            res[1] += (1 / exp(alpha * complementary_rate_rank_h1_vector[i].first)) * recombinationProbs[n][1].second /
                      nHaps;
        }
        if (i < complementary_rate_rank_h2_vector.size()) {
            int n = complementary_rate_rank_h2_vector[i].second;
            /// hap2 -> hap1 copy
            res[1] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][2].first /
                      (nHaps - size_d);
            /// hap2 -> hap1 recombination
            res[1] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][2].second /
                      nHaps;
            /// hap1 -> hap1 recombination provided by hap2 -> hap1
            res[0] += (1 / exp(alpha * complementary_rate_rank_h2_vector[i].first)) * recombinationProbs[n][0].second /
                      nHaps;
        }
    }
    file2 << "block1 = " << block1.blk_idx << ", block2 = " << block2.blk_idx << std::endl;
    file2 << "error_rate_rank_h1_vector" << std::endl;
    for (const auto &item : error_rate_rank_h1_vector) {
        file2 << item.first << ";" << item.second << std::endl;
    }
    file2 << "error_rate_rank_h2_vector" << std::endl;
    for (const auto &item : error_rate_rank_h2_vector) {
        file2 << item.first << ";" << item.second << std::endl;
    }
    file2 << "complementary_rate_rank_h1_vector" << std::endl;
    for (const auto &item : complementary_rate_rank_h1_vector) {
        file2 << item.first << ";" << item.second << std::endl;
    }
    file2 << "complementary_rate_rank_h2_vector" << std::endl;
    for (const auto &item : complementary_rate_rank_h2_vector) {
        file2 << item.first << ";" << item.second << std::endl;
    }
    file4 << "block1 = " << block1.blk_idx << ", block2 = " << block2.blk_idx << ", res = " << res[0] << ";" << res[1]
          << ";" << res[2] << ";" << res[3] << std::endl;
    file.close();
    file1.close();
    file2.close();
    file3.close();
    file4.close();
    return res;
}

void HaploBitsArrays::CompressedHaploBitsT::phasingInBlock(std::map<uint, Block> &hapMap, int blockSize) {
    /// for loop cross each block, find out the potential complementary haps
    for (auto pair = hapMap.begin(); pair != hapMap.end(); pair++) {
        Block block = pair->second;
//        std::cout << "block id = " << block.blk_idx << std::endl;
        std::set<std::string> cur_hap_set;
        std::set<std::string> comp_hap_set;
        std::map<std::string, BlockResItem> blockCompCountMap;
        std::map<std::string, BlockResItem> blockResMap;
        /// first look through the n best haps
        for (int n = 0; n < bestK.size(); ++n) {
            std::string cur_hap;
            std::string comp_hap;
            for (int i = 0; i < block.het_idx.size(); ++i) {
                uint het_id = block.het_idx[i];
                int bit = getBit(n, het_id + 1);
                cur_hap += std::to_string(bit);
            }
            if (cur_hap_set.count(cur_hap) == 0) {
                cur_hap_set.insert(cur_hap);
            }
            if (blockCompCountMap.count(cur_hap) == 0) {
                BlockResItem item = BlockResItem(1, 0);
                blockCompCountMap.insert(std::make_pair(cur_hap, item));
            } else {
                const std::map<std::string, BlockResItem>::iterator &iterator = blockCompCountMap.find(cur_hap);
                iterator->second.cur_count++;
            }
            comp_hap = find_comp(cur_hap);
            if (comp_hap_set.count(comp_hap) == 0) {
                comp_hap_set.insert(comp_hap);
            }
            if (comp_hap_set.count(cur_hap) != 0) {
                if (blockCompCountMap.count(cur_hap) == 0 || blockCompCountMap.find(cur_hap)->second.comp_count == 0) {
                    if (blockCompCountMap.count(comp_hap) == 0) {
                        std::cout << "error occurs! Something wrong with in block comp hap" << std::endl;
                        std::cout << "block id = " << block.blk_idx << ", cur hap = " << cur_hap << ", comp hap = "
                                  << comp_hap << std::endl;
                        return;
                    }
                    blockCompCountMap.find(comp_hap)->second.comp_count++;
                    if (blockResMap.count(comp_hap) == 0) {
                        blockResMap.insert(std::make_pair(comp_hap, blockCompCountMap.find(comp_hap)->second));
                    } else {
                        blockResMap.find(comp_hap)->second = blockCompCountMap.find(comp_hap)->second;
                    }
                } else {
                    if (blockResMap.count(cur_hap) == 0) {
                        blockResMap.insert(std::make_pair(cur_hap, blockCompCountMap.find(cur_hap)->second));
                    } else {
                        blockResMap.find(cur_hap)->second = blockCompCountMap.find(cur_hap)->second;
                    }
                }
            }
        }
        /// cur block contains no complementary haps
        if (blockResMap.size() == 0) {
            continue;
        }
        /// cur block only contains one complementary haps
        if (blockResMap.size() == 1) {
            if (blockResMap.begin()->first.size() != block.het_idx.size()) {
                std::cout << "error occurs!" << std::endl;
                std::cout << "hap size not equal to het idx size" << std::endl;
                std::cout << "block id = " << block.blk_idx << ", find comp hap = " << blockResMap.begin()->first
                          << ", find comp hap size = " << blockResMap.begin()->first.size() << ", het idx size = "
                          << block.het_idx.size() << std::endl;
                return;
            }
            std::string hap_res = blockResMap.begin()->first;
            for (int i = 0; i < block.het_idx.size(); ++i) {
                pair->second.phased_het_idx.emplace_back(i);
                if (hap_res[i] == '1') {
                    pair->second.pseudo_hap1.emplace_back(1);
                    pair->second.pseudo_hap2.emplace_back(0);
                } else {
                    pair->second.pseudo_hap1.emplace_back(0);
                    pair->second.pseudo_hap2.emplace_back(1);
                }
            }
        } else {
            /// reduce the blockResMap if it contains multi complementary haps
            std::set<int> exclude_id_set;
            std::set<int> cross_id_set1, cross_id_set2, cross_id_set3, cross_id_set4, cross_id_set5, cross_id_set6;
            for (auto cur_iter = blockResMap.begin(); cur_iter != blockResMap.end(); cur_iter++) {
                auto next_iter = cur_iter;
                next_iter++;
                int distance = 1;
                while (next_iter != blockResMap.end()) {
                    std::set<int> dif_idx_set, same_idx_set;
                    std::string cur_hap = cur_iter->first;
                    std::string next_hap = next_iter->first;
                    for (int i = 0; i < cur_hap.size(); ++i) {
                        if (cur_hap[i] != next_hap[i]) {
                            dif_idx_set.insert(i);
                        } else {
                            same_idx_set.insert(i);
                        }
                    }
                    if (cur_iter == blockResMap.begin() && distance == 1) {
                        cross_id_set1 = dif_idx_set;
                        cross_id_set2 = same_idx_set;
                    } else {
                        std::vector<std::set<int>> cross_id_set_list;
                        int max_len = 0, sec_len = 0;
                        /// find the cross set1 (start with dif set of hap1 and hap2)
                        std::set_intersection(cross_id_set1.begin(), cross_id_set1.end(), dif_idx_set.begin(),
                                              dif_idx_set.end(), std::inserter(cross_id_set3, cross_id_set3.begin()));
                        std::set_intersection(cross_id_set1.begin(), cross_id_set1.end(), same_idx_set.begin(),
                                              same_idx_set.end(), std::inserter(cross_id_set4, cross_id_set4.begin()));
//                        if (cross_id_set3.size() >= cross_id_set4.size()) {
//                            cross_id_set1 = cross_id_set3;
//                        } else {
//                            cross_id_set1 = cross_id_set4;
//                        }
//                        cross_id_set3.clear();
//                        cross_id_set4.clear();
                        /// find the cross set2 (start with same set of hap1 and hap2)
                        std::set_intersection(cross_id_set2.begin(), cross_id_set2.end(), dif_idx_set.begin(),
                                              dif_idx_set.end(), std::inserter(cross_id_set5, cross_id_set5.begin()));
                        std::set_intersection(cross_id_set2.begin(), cross_id_set2.end(), same_idx_set.begin(),
                                              same_idx_set.end(), std::inserter(cross_id_set6, cross_id_set6.begin()));
//                        if (cross_id_set3.size() >= cross_id_set4.size()) {
//                            cross_id_set2 = cross_id_set3;
//                        } else {
//                            cross_id_set2 = cross_id_set4;
//                        }
                        /// contain the sets in the list
                        cross_id_set_list.emplace_back(cross_id_set3);
                        cross_id_set_list.emplace_back(cross_id_set4);
                        cross_id_set_list.emplace_back(cross_id_set5);
                        cross_id_set_list.emplace_back(cross_id_set6);
                        for (int i = 0; i < cross_id_set_list.size(); ++i) {
                            if (max_len < cross_id_set_list[i].size()) {
                                max_len = cross_id_set_list[i].size();
                                cross_id_set1 = cross_id_set_list[i];
                            } else if (sec_len < cross_id_set_list[i].size()) {
                                sec_len = cross_id_set_list[i].size();
                                cross_id_set2 = cross_id_set_list[i];
                            }
                        }
                        /// clean up the sets
                        cross_id_set3.clear();
                        cross_id_set4.clear();
                        cross_id_set5.clear();
                        cross_id_set6.clear();
                    }
                    next_iter++;
                }
            }
            /// here find out the different snp sites in above haps which should be removed
            if (cross_id_set1.size() > cross_id_set2.size()) {
                for (int i = 0; i < blockResMap.begin()->first.size(); ++i) {
                    if (cross_id_set1.count(i) != 0) {
                        pair->second.phased_het_idx.push_back(i);
                        if (blockResMap.begin()->first[i] == '1') {
                            pair->second.pseudo_hap1.push_back(1);
                            pair->second.pseudo_hap2.push_back(0);
                        } else {
                            pair->second.pseudo_hap1.push_back(0);
                            pair->second.pseudo_hap2.push_back(1);
                        }
                    }
                }
            } else {
                for (int i = 0; i < blockResMap.begin()->first.size(); ++i) {
                    if (cross_id_set2.count(i) != 0) {
                        pair->second.phased_het_idx.push_back(i);
                        if (blockResMap.begin()->first[i] == '1') {
                            pair->second.pseudo_hap1.push_back(1);
                            pair->second.pseudo_hap2.push_back(0);
                        } else {
                            pair->second.pseudo_hap1.push_back(0);
                            pair->second.pseudo_hap2.push_back(1);
                        }
                    }
                }
            }
            /// there must be at least one remained column after excluding, so, if the total excluded id is also the total snp id
            /// in python script, this means only find one pair of complementary haps, while here is only for prevention
            //todo: this might..maybe must be no use
            if (exclude_id_set.size() == blockResMap.begin()->first.size()) {
                exclude_id_set.clear();
            }
        }
    }
}

void HaploBitsArrays::CompressedHaploBitsT::phasingInBlockV2(std::map<uint, Block> &hapMap, int blockSize) {
/// for loop cross each block, find out the potential complementary haps
    int loop_id = 0;
    for (auto pair = hapMap.begin(); pair != hapMap.end(); pair++) {
        Block block = pair->second;
//        std::cout << "block id = " << block.blk_idx << std::endl;
        std::set<std::string> cur_hap_set;
        std::set<std::string> comp_hap_set;
        std::map<std::string, BlockResItem> blockCompCountMap;
        std::map<std::string, BlockResItem> blockResMap;
        //todo: add a new data-structure here to record the relationship between hap_seq and ref_hap_id
        std::map<std::string, CompHapRefIdItem> compHapRefIdMap;
        std::map<std::string, CompHapRefIdItem> compHapRefIdResMap;
        std::map<int, int> refIdDifHomCountMap;
        /// first look through the n best haps
        for (int n = 0; n < bestK.size(); ++n) {
            std::string cur_hap;
            std::string comp_hap;
            for (int i = 0; i < block.het_idx.size(); ++i) {
                uint het_id = block.het_idx[i];
                int bit = getBit(n, het_id + 1);
                cur_hap += std::to_string(bit);
                if (i != 0) {
                    if (refIdDifHomCountMap.count(bestK[n]) == 0) {
                        refIdDifHomCountMap.insert(
                                std::make_pair(bestK[n], haploBitsTWithHom[bestK[n]][2 * het_id + 1]));
                    } else {
                        refIdDifHomCountMap.find(bestK[n])->second += haploBitsTWithHom[bestK[n]][2 * het_id + 1];
                    }
                }
            }
            if (cur_hap_set.count(cur_hap) == 0) {
                cur_hap_set.insert(cur_hap);
            }
            if (blockCompCountMap.count(cur_hap) == 0) {
                /// storing the new hap seq in comp count map
                BlockResItem item = BlockResItem(1, 0);
                blockCompCountMap.insert(std::make_pair(cur_hap, item));
                /// storing the new hap and the ref_id in compHapRefIdMap
                CompHapRefIdItem compHapRefIdItem = CompHapRefIdItem();
                compHapRefIdItem.curRefIdSet.insert(bestK[n]);
                compHapRefIdMap.insert(std::make_pair(cur_hap, compHapRefIdItem));
            } else {
                blockCompCountMap.find(cur_hap)->second.cur_count++;
                compHapRefIdMap.find(cur_hap)->second.curRefIdSet.insert(bestK[n]);
            }
            comp_hap = find_comp(cur_hap);
            if (comp_hap_set.count(comp_hap) == 0) {
                comp_hap_set.insert(comp_hap);
            }
            if (comp_hap_set.count(cur_hap) != 0) {
                /// the comp hap for cur ref has been found before, therefore, we should only add the comp_count for the comp hap sequence
                if (blockCompCountMap.count(cur_hap) == 0 || blockCompCountMap.find(cur_hap)->second.comp_count == 0) {
                    if (blockCompCountMap.count(comp_hap) == 0) {
                        std::cout << "error occurs! Something wrong with in block comp hap" << std::endl;
                        std::cout << "block id = " << block.blk_idx << ", cur hap = " << cur_hap << ", comp hap = "
                                  << comp_hap << std::endl;
                        return;
                    }
                    blockCompCountMap.find(comp_hap)->second.comp_count++;
                    /// updating the result map with comp hap record
                    if (blockResMap.count(comp_hap) == 0) {
                        blockResMap.insert(std::make_pair(comp_hap, blockCompCountMap.find(comp_hap)->second));
                        compHapRefIdResMap.insert(std::make_pair(comp_hap, compHapRefIdMap.find(comp_hap)->second));
                        auto iterator = compHapRefIdMap.find(cur_hap);
                        compHapRefIdResMap.find(comp_hap)->second.compRefIdSet.insert(
                                iterator->second.curRefIdSet.begin(), iterator->second.curRefIdSet.end());
                    } else {
                        blockResMap.find(comp_hap)->second = blockCompCountMap.find(comp_hap)->second;
                        compHapRefIdResMap.find(comp_hap)->second.compRefIdSet.insert(bestK[n]);
                    }
                } else {
                    /// otherwise, cur hap sequence has been found before comp hap, therefore, we only need to add the cur_count on cur hap
                    if (blockResMap.count(cur_hap) == 0) {
                        blockResMap.insert(std::make_pair(cur_hap, blockCompCountMap.find(cur_hap)->second));
                        compHapRefIdResMap.insert(std::make_pair(cur_hap, compHapRefIdMap.find(cur_hap)->second));
                        auto iterator = compHapRefIdMap.find(comp_hap);
                        compHapRefIdResMap.find(cur_hap)->second.compRefIdSet.insert(
                                iterator->second.curRefIdSet.begin(), iterator->second.curRefIdSet.end());
                    } else {
                        blockResMap.find(cur_hap)->second = blockCompCountMap.find(cur_hap)->second;
                        compHapRefIdResMap.find(cur_hap)->second.curRefIdSet.insert(bestK[n]);
                    }
                }
            }
        }
        //todo: use the hom sites in the block, comparing cur target with different ref hap, this should be added into the multi comp pairs code block
        /// cur block contains no complementary haps
        if (blockResMap.size() == 0) {
            continue;
        }
        /// cur block only contains one complementary haps
        if (blockResMap.size() == 1) {
            if (blockResMap.begin()->first.size() != block.het_idx.size()) {
                std::cout << "error occurs!" << std::endl;
                std::cout << "hap size not equal to het idx size" << std::endl;
                std::cout << "block id = " << block.blk_idx << ", find comp hap = " << blockResMap.begin()->first
                          << ", find comp hap size = " << blockResMap.begin()->first.size() << ", het idx size = "
                          << block.het_idx.size() << std::endl;
                return;
            }
            std::string hap_res = blockResMap.begin()->first;
            pair->second.phased_het_idx.clear();
            pair->second.pseudo_hap1.clear();
            pair->second.pseudo_hap2.clear();
            for (int i = 0; i < block.het_idx.size(); ++i) {
                pair->second.phased_het_idx.emplace_back(i);
                if (hap_res[i] == '1') {
                    pair->second.pseudo_hap1.emplace_back(1);
                    pair->second.pseudo_hap2.emplace_back(0);
                } else {
                    pair->second.pseudo_hap1.emplace_back(0);
                    pair->second.pseudo_hap2.emplace_back(1);
                }
            }
        } else {
            /// reduced the multi comp pairs by finding out the min dif hom sites count and the related comp pair
            std::pair<int, int> minDifHomCountPair = std::make_pair(INT_MAX, INT_MAX);
            std::pair<int, int> minDifRefIdPair = std::make_pair(-1, -1);
            int maxSupportHapCount = 0;
            std::string minDifHap;
            for (const auto &item : compHapRefIdResMap) {
                std::pair<int, int> curMinDifCountPair = std::make_pair(INT_MAX, INT_MAX);
                std::pair<int, int> curMinDifRefIdPair = std::make_pair(-1, -1);
                int curMaxSupportHapCount = 0;
                for (auto ref_id : item.second.curRefIdSet) {
                    if (refIdDifHomCountMap[ref_id] < curMinDifCountPair.first) {
                        curMinDifCountPair.first = refIdDifHomCountMap[ref_id];
                        curMinDifRefIdPair.first = ref_id;
                    }
                }
                for (auto ref_id : item.second.compRefIdSet) {
                    if (refIdDifHomCountMap[ref_id] < curMinDifCountPair.second) {
                        curMinDifCountPair.second = refIdDifHomCountMap[ref_id];
                        curMinDifRefIdPair.second = ref_id;
                    }
                }
                if ((curMinDifCountPair.first < minDifHomCountPair.first &&
                     curMinDifCountPair.second < minDifHomCountPair.second) ||
                    (curMinDifCountPair.first + curMinDifCountPair.second) <
                    (minDifHomCountPair.first + minDifHomCountPair.second)) {
                    minDifHomCountPair = curMinDifCountPair;
                    minDifRefIdPair = curMinDifRefIdPair;
                    minDifHap = item.first;
                }
            }
            /// the related comp pair is stored in minDifHap, and update the phasing info by rearrange the snp sequence according to minDifHap
            std::cout << "min dif hom count = " << minDifHomCountPair.first << "," << minDifHomCountPair.second
                      << std::endl;
            std::cout << "min dif ref ids = " << minDifRefIdPair.first << "," << minDifRefIdPair.second << std::endl;
            std::cout << "supporting cur hap count = " << blockResMap.find(minDifHap)->second.cur_count
                      << ", supporting comp hap count = " << blockResMap.find(minDifHap)->second.comp_count
                      << std::endl;
            std::cout << "min dif haplotype seq = " << minDifHap << std::endl;
            pair->second.phased_het_idx.clear();
            pair->second.pseudo_hap1.clear();
            pair->second.pseudo_hap2.clear();
            for (int i = 0; i < block.het_idx.size(); ++i) {
                pair->second.phased_het_idx.emplace_back(i);
                if (minDifHap[i] == '1') {
                    pair->second.pseudo_hap1.emplace_back(1);
                    pair->second.pseudo_hap2.emplace_back(0);
                } else {
                    pair->second.pseudo_hap1.emplace_back(0);
                    pair->second.pseudo_hap2.emplace_back(1);
                }
            }
        }
        loop_id++;
    }
}

uint64 HaploBitsArrays::CompressedHaploBitsT::getNSites() const {
    return NSites;
}

std::map<uint, Block>
HaploBitsArrays::CompressedHaploBitsT::generateCompHapMapBySize(std::map<uint, Block> &hapMap, int blockSize) {
    return std::map<uint, Block>();
}

const std::vector<std::string> &HaploBitsArrays::CompressedHaploBitsT::getAllHaps() const {
    return all_haps;
}

void HaploBitsArrays::CompressedHaploBitsT::setAllHaps(const std::vector<std::string> &allHaps) {
    all_haps = allHaps;
}

const std::vector<std::string> &HaploBitsArrays::CompressedHaploBitsT::getAllHapsWithCompressedHom() const {
    return all_haps_with_compressed_hom;
}

void HaploBitsArrays::CompressedHaploBitsT::setAllHapsWithCompressedHom(
        const std::vector<std::string> &allHapsWithCompressedHom) {
    all_haps_with_compressed_hom = allHapsWithCompressedHom;
}
