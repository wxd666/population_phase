#include "util.h"
#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

void logging(std::ostream &stream, const std::string &message) {
    std::time_t t = std::time(nullptr);

    stream << "[SpecHap " << std::put_time(std::localtime(&t), "%Y:%m:%d %H:%M:%S]") << message << std::endl;
}

void runTimeLogging(std::ofstream &stream, clock_t &start_t, clock_t &end_t, const std::string &message) {
    std::time_t t = std::time(nullptr);
    end_t = clock();
    stream << "[SpecHap " << std::put_time(std::localtime(&t), "%Y:%m:%d %H:%M:%S]") << message << std::endl;
    stream << "[SpecHap] " << message << ", run time = " << double(end_t - start_t) / CLOCKS_PER_SEC << "s"
           << std::endl;
    start_t = clock();
}
