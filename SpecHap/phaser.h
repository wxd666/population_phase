//
// Created by yyh on 3/4/2019.
//

#ifndef SPECHAP_PHASER_H
#define SPECHAP_PHASER_H

#include "type.h"
#include "vcf_io.h"
#include "frag_io.h"
#include "optionparser.h"
#include "util.h"
#include "haploBitsArrays.h"
#include "block.h"
#include <ctime>
#include "vcfHandler.h"


extern int WINDOW_SIZE;
extern int WINDOW_OVERLAP;
extern int MAX_BARCODE_SPANNING;
extern bool HYBRID;
extern bool KEEP_PS;
extern int MAX_HIC_INSERTION;
extern int OPERATION;
extern int RECURSIVE_LIMIT;
extern std::string C_BLOCK;
extern int ITER_NUM;
extern int DECREMENT_1_HOM_DIF;
extern int DECREMENT_2_HOM_DIF;
extern int SUPPORT_COUNT_SCALE;

class Phaser {
public:
    Phaser() = default;

    explicit Phaser(const std::string &ref, const std::string &target, const std::string &fnout);

    explicit Phaser(const std::string &ref, const std::string &target, const std::string &fnout, int &chrom,
                    const std::string &geneticMapFile, const std::string &tmpFile, const std::string &writeMode,
                    double cMmax);

    explicit Phaser(const std::string &vcfFile, const std::string &fnout, int &chrom, const std::string &geneticMapFile,
                    const std::string &tmpFile, const std::string &writeMode, double cMmax);

    ~Phaser();

    void phasing();

    void phasing_cohort();

    uint getGenoType0123(uint64 m64j, uint64 n) const;

    std::vector<uint> findBestHaps(uint n0, uint K, bool useTargetHaps);

    std::vector<uint> findLocalBestHaps(uint n0, uint K, bool useTargetHaps, int startPos, int endPos);

    HaploBitsArrays::CompressedHaploBitsT *constructHapBitsT(uint64 n0, uint K, bool useTargetHaps);

    HaploBitsArrays::CompressedHaploBitsT *
    constructLocalHapBitsT(uint64 n0, uint K, bool useTargetHaps, int startPos, int endPos);

    void set_blk(const char *contig, ChromoPhaser *chromo_phaser, VCFReader &frvcf);

    void set_het(bcf1_t *record, ptr_ResultforSingleVariant resultforSingleVariant, const unsigned int blk_no);

    void get_blk();

    void link_blk();

    void correct_hap();

    void check_phased();

    void resize_blk_id_map();

    void recursive_set_hap_map(HaploBitsArrays::CompressedHaploBitsT &hapBitsT, std::map<uint, Block> &empty_blks);

    void get_empty_phased_blks();

    static void
    get_empty_split_blks(std::map<uint, Block> &partial_phased_blks, std::map<uint, Block> &tmp_empty_hap_map);

    void split_blk(std::map<uint, Block> &tmp_empty_hap_map);

    void add_phased_to_hap_map(std::map<uint, Block> &partial_phased_blks);

    void initialize_hap_map_list();

    void set_block_in_hapMap_by_size(std::map<uint, Block> &hapMap, uint het_idx, uint het_pos, uint snp_idx,
                                     int block_size);

    std::string find_comp(std::string in_s);

    int load_contig_blocks_cohort(uint &nt);

//    void set_blk_v2(const char *contig, ChromoPhaser *chromo_phaser, VCFReader &frvcf);
    int *gt;


private:
    void sort_frag_file(std::string file_name);

    double threshold;
    VCFReader *tarVcf;
    VCFReader *validVcf;
    VCFWriter *fwvcf;
    FragmentReader *frfrag;
    BEDReader *frbed;
    Spectral *spectral;
    VcfHandler *vcfHandler;
    int coverage;
    uint cur_tgt_idx;
    HaploBitsArrays::HaploBitsT *haploBitsT;
    std::vector<bool> hapsRef;     // M*2*Nref
    std::vector<std::map<uint, std::vector<uint>>> block_id_map;
    std::vector<std::map<uint, Block>> block_res;
    std::map<uint, Block> hap_map;
    std::map<uint, Block> transed_hap_map;
    std::map<uint, Block> phased_hap_map;
    std::map<uint, Block> empty_hap_map;
    std::map<uint, Block> test_hap_map;
    std::map<uint, Block> hap_map_4;
    std::map<uint, Block> hap_map_8;
    std::map<uint, Block> hap_map_16;
    std::map<uint, Block> hap_map_32;
    std::ofstream runTime_log;
    bool recursive_complete_empty_blk_flag;
    // 1: reference panel phase, 2: cohort phase
    int refCohortFlag;

    void phasing_by_chrom(uint var_count, ChromoPhaser *chromo_phaser);

    void phase_HiC_recursive(ChromoPhaser *chromo_phaser, std::set<uint> &connected_comp);

    void phase_HiC_poss(ChromoPhaser *chromo_phaser);

    void recursion();

    void recursion_v2();

    void transform_hapmap(int size);

    void transform_hapmap_v2();

    void phase_hap_map(bool isCohort, uint nt);
    void phase_hap_map_v2(bool isCohort, uint nt);
    static void phase_pipeline(std::map<uint, Block> &hap_map_n, std::vector<std::string> &best_haps,
                               std::vector<std::string> &all_haps);

    void phase_pipeline_v2(std::map<uint, Block> &hap_map_n, std::vector<std::string> &best_haps,
                               std::vector<std::string> &all_haps, std::vector<std::string> &compress_hom_haps, std::vector<std::string> &best_hom_haps, std::vector<uint> &bestk_ids);

    int load_contig_records(ChromoPhaser *chromo_phaser);

    int load_contig_blocks(ChromoPhaser *chromo_phaser);

    static std::string comp(std::string hap);
    static std::map<uint, std::string> get_local_ref_map(uint start, uint end, std::vector<std::string> &haps);

//    static std::map<uint, std::string> get_local_ref_map_v2(uint start, uint end, std::vector<std::string> &haps, std::vector<std::string> &best_hom_haps);
    static std::map<uint, std::string> get_local_ref_map_v2(uint start, uint end, std::vector<std::string> &haps, std::vector<std::string> &best_hom_haps, std::vector<uint> &ids);

    static std::map<uint, std::string> get_local_ref_map_v3(uint start, uint end, std::vector<std::string> &haps, std::vector<std::string> &best_hom_haps, std::vector<uint> &ids, std::map<uint, uint> &ref_id_hom_dif_map);

    static bool check_hom(std::string &local_seq, uint index, std::string &homo_hap);

    static int count_hom_dif(std::string &local_seq, uint index, std::string &homo_hap);

    static void sort_comp_res(std::vector<std::pair<uint, CurComp>> &cur_comp_res);

    static void
    first_phase_iter(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos);

    static void
    second_phase_iter(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos);

    static std::map<uint, std::string>
    get_shielded_local_ref_map(uint shield_pos, uint start, uint end, std::vector<std::string> &haps);

    static std::vector<std::string> get_local_refs(std::map<uint, std::string> &local_ref_map);

    static std::set<std::string> unique_haps(std::vector<std::string> &haps);

    static std::vector<uint> get_ids(std::string hap, std::vector<std::string> &refs);

    static std::vector<uint> convert_str_2_vec(std::string &string);

    std::vector<std::string> get_best_haps_map(std::vector<std::string> &all_haps);

    std::string vec_to_string(std::vector<uint> vec);
    std::map<uint, std::string>
    get_shielded_local_ref_map_v2(uint shield_pos, uint start, uint end, std::vector<std::string> &haps, std::map<uint, std::string> &local_ref_map, std::vector<uint> ids);

    void first_phase_iter_v2(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos, std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids, std::vector<std::string> &all_haps);

    void first_phase_iter_v3(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos, std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids, std::vector<std::string> &all_haps, std::map<uint, uint> &ref_id_dif_count_map);

    void second_phase_iter_v2(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos, std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids, std::vector<std::string> &all_haps);

    void second_phase_iter_v3(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos, std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids, std::vector<std::string> &all_haps, std::map<uint, uint> &ref_id_dif_count_map);

    static std::vector<uint> get_hap_ids(std::string hap, std::map<uint, std::string> &haps_dict);

    static int decrease_support_count(std::map<uint, uint> &ref_id_dif_count_map, uint cur_ref_id);

    template<typename Cont, typename Pred>
    inline Cont filter(const Cont &container, Pred predicate) {
        Cont result;
        std::copy_if(container.begin(), container.end(), std::back_inserter(result), predicate);
        return result;
    }

};

#endif //SPECHAP_PHASER_H
