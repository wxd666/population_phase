//
// Created by yangshuo on 5/7/2021.
//

#include "mapHandler.h"
#include "mapInterpolater.h"

std::vector<double> MapHandler::parse_map(std::vector<std::pair<int, int>> &chrBps,
                                          const std::string &geneticMapFile) {
    std::cout << "Filling in genetic map coordinates using reference file:" << std::endl;
    std::cout << "  " << geneticMapFile << std::endl;
    Genetics::MapInterpolater mapInterpolater(geneticMapFile);
    std::vector<double> cMs(chrBps.size());
    for (uint64 m = 0; m < chrBps.size(); m++)
        cMs[m] = 100 * mapInterpolater.interp(chrBps[m].first, chrBps[m].second);
    std::cout << "success" << std::endl;
    return cMs;
}

MapHandler::MapHandler() {
//
}

MapHandler::~MapHandler() {
    //
}