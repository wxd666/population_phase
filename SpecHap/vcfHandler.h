//
// Created by yangshuo on 5/7/2021.
//

#ifndef SPECHAP_VCFHANDLER_H
#define SPECHAP_VCFHANDLER_H

#include <vector>
#include <string>
#include <boost/utility.hpp>
#include "type.h"
#include <iostream>

class VcfHandler : boost::noncopyable {
public:
    uint64 Nref, Ntarget, M; // N = Nref + Ntarget
    uint64 Mseg64; // number of <=64-SNP chunks
    std::vector<std::vector<double>> seg64cMvecs;
    std::vector<std::string> targetIDs;
    std::string cur_chr;

    // perform synced read
    std::vector<bool> hapsRef;     // M*2*Nref
    std::vector<uchar> genosTarget; // M*Ntarget
    std::vector<double> cMs;

    // 1: reference panel phase, 2: cohort phase
    int refCohortFlag;

    // for store het and snp position
    std::map<uint, std::vector<uint>> snp_pos_map;
    std::map<uint, std::vector<uint>> het_pos_map;


    std::vector<std::pair<int, int>> parse_vcf
            (const std::string &vcfRef, const std::string &vcfTarget, int &chrom,
             std::vector<bool> &hapsRef, std::vector<uchar> &genosTarget, const std::string &tmpFile,
             const std::string &writeMode);

    std::vector<std::pair<int, int>>
    initVcf(const std::string &vcfFile, int inputChrom, int chromX, const std::string &tmpFile,
            const std::string &writeMode);


    VcfHandler(const std::string &vcfRef, const std::string &vcfTarget, int &chrom,
               const std::string &geneticMapFile, const std::string &tmpFile, const std::string &writeMode);

    VcfHandler(const std::string &vcfFile, int inputChrom, int chromX, const std::string &geneticMapFile,
               const std::string &tmpFile, const std::string &writeMode);

    VcfHandler();

    ~VcfHandler();

    uint64 getNref(void) const;

    uint64 getNtarget(void) const;

    const std::string &getTargetID(int n) const;
};

#endif //SPECHAP_VCFHANDLER_H
