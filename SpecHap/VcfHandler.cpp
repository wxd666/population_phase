//
// Created by wangxuedong on 5/18/21.
//
#include "vcfHandler.h"
#include <iostream>
#include <map>
#include <algorithm>
#include <cstring>

#include <htslib/thread_pool.h>
#include <htslib/vcf.h>
#include <htslib/synced_bcf_reader.h>
#include <fstream>

#include "omp.h"
#include "mapHandler.h"


void process_ref_genotypes(int nsmpl, int ngt, int32_t *gt, bool allowHaploid,
                           std::vector<bool> &hapsRef, int &numMissing, int &numUnphased, uint &w) {
    numMissing = numUnphased = 0;
    if (ngt != 2 * nsmpl && !(ngt == nsmpl && allowHaploid)) {
        std::cerr << "ERROR: ref ploidy != 2 (ngt != 2*nsmpl): ngt="
                  << ngt << ", nsmpl=" << nsmpl << std::endl;
        exit(1);
    }
    int ploidy = ngt / nsmpl;
    for (int i = 0; i < nsmpl; i++) {
        int32_t *ptr = gt + i * ploidy;
        bool haps[2];
        bool missing = false, unphased = false;
        for (int j = 0; j < ploidy; j++) {
            if (ptr[j] == bcf_int32_vector_end) {
                if (j == 0) {
                    std::cerr << "ERROR: ptr[0]==bcf_int32_vector_end... zero ploidy?" << std::endl;
                    exit(1);
                } else { // 2nd of ploidy==2 genotypes is set to bcf_int32_vector_end => haploid
                    if (missing) continue;  // missing diploid genotype can be written in VCF as "."
                    else if (allowHaploid) { // X chromosome => haploid ok
                        haps[j] = haps[j - 1]; // encode as diploid homozygote
                        unphased = false;
                    } else {
                        std::cerr << "ERROR: ref genotypes contain haploid sample" << std::endl;
                        exit(1);
                    }
                }
            } else {
                if (bcf_gt_is_missing(ptr[j])) { // missing allele
                    missing = true;
                } else {
                    int idx = bcf_gt_allele(ptr[j]); // allele index
                    haps[j] = (idx >= 1); // encode REF allele -> 0, ALT allele(s) -> 1
                    if (j == 1 && !bcf_gt_is_phased(ptr[j])) unphased = true;
                }
            }
        }
        if (ploidy == 1) haps[1] = haps[0];
        if (missing) {
            haps[0] = haps[1] = 0; // set both alleles to REF allele
            numMissing++;
        } else if (unphased) {
//            if (haps[0] != haps[1] && ((w = 18000 * (w & 65535) + (w >> 16)) & 1))
//                std::swap(haps[0], haps[1]); // randomize phasing
            numUnphased++;
        }
        hapsRef.push_back(haps[0]);
        hapsRef.push_back(haps[1]);
    }
}

void process_target_genotypes(int nsmpl, int ngt, int32_t *gt, bool allowHaploid,
                              std::vector<uchar> &genosTarget, int &numMissing) {
    numMissing = 0;
    if (ngt != 2 * nsmpl && !(ngt == nsmpl && allowHaploid)) {
        std::cerr << "ERROR: target ploidy != 2 (ngt != 2*nsmpl): ngt="
                  << ngt << ", nsmpl=" << nsmpl << std::endl;
        exit(1);
    }
    int ploidy = ngt / nsmpl;
    for (int i = 0; i < nsmpl; i++) {
        int32_t *ptr = gt + i * ploidy;
        bool missing = false;
        uchar g = 0;
        for (int j = 0; j < ploidy; j++) {
            if (ptr[j] == bcf_int32_vector_end) {
                if (j == 0) {
                    std::cerr << "ERROR: ptr[0]==bcf_int32_vector_end... zero ploidy?" << std::endl;
                    exit(1);
                } else { // 2nd of ploidy==2 genotypes is set to bcf_int32_vector_end => haploid
                    if (missing) continue;  // missing diploid genotype can be written in VCF as "."
                    else if (allowHaploid) // X chromosome => haploid ok
                        g *= 2; // encode as diploid homozygote
                    else {
                        std::cerr << "ERROR: target genotypes contain haploid sample" << std::endl;
                        exit(1);
                    }
                }
            } else {
                if (bcf_gt_is_missing(ptr[j])) { // missing allele
                    missing = true;
                } else {
                    int idx = bcf_gt_allele(ptr[j]); // allele index
                    if (idx > 1) {
                        std::cerr << "ERROR: multi-allelic site found in target; should have been filtered"
                                  << std::endl;
                        exit(1);
                    }
                    g += idx;
                }
            }
        }
        if (missing) {
            g = 9;
            numMissing++;
        } else if (ploidy == 1) g *= 2;   // encode as diploid homozygote
        genosTarget.push_back(g);
    }
}

void process_ref_genotypes_cohort(int nsmpl, int ngt, int32_t *gt, bool allowHaploid,
                           std::vector<bool> &hapsRef, int &numMissing, int &numUnphased, uint &w, std::map<uint, std::vector<uint>> &snp_pos_map, std::map<uint, std::vector<uint>> &het_pos_map, bcf1_t *rec) {
    numMissing = numUnphased = 0;
    if (ngt != 2 * nsmpl && !(ngt == nsmpl && allowHaploid)) {
        std::cerr << "ERROR: ref ploidy != 2 (ngt != 2*nsmpl): ngt="
                  << ngt << ", nsmpl=" << nsmpl << std::endl;
        exit(1);
    }
    int ploidy = ngt / nsmpl;
    for (int i = 0; i < nsmpl; i++) {
        int32_t *ptr = gt + i * ploidy;
        bool haps[2];
        bool missing = false, unphased = false;
        for (int j = 0; j < ploidy; j++) {
            if (ptr[j] == bcf_int32_vector_end) {
                if (j == 0) {
                    std::cerr << "ERROR: ptr[0]==bcf_int32_vector_end... zero ploidy?" << std::endl;
                    exit(1);
                } else { // 2nd of ploidy==2 genotypes is set to bcf_int32_vector_end => haploid
                    if (missing) continue;  // missing diploid genotype can be written in VCF as "."
                    else if (allowHaploid) { // X chromosome => haploid ok
                        haps[j] = haps[j - 1]; // encode as diploid homozygote
                        unphased = false;
                    } else {
                        std::cerr << "ERROR: ref genotypes contain haploid sample" << std::endl;
                        exit(1);
                    }
                }
            } else {
                if (bcf_gt_is_missing(ptr[j])) { // missing allele
                    missing = true;
                } else {
                    int idx = bcf_gt_allele(ptr[j]); // allele index
                    haps[j] = (idx >= 1); // encode REF allele -> 0, ALT allele(s) -> 1
                    if (j == 1 && !bcf_gt_is_phased(ptr[j])) unphased = true;
                }
            }
        }
        if (ploidy == 1) haps[1] = haps[0];
        if (missing) {
            haps[0] = haps[1] = 0; // set both alleles to REF allele
            numMissing++;
        } else if (unphased) {
//            if (haps[0] != haps[1] && ((w = 18000 * (w & 65535) + (w >> 16)) & 1))
//                std::swap(haps[0], haps[1]); // randomize phasing
            numUnphased++;
        }
        if(haps[0]!=haps[1]){
            het_pos_map[i].emplace_back(rec->pos);
        }
        snp_pos_map[i].emplace_back(rec->pos);
        hapsRef.push_back(haps[0]);
        hapsRef.push_back(haps[1]);
    }

}

void process_target_genotypes_cohort(int nsmpl, int ngt, int32_t *gt, bool allowHaploid,
                              std::vector<uchar> &genosTarget, int &numMissing) {
    numMissing = 0;
    if (ngt != 2 * nsmpl && !(ngt == nsmpl && allowHaploid)) {
        std::cerr << "ERROR: target ploidy != 2 (ngt != 2*nsmpl): ngt="
                  << ngt << ", nsmpl=" << nsmpl << std::endl;
        exit(1);
    }
    int ploidy = ngt / nsmpl;
    for (int i = 0; i < nsmpl; i++) {
        int32_t *ptr = gt + i * ploidy;
        bool missing = false;
        uchar g = 0;
        for (int j = 0; j < ploidy; j++) {
            if (ptr[j] == bcf_int32_vector_end) {
                if (j == 0) {
                    std::cerr << "ERROR: ptr[0]==bcf_int32_vector_end... zero ploidy?" << std::endl;
                    exit(1);
                } else { // 2nd of ploidy==2 genotypes is set to bcf_int32_vector_end => haploid
                    if (missing) continue;  // missing diploid genotype can be written in VCF as "."
                    else if (allowHaploid) // X chromosome => haploid ok
                        g *= 2; // encode as diploid homozygote
                    else {
                        std::cerr << "ERROR: target genotypes contain haploid sample" << std::endl;
                        exit(1);
                    }
                }
            } else {
                if (bcf_gt_is_missing(ptr[j])) { // missing allele
                    missing = true;
                } else {
                    int idx = bcf_gt_allele(ptr[j]); // allele index
                    if (idx > 1) {
                        std::cerr << "ERROR: multi-allelic site found in target; should have been filtered"
                                  << std::endl;
                        exit(1);
                    }
                    g += idx;
                }
            }
        }
        if (missing) {
            g = 9;
            numMissing++;
        } else if (ploidy == 1) g *= 2;   // encode as diploid homozygote

        genosTarget.push_back(g);
    }


}


// fills in chrom if chrom==0

std::vector<std::pair<int, int> > VcfHandler::parse_vcf
        (const std::string &vcfRef, const std::string &vcfTarget, int &chrom, std::vector<bool> &hapsRef,
         std::vector<uchar> &genosTarget, const std::string &tmpFile,
         const std::string &writeMode) {

    std::vector<std::pair<int, int> > chrBps;
    const int chromX = 23;
    std::cout << chrom << std::endl;
//    bcf_srs_t *sr1 = bcf_sr_init();
//    bcf_sr_set_opt(sr1, BCF_SR_PAIR_LOGIC, BCF_SR_PAIR_BOTH_REF);
//    bcf_sr_set_opt(sr1, BCF_SR_REQUIRE_IDX);
//    for (int i=0; i<1; i++)
//        bcf_sr_add_reader(sr1, vcfRef.c_str());
//    while ( bcf_sr_next_line(sr1) )
//    {
//        for (int i=0; i<1; i++)
//        {
//            bcf1_t *line = bcf_sr_get_line(sr1,i);
//
//        }
//    }
//    if ( sr1->errnum ) std::cerr << "Error: %s\n", bcf_sr_strerror(sr1->errnum);
//    bcf_sr_destroy(sr1);


    bcf_srs_t *sr = bcf_sr_init();
    bcf_sr_set_opt(sr, BCF_SR_PAIR_LOGIC, BCF_SR_PAIR_EXACT);
    bcf_sr_set_opt(sr, BCF_SR_REQUIRE_IDX);


//    if ( chrom!=0 )
//    {
//        kstring_t str = {0,0,0};
//        if ( chrom==chromX )
//        {
//            // this is not perfect, better would be to have the chr name passed to us
//            ksprintf(&str,"X:%d-%d,chrX:%d-%d",(uint32_t)22,(uint32_t)22,(uint32_t)22,(uint32_t)22);
//        }
//        else
//            ksprintf(&str,"%d:%d-%d,chr%d:%d-%d",chrom,(uint32_t)0,(uint32_t)33,chrom,(uint32_t)33,(uint32_t)33);
//
//        if ( bcf_sr_set_regions(sr, str.s, 0)!=0 )
//        {
//            std::cerr << "ERROR: failed to initialize the region:" << str.s;
//            exit(1);
//        }
//        free(str.s);
//    }

    if (!bcf_sr_add_reader(sr, vcfRef.c_str())) {
        std::cerr << "ERROR: Could not open " << vcfRef << " for reading: " << bcf_sr_strerror(sr->errnum)
                  << std::endl;
        exit(1);
    }
    if (!bcf_sr_add_reader(sr, vcfTarget.c_str())) {
        std::cerr << "ERROR: Could not open " << vcfTarget << " for reading: "
                  << bcf_sr_strerror(sr->errnum) << std::endl;
        exit(1);
    }

//    bcf_sr_add_reader(sr, vcfRef.c_str());
//    bcf_sr_add_reader(sr, vcfTarget.c_str());

    bcf_hdr_t *ref_hdr = bcf_sr_get_header(sr, 0);
    bcf_hdr_t *tgt_hdr = bcf_sr_get_header(sr, 1);

    // Open VCF for writing, "-" stands for standard output
    //      wbu .. uncompressed BCF
    //      wb  .. compressed BCF
    //      wz  .. compressed VCF
    //      w   .. uncompressed VCF
    htsFile *out = hts_open(tmpFile.c_str(), writeMode.c_str());
    htsThreadPool p = {hts_tpool_init(omp_get_max_threads()),
                       0};
    hts_set_thread_pool(out, &p);
    // Print the VCF header
    bcf_hdr_write(out, tgt_hdr);

    Nref = bcf_hdr_nsamples(ref_hdr);
    Ntarget = bcf_hdr_nsamples(tgt_hdr);
    std::map<int, int> bpToSyncedIndex1; // bp -> 1-based index (m+1)

    // Read target sample IDs
    targetIDs.resize(Ntarget);
    for (uint i = 0; i < Ntarget; i++)
        targetIDs[i] = tgt_hdr->samples[i];

    std::cout << std::endl;
    std::cout << "Reference samples: Nref = " << Nref << std::endl;
    std::cout << "Target samples: Ntarget = " << Ntarget << std::endl;

    M = 0;
    uint Mexclude = 0, MtargetOnly = 0, MrefOnly = 0, MmultiAllelicTgt = 0,
            MmultiAllelicRef = 0, MmonomorphicRef = 0;
    uint MwithMissingRef = 0, MwithUnphasedRef = 0, MnotInRegion = 0, MnotOnChrom = 0;
    uint MrefAltError = 0, numRefAltSwaps = 0;
    uint64 GmissingRef = 0, GunphasedRef = 0, GmissingTarget = 0;
    uint w = 521288629; // fast rng: Marsaglia's MWC

    int mref_gt = 0, *ref_gt = NULL;
    int mtgt_gt = 0, *tgt_gt = NULL;
    int mtgt_ps = 0, *tgt_ps = NULL;
    int Mps = 0;
    uint64 err_ps = 0, good_ps = 0;
    int prev_rid = -1; // chromosome BCF id and human-readable numeric id
    while (bcf_sr_next_line(sr)) {
        bcf1_t *ref = bcf_sr_get_line(sr, 0);
        bcf1_t *tgt = bcf_sr_get_line(sr, 1);
//
        if (!ref) {
            //fprintf(stderr, "onlyT .. %s:%d\n", bcf_seqname(tgt_hdr, tgt), tgt->pos+1);
            bcf_unpack(tgt, BCF_UN_STR); // unpack thru ALT
            if (tgt->n_allele > 1 && strcmp(tgt->d.allele[1], ".") != 0) // report if polymorphic in target
                MtargetOnly++;
            continue;
        }
        if (!tgt) {
            //fprintf(stderr, "onlyR .. %s:%d\n", bcf_seqname(ref_hdr, ref), ref->pos+1);
            bcf_unpack(ref, BCF_UN_STR); // unpack thru ALT
            if (ref->n_allele > 1 && strcmp(ref->d.allele[1], ".") != 0) // report if polymorphic in ref
                MrefOnly++;
            continue;
        }
        //fprintf(stderr, "match .. %s:%d\n", bcf_seqname(ref_hdr, ref), ref->pos+1);

        // deal with multi-allelic and monomorphic markers
        // drop multi-allelic target markers
        int ntgt_gt = bcf_get_genotypes(tgt_hdr, tgt, &tgt_gt, &mtgt_gt);
        if (tgt->n_allele > 2) {
            MmultiAllelicTgt++;
            continue;
        }
        // drop monomorphic reference markers
        if (ref->n_allele == 1) {
            MmonomorphicRef++;
            continue;
        }
        // preserve monomorphic markers if biallelic in the reference panel
        if (tgt->n_allele < 2) {
            bcf_update_alleles(tgt_hdr, tgt, (const char **) ref->d.allele, ref->n_allele);
        }
        // drop multi-allelic reference markers
        if (ref->n_allele > 2) {
            MmultiAllelicRef++;
            //        if (outputUnphased) { bcf_write(out, tgt_hdr, tgt); isTmpPhased.push_back(false); }
            continue;
        }

        // check for REF/ALT swaps
        bool refAltSwap = false;
        if (strcmp(tgt->d.allele[0], ref->d.allele[0]) == 0 &&
            strcmp(tgt->d.allele[1], ref->d.allele[1]) == 0) {
            refAltSwap = false;
        }
            //    else if (allowRefAltSwap && bcf_is_snp(tgt) && // allow swap if --allowRefAltSwap is set
            //        strcmp(tgt->d.allele[0], ref->d.allele[1]) == 0 &&
            //        strcmp(tgt->d.allele[1], ref->d.allele[0]) == 0) {
            //        refAltSwap = true;
            //        numRefAltSwaps++;
            //    }
        else {
            MrefAltError++;
            //        if (outputUnphased) { bcf_write(out, tgt_hdr, tgt); isTmpPhased.push_back(false); }
            continue;
        }


        // Check the chromosome: if region was requested (chrom is set), synced
        // reader already positioned us in the right region. Otherwise, we process
        // only the first chromosome in the file and quit
        if (prev_rid < 0) {
            prev_rid = tgt->rid;
            if (!chrom) // learn the human-readable id
            {
                std::cout << chrom << std::endl;
//                chrom = 22;
//                chrom = StringUtils::bcfNameToChrom(bcf_hdr_id2name(tgt_hdr, tgt->rid), 1, 0);
            }

        }
        if (prev_rid != tgt->rid) break;

        M++; // SNP passes checks
        bpToSyncedIndex1[tgt->pos + 1] = M; // TODO: be careful about duplicate bp (multiallelics?)

        // append chromosome number and base std::pair coordinate to chrBps
        chrBps.push_back(std::make_pair(chrom, tgt->pos + 1));

        // process reference haplotypes: append 2*Nref entries (0/1 std::pairs) to hapsRef[]
        // check for missing/unphased ref genos (missing -> REF allele; unphased -> random phase)
        int nref_gt = bcf_get_genotypes(ref_hdr, ref, &ref_gt, &mref_gt);
        int numMissing, numUnphased;


        process_ref_genotypes(Nref, nref_gt, ref_gt, chrom == chromX,
                              hapsRef, numMissing, numUnphased, w);

        if (numMissing) MwithMissingRef++;
        if (numUnphased) MwithUnphasedRef++;
        GmissingRef += numMissing;
        GunphasedRef += numUnphased;

        // process target genotypes: append Ntarget entries (0/1/2/9) to genosTarget[]
        process_target_genotypes(Ntarget, ntgt_gt, tgt_gt, chrom == chromX, genosTarget, numMissing);
        GmissingTarget += numMissing;


        // print the record
        bcf_write(out, tgt_hdr, tgt);
        //    isTmpPhased.push_back(true);
    }

    bcf_sr_destroy(sr);
    hts_close(out);
    hts_tpool_destroy(p.pool);
    free(ref_gt);
    free(tgt_gt);

    std::cout << "SNPs to analyze: M = " << M << " SNPs in both target and reference" << std::endl;
    if (Mps) {
        std::cout << "                     " << Mps << " SNPs with FORMAT:PS field" << std::endl;
        std::cout << good_ps << " usable FORMAT:PS constraints" << std::endl;
        std::cout << err_ps << " unusable FORMAT:PS constraints" << std::endl;
    }
    if (numRefAltSwaps)
        std::cerr << "--> WARNING: REF/ALT were swapped in " << numRefAltSwaps << " of these SNPs <--"
                  << std::endl;
    std::cout << std::endl;
    std::cout << "SNPs ignored: " << MtargetOnly << " SNPs in target but not reference" << std::endl;
    if (MtargetOnly > M / 10U)
        std::cerr << "              --> WARNING: Check REF/ALT agreement between target and ref? <--"
                  << std::endl;
    std::cout << "              " << MrefOnly << " SNPs in reference but not target" << std::endl;
    if (MnotOnChrom)
        std::cout << "              " << MnotOnChrom << " SNPs not in specified chrom" << std::endl;
    if (MnotInRegion)
        std::cout << "              " << MnotInRegion << " SNPs not in selected region (+ flanks)"
                  << std::endl;
    std::cout << "              " << MmultiAllelicTgt << " multi-allelic SNPs in target" << std::endl;
    if (MmultiAllelicRef)
        std::cout << "              " << MmultiAllelicRef << " SNPs biallelic in target but multi-allelic in ref"
                  << std::endl;
    if (MmonomorphicRef)
        std::cout << "              " << MmonomorphicRef << " SNPs biallelic in target but monomorphic in ref"
                  << std::endl;
    if (MrefAltError)
        std::cout << "              " << MrefAltError << " SNPs with allele mismatches" << std::endl;
    if (Mexclude)
        std::cout << "              " << Mexclude << " SNPs excluded based on --vcfExclude" << std::endl;
    std::cout << std::endl;

    if (MwithMissingRef) {
        std::cerr << "WARNING: Reference contains missing genotypes (set to reference allele)" << std::endl;
        std::cerr << "         Fraction of sites with missing data:  "
                  << MwithMissingRef / (double) M << std::endl;
        std::cerr << "         Fraction of ref genotypes missing:    "
                  << GmissingRef / (double) M / Nref << std::endl;
    }
    if (MwithUnphasedRef) {
        std::cerr << "WARNING: Reference contains unphased genotypes (set to random phase)" << std::endl;
        std::cerr << "         Fraction of sites with unphased data: "
                  << MwithUnphasedRef / (double) M << std::endl;
        std::cerr << "         Fraction of ref genotypes unphased:   "
                  << GunphasedRef / (double) M / Nref << std::endl;
    }
    std::cout << "Missing rate in target genotypes: " << GmissingTarget / (double) M / Ntarget << std::endl;
    std::cout << std::endl;

    int numTmpUnphased = 0;
//    for (uint j = 0; j < isTmpPhased.size(); j++)
//        numTmpUnphased += !isTmpPhased[j];
    if (numTmpUnphased)
        std::cout << "SNPs excluded from phasing that will be kept in output (unphased): "
                  << numTmpUnphased << std::endl << std::endl;


    if (M <= 1U) {
        std::cerr << std::endl << "ERROR: Target and ref have too few matching SNPs (M = " << M << ")" << std::endl;
        exit(1);
    }


    return chrBps;
}

VcfHandler::VcfHandler(const std::string &vcfRef, const std::string &vcfTarget,
                       int &chrom, const std::string &geneticMapFile, const std::string &tmpFile,
                       const std::string &writeMode) {
    refCohortFlag = 1;
    // perform synced read
//    std::vector <bool> hapsRef;     // M*2*Nref
//    std::vector <uchar> genosTarget; // M*Ntarget
    std::vector<std::pair<int, int> > chrBps = parse_vcf(vcfRef, vcfTarget, chrom,
                                                         hapsRef, genosTarget, tmpFile, writeMode);
    std::cout << "chrBps size = " << chrBps.size() << std::endl;
    // interpolate genetic coordinates
    cMs = MapHandler::parse_map(chrBps, geneticMapFile);
    uint64 physRange = 0;
    double cMrange = 0;
    for (uint64 m = 0; m + 1 < chrBps.size(); m++)
        if (chrBps[m + 1].first == chrBps[m].first) {
            physRange += chrBps[m + 1].second - chrBps[m].second;
            cMrange += cMs[m + 1] - cMs[m];
        }
//    snpRate = M/cMrange;

    if (physRange == 0 || cMrange == 0) {
        std::cerr << "ERROR: Physical and genetic distance ranges must be positive" << std::endl;
        exit(1);
    }
}

VcfHandler::~VcfHandler() {

}


uint64 VcfHandler::getNref(void) const { return Nref; }

uint64 VcfHandler::getNtarget(void) const { return Ntarget; }

const std::string &VcfHandler::getTargetID(int n) const { return targetIDs[n]; }

VcfHandler::VcfHandler(const std::string &vcfFile, const int inputChrom, const int chromX,
                       const std::string &geneticMapFile, const std::string &tmpFile, const std::string &writeMode) {
    refCohortFlag = 2;
    // perform synced read
    std::vector<std::pair<int, int> > chrBps = initVcf(vcfFile, inputChrom, chromX, tmpFile, writeMode);
    std::cout << "chrBps size = " << chrBps.size() << std::endl;
    // interpolate genetic coordinates
    cMs = MapHandler::parse_map(chrBps, geneticMapFile);
    uint64 physRange = 0;
    double cMrange = 0;
    for (uint64 m = 0; m + 1 < chrBps.size(); m++)
        if (chrBps[m + 1].first == chrBps[m].first) {
            physRange += chrBps[m + 1].second - chrBps[m].second;
            cMrange += cMs[m + 1] - cMs[m];
        }
//    snpRate = M/cMrange;

    if (physRange == 0 || cMrange == 0) {
        std::cerr << "ERROR: Physical and genetic distance ranges must be positive" << std::endl;
        exit(1);
    }
}

std::vector<std::pair<int, int>> VcfHandler::initVcf(const std::string &vcfFile, const int inputChrom, const int chromX, const std::string &tmpFile,
                         const std::string &writeMode) {
    std::vector<std::pair<int, int> > chrBps;
    std::cout << inputChrom << std::endl;

    bcf_srs_t *sr = bcf_sr_init();
    bcf_sr_set_opt(sr, BCF_SR_PAIR_LOGIC, BCF_SR_PAIR_EXACT);
    bcf_sr_set_opt(sr, BCF_SR_REQUIRE_IDX);

    if (!bcf_sr_add_reader(sr, vcfFile.c_str())) {
        std::cerr << "ERROR: Could not open " << vcfFile << " for reading: " << bcf_sr_strerror(sr->errnum)
                  << std::endl;
        exit(1);
    }
    int nseq = 0;
    bcf_hdr_t *hdr = bcf_sr_get_header(sr, 0);
    auto samples = hdr->samples;
//    const char **seqnames = NULL;
    cur_chr = bcf_hdr_seqnames(hdr, &nseq)[0];
//    chrom = StringUtils::bcfNameToChrom(bcf_hdr_id2name(hdr, hdr->rid), 1, 0);

    // Open VCF for writing, "-" stands for standard output
    //      wbu .. uncompressed BCF
    //      wb  .. compressed BCF
    //      wz  .. compressed VCF
    //      w   .. uncompressed VCF
    htsFile *out = hts_open(tmpFile.c_str(), writeMode.c_str());
    htsThreadPool p = {hts_tpool_init(omp_get_max_threads()),
                       0};
    hts_set_thread_pool(out, &p);
    // Print the VCF header
    bcf_hdr_write(out, hdr);

    Nref = Ntarget = bcf_hdr_nsamples(hdr);
//    header = bcf_hdr_read(vcf_file);
//    ->header->samples
    std::map<int, int> bpToSyncedIndex1; // bp -> 1-based index (m+1)

    // Read target sample IDs
    targetIDs.resize(Ntarget);
    for (uint i = 0; i < Ntarget; i++)
        targetIDs[i] = hdr->samples[i];

    std::cout << std::endl;
    std::cout << "Reference samples: Nref = " << Nref << std::endl;
    std::cout << "Target samples: Ntarget = " << Ntarget << std::endl;

    M = 0;
    uint Mexclude = 0, MtargetOnly = 0, MrefOnly = 0, MmultiAllelicTgt = 0,
            MmultiAllelicRef = 0, MmonomorphicRef = 0;
    uint MwithMissingRef = 0, MwithUnphasedRef = 0, MnotInRegion = 0, MnotOnChrom = 0;
    uint MrefAltError = 0, numRefAltSwaps = 0;
    uint64 GmissingRef = 0, GunphasedRef = 0, GmissingTarget = 0;
    uint w = 521288629; // fast rng: Marsaglia's MWC

    int mrec_gt = 0, *rec_gt = NULL;

    int prev_rid = -1; // chromosome BCF id and human-readable numeric id
    while (bcf_sr_next_line(sr)) {
        bcf1_t *rec = bcf_sr_get_line(sr, 0);

        if (!rec) {
            //fprintf(stderr, "onlyT .. %s:%d\n", bcf_seqname(tgt_hdr, tgt), tgt->pos+1);
            std::cerr << "no record here" << std::endl;
            continue;
        }
        //fprintf(stderr, "match .. %s:%d\n", bcf_seqname(ref_hdr, ref), ref->pos+1);

        // deal with multi-allelic and monomorphic markers
        // drop multi-allelic target markers
        int nrec_gt = bcf_get_genotypes(hdr, rec, &rec_gt, &mrec_gt);
        if (rec->n_allele > 2) {
            MmultiAllelicTgt++;
            continue;
        }
        // drop monomorphic reference markers
        if (rec->n_allele == 1) {
            MmonomorphicRef++;
            continue;
        }
        // preserve monomorphic markers if biallelic in the reference panel
        if (rec->n_allele < 2) {
            // here cause we are doing cohort phase, if all the samples here is monomorphic, then just skip this snp site
            // bcf_update_alleles(hdr, tgt, (const char **) ref->d.allele, ref->n_allele);
            continue;
        }
        // drop multi-allelic reference markers
        if (rec->n_allele > 2) {
            MmultiAllelicRef++;
            continue;
        }

        // check for REF/ALT swaps
        // todo: no use here? why keeping it?
        bool refAltSwap = false;
//        if (strcmp(tgt->d.allele[0], ref->d.allele[0]) == 0 &&
//            strcmp(tgt->d.allele[1], ref->d.allele[1]) == 0) {
//            refAltSwap = false;
//        }
//        else {
//            MrefAltError++;
//            continue;
//        }


        // Check the chromosome: if region was requested (chrom is set), synced
        // reader already positioned us in the right region. Otherwise, we process
        // only the first chromosome in the file and quit
        if (prev_rid < 0) {
            prev_rid = rec->rid;
            if (!inputChrom) // learn the human-readable id
            {
                std::cout << inputChrom << std::endl;
//                chrom = 22;
//                chrom = StringUtils::bcfNameToChrom(bcf_hdr_id2name(tgt_hdr, tgt->rid), 1, 0);
            }

        }
        if (prev_rid != rec->rid) break;

        M++; // SNP passes checks
        bpToSyncedIndex1[rec->pos + 1] = M; // TODO: be careful about duplicate bp (multiallelics?)

        // append chromosome number and base std::pair coordinate to chrBps
        chrBps.push_back(std::make_pair(inputChrom, rec->pos + 1));

        // process reference haplotypes: append 2*Nref entries (0/1 std::pairs) to hapsRef[]
        // check for missing/unphased ref genos (missing -> REF allele; unphased -> random phase)
        int numMissing, numUnphased;
        process_ref_genotypes_cohort(Nref, nrec_gt, rec_gt, inputChrom == chromX,
                              hapsRef, numMissing, numUnphased, w, snp_pos_map, het_pos_map,rec);

        if (numMissing) MwithMissingRef++;
        if (numUnphased) MwithUnphasedRef++;
        GmissingRef += numMissing;
        GunphasedRef += numUnphased;

        // process target genotypes: append Ntarget entries (0/1/2/9) to genosTarget[]
        process_target_genotypes_cohort(Ntarget, nrec_gt, rec_gt, inputChrom == chromX, genosTarget, numMissing);
        GmissingTarget += numMissing;


        // print the record
        bcf_write(out, hdr, rec);
        //    isTmpPhased.push_back(true);
    }
//    uint size_of_bits = hapsRef.size();
//    int count = 0;
//    std::ofstream out2("check_bits_ref.txt", std::ios::out);
//    for(int i=0; i< hapsRef.size();i++){
//        if(i%1030<1030){
//            out2<< hapsRef[i];
//        }
//        if(i%1030==1029){
//            out2<< std::endl;
//        }
//    }
//    for(std::_Bit_reference  bbb: hapsRef){
//        if(bbb){
//            std::cout<<bbb<<std::endl;
//            count++;
//        }
//    }

//    uint size_of_bits_2 = genosTarget.size();
//    int count_2 = 0;
//    std::ofstream out1("check_bits.txt", std::ios::out);
//    for(int i=0; i< genosTarget.size();i++){
//        if(i%1030<1030){
//            out1<< genosTarget[i];
//        }
//        if(i%1030==1029){
//            out1<< std::endl;
//        }
//    }
//    for(auto &bbb: genosTarget){
//        if(bbb){
//            std::cout<<bbb<<std::endl;
//            count_2++;
//        }
//    }

    bcf_sr_destroy(sr);
    hts_close(out);
    hts_tpool_destroy(p.pool);
    free(rec_gt);

    std::cout << "SNPs to analyze: M = " << M << " SNPs in both target and reference" << std::endl;
    std::cout << std::endl;
    std::cout << "SNPs ignored: " << MtargetOnly << " SNPs in target but not reference" << std::endl;
    if (MtargetOnly > M / 10U)
        std::cerr << "              --> WARNING: Check REF/ALT agreement between target and ref? <--"
                  << std::endl;
    std::cout << "              " << MrefOnly << " SNPs in reference but not target" << std::endl;
    std::cout << "              " << MmultiAllelicTgt << " multi-allelic SNPs in target" << std::endl;
    if (MmultiAllelicRef)
        std::cout << "              " << MmultiAllelicRef << " SNPs biallelic in target but multi-allelic in ref"
                  << std::endl;
    if (MmonomorphicRef)
        std::cout << "              " << MmonomorphicRef << " SNPs biallelic in target but monomorphic in ref"
                  << std::endl;
    std::cout << std::endl;

    if (MwithMissingRef) {
        std::cerr << "WARNING: Reference contains missing genotypes (set to reference allele)" << std::endl;
        std::cerr << "         Fraction of sites with missing data:  "
                  << MwithMissingRef / (double) M << std::endl;
        std::cerr << "         Fraction of ref genotypes missing:    "
                  << GmissingRef / (double) M / Nref << std::endl;
    }
    if (MwithUnphasedRef) {
        std::cerr << "WARNING: Reference contains unphased genotypes (set to random phase)" << std::endl;
        std::cerr << "         Fraction of sites with unphased data: "
                  << MwithUnphasedRef / (double) M << std::endl;
        std::cerr << "         Fraction of ref genotypes unphased:   "
                  << GunphasedRef / (double) M / Nref << std::endl;
    }
    std::cout << "Missing rate in target genotypes: " << GmissingTarget / (double) M / Ntarget << std::endl;
    std::cout << std::endl;

    int numTmpUnphased = 0;

    if (M <= 1U) {
        std::cerr << std::endl << "ERROR: Target and ref have too few matching SNPs (M = " << M << ")" << std::endl;
        exit(1);
    }
    return chrBps;
}

VcfHandler::VcfHandler() {}
