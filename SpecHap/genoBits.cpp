//
// Created by wangxuedong on 5/18/21.
//
#include <vector>
#include <iostream>
#include "genoBits.h"
#include "type.h"

void GenoBitsbuilder::to_geno_bits(const std::vector<bool> &hapsRef,
                                   const std::vector<uchar> &genosTarget, const std::vector<double> &cMs,
                                   double cMmax, uint64 M, uint64 Nref, uint64 Ntarget) {
    const uint segMin = 16;
    std::vector<uint64> snpInds;
    std::vector<double> cMvec;
    std::vector<std::vector<uint64> > seg64snpInds;
    for (uint64 m = 0; m < M; m++) {
        if (cMvec.size() == 64 || (cMvec.size() >= segMin && cMs[m] > cMvec[0] + cMmax)) {
            seg64snpInds.push_back(snpInds);
            seg64cMvecs.push_back(cMvec);
            snpInds.clear();
            cMvec.clear();
        }
        snpInds.push_back(m);
        cMvec.push_back(cMs[m]);
    }
    seg64snpInds.push_back(snpInds);
    seg64cMvecs.push_back(cMvec);

    Mseg64 = seg64snpInds.size();
    effectiveSNPSites.resize(Mseg64);

    for (int i = 0; i < Mseg64; ++i) {
        std::vector<uint64> cur_seg = seg64snpInds[i];
        blockPaddingCounts.push_back(64 - cur_seg.size());
        if (i > 0) {
            blockPaddingCounts[i] += blockPaddingCounts[i - 1];
        }
        effectiveSNPSites[i].resize(64);
        for (int j = 0; j < cur_seg.size(); ++j) {
            effectiveSNPSites[i][j] = true;
        }
    }
    for (int i = 0; i < Mseg64; ++i) {
        blockPaddingCounts[i] -= 64 - seg64snpInds[i].size();
    }
    std::cout << "Number of <=(64-SNP, " << cMmax << "cM) segments: " << Mseg64 << std::endl;
    std::cout << "Average # SNPs per segment: " << M / Mseg64 << std::endl;

    uint64 Nr = Nref;
    uint64 Nm = Ntarget;
    // todo:: ref and target was assigned the same size of storage, concat them.
    haploBits = ALIGNED_MALLOC_UINT64_MASKS(Mseg64 * Nr);
    genoBits = ALIGNED_MALLOC_UINT64_MASKS(Mseg64 * Nm);
//    auto sth = haploBits[0];
//    int s_size = sizeof(haploBits[0]);
//    int ss_size = sizeof(haploBits);
    memset(haploBits, 0, Mseg64 * Nr * sizeof(haploBits[0]));
    memset(genoBits, 0, Mseg64 * Nm * sizeof(genoBits[0]));
    int ssss = seg64snpInds.size();
    for (uint64 m64 = 0; m64 < Mseg64; m64++) {
        for (uint64 j = 0; j < seg64snpInds[m64].size(); j++) {
//            int s1 = seg64snpInds[m64].size();
            uint64 m = seg64snpInds[m64][j];
            for (uint64 nr = 0; nr < Nref; nr++) { // store haploBits for ref haplotypes in genoBits
                bool haps0 = hapsRef[m * 2 * Nref + 2 * nr];
                bool haps1 = hapsRef[m * 2 * Nref + 2 * nr + 1];
                if (haps0||haps1){
                }
                haploBits[m64 * Nr + nr].is0 |= ((uint64) haps0) << j;
                haploBits[m64 * Nr + nr].is2 |= ((uint64) haps1) << j;
//                auto test = haploBits[m64 * Nr + nr];
            }
//            for (uint64 n = Nref; n < N; n++) { // set genoBits for target genotypes
            for (uint64 nm = 0; nm < Ntarget; nm++) { // set genoBits for target genotypes
                uchar geno = genosTarget[m * Ntarget + nm];
                genoBits[m64 * Nm + nm].is0 |= ((uint64) (geno == 0)) << j;
                genoBits[m64 * Nm + nm].is2 |= ((uint64) (geno == 2)) << j;
                genoBits[m64 * Nm + nm].is9 |= ((uint64) (geno == 9)) << j;
            }
        }
        for (uint64 n = 0; n < Nm; n++)
            for (uint64 j = seg64snpInds[m64].size(); j < 64; j++)
                genoBits[m64 * Nm + n].is9 |= 1ULL << j;
    }
}

GenoBitsbuilder::GenoBitsbuilder(const std::vector<bool> &hapsRef, const std::vector<uchar> &genosTarget,
                                 const std::vector<double> &cMs, double cMmax, uint64 M, uint64 Nref, uint64 Ntarget) {

    to_geno_bits(hapsRef, genosTarget, cMs, cMmax, M, Nref, Ntarget);
}

GenoBitsbuilder::~GenoBitsbuilder() {
}


const uint64_masks *GenoBitsbuilder::getGenoBits(void) const { return genoBits; }

const uint64_masks *GenoBitsbuilder::getHaploBits(void) const { return haploBits; }

uint64 GenoBitsbuilder::getMseg64(void) const { return Mseg64; }

std::vector<std::vector<double> > GenoBitsbuilder::getSeg64cMvecs(void) const { return seg64cMvecs; }

std::vector<std::vector<bool>> GenoBitsbuilder::getEffectiveSNPSites(void) const {
    return effectiveSNPSites;
}

std::vector<int> GenoBitsbuilder::getBlockPaddingCount() const {
    return blockPaddingCounts;
}


