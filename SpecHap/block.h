//
// Created by wangxuedong on 7/8/21.
//

#ifndef SPECHAP_BLOCK_H
#define SPECHAP_BLOCK_H


#include <vector>
#include <map>
#include <unordered_map>
#include "results.h"

class CurComp
{
public:
    explicit CurComp();
    ~CurComp();
    std::string cur;
    uint cur_count;
    std::vector<uint> cur_id_list;
    std::string comp;
    uint comp_count;
    std::vector<uint> comp_id_list;
    uint min_count;
    uint total_count;
};


class Block
{
private:
public:
    explicit Block();
    ~Block();
    uint blk_idx;
    std::vector<uint> hap1;
    std::vector<uint> hap2;
    std::vector<uint> hap_range_idx;
    std::vector<uint> snp_idx;
    std::vector<uint> het_idx;
    std::vector<uint> pseudo_hap1;
    std::vector<uint> pseudo_hap2;
    std::vector<uint> phased_het_idx;
    std::string tag;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;
//    std::unordered_map<uint, CurComp> cur_comp_res;
    uint blk_size;
    uint start;
    uint end;

};



#endif //SPECHAP_BLOCK_H
