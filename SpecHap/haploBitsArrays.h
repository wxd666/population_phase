//
// Created by yangshuo on 5/7/2021.
//

#ifndef SPECHAP_HAPLOBITSARRAYS_H
#define SPECHAP_HAPLOBITSARRAYS_H

#include "type.h"
#include "memoryUtils.h"
#include <iostream>
#include <math.h>
#include "block.h"
#include <algorithm>
#include <string>

namespace HaploBitsArrays {

    struct SortDiv {
        int a, d;
    };

    struct BlockResItem {
        int cur_count, comp_count;
        BlockResItem(int cur_C, int comp_C) {
            cur_count = cur_C;
            comp_count = comp_C;
        }
    };

    struct CompHapRefIdItem {
        std::set<int> curRefIdSet;
        std::set<int> compRefIdSet;
    };

    class HaploBitsT {
    public:
        const uint64_masks *ref_bits; /// matrix: lines : Mseg64, columns : NREF
        const uint64_masks *tar_bits; /// matrix: lines : Mseg64, columns : Ntarget
        std::vector<std::vector<bool>> effectiveSNPSites;
        uint64 Nref, Ntarget, M; // N = Nref + Ntarget, M snps pass check
        uint64 Mseg64; // number of <=64-SNP chunks
        std::vector<std::vector<double> > seg64cMvecs;
        std::vector<double> cms;
        std::vector<int> block_padding_counts;
        std::vector<double> cms64j;

        HaploBitsT();

        explicit HaploBitsT(const uint64_masks *refBits, const uint64_masks *tarBits,
                            const std::vector<std::vector<bool>> &effectiveSnpSites, uint64 nref, uint64 ntarget,
                            uint64 m,
                            uint64 mseg64, const std::vector<std::vector<double>> &seg64CMvecs,
                            const std::vector<double> &cMs,
                            const std::vector<int> &blockPaddingCounts);

        ~HaploBitsT();
    };

    class CompressedHaploBitsT {
    public:
        CompressedHaploBitsT(const uint64_masks *ref_bits, uint64 Mseg64, uint64 Nref,
                             const std::vector<uint64> &splits64j,
                             const std::vector<uchar> &splitGenos, const std::vector<uint64_masks> &tgtGenoBits,
                             const std::vector<uint> &bestHaps, const std::vector<double> &cMcoords);

        inline double cal_score(double a, double b) const {
            double score;
            if (a > b)
            {
                score = a + log10(1 + pow(10, b - a)) - log10(2);
            }
            else
            {
                score = b + log10(1 + pow(10, a - b)) - log10(2);
            }
            return score;
        }

        inline std::string find_comp(std::string cur_hap) {
            std::string comp_hap;
            for (int i = 0; i < cur_hap.size(); ++i) {
                if (cur_hap[i] == '0') {
                    comp_hap += '1';
                }
                if (cur_hap[i] == '1') {
                    comp_hap += '0';
                }
            }
            return comp_hap;
        }

        int getBit(uint64 n, uint64 m) const;

        uint64 getBits64(uint64 n, uint64 m64) const;

        int getNhaps() const;

        int getM() const;

        double *getWeightByPBWT(uint start, uint end) const;

        double *getWeightByRecombination(uint start, uint end, uint mapIdx0, uint mapIdx1) const;

        double *getWeightByRecombinationV2(uint start, uint end, uint mapIdx0, uint mapIdx1);

        double * getWeightByRecombinationV3(uint start, uint end, uint mapIdx0, uint mapIdx1) const;

        double * getWeightByRecombinationV4(uint start, uint end, uint mapIdx0, uint mapIdx1) const;

        double * getWeightByRecombinationV5(uint start, uint end, uint mapIdx0, uint mapIdx1) const;

        void setCms(std::vector<double> cMs);

        void initAlleleIs0Array(int blk_start, int blk_end);

        const std::vector<uint> &getBestK() const;

        const std::vector<bool> &getHapsRef() const;

        void setHapsRef(const std::vector<bool> &hapsRef);

        /**
         * get weight by blocks with phased snp sites, using block.pseudo haps and phased het idx, using the block end site for recombination
         */
        double * getWeightByBlocks(const Block& block1, const Block& block2);

        /**
         * this interface would do the same job as getWeightByBlocks, but using site-site for finding recombination
         */
        double * getWeightByBlocksV2(const Block& block1, const Block& block2);

        double * getWeightByBlocksV3(const Block& block1, const Block& block2);

        double * getWeightByBlocksV4(const Block& block1, const Block& block2);

        double * getWeightByBlocksV5(const Block& block1, const Block& block2);

        double * getWeightByBlocksV6(const Block& block1, const Block& block2);

        /**
         * phasing the snp sites in each block by finding out the uniq complementary pair
         * @param hapMap
         * @param blockSize
         */
        void phasingInBlock(std::map<uint, Block>& hapMap, int blockSize);

        std::map<uint, Block> generateCompHapMapBySize(std::map<uint, Block>& hapMap, int blockSize);

        /**
         * phasing the snp sites in each block by finding out the uniq complementary pair
         * if more than 1 uniq complementary pairs have been found, then try to sort them with hom snp sites
         * in this interface, we tried to find out the min dif haplotype instead of sorting the whole comp pair map
         * @param hapMap
         * @param blockSize
         */
        void phasingInBlockV2(std::map<uint, Block>& hapMap, int blockSize);

        void setM(uint64 m);

        void testGetWeight();

        inline double sq(double x) const { return x * x; }

        inline int popCount64(uint64 i) {
            int errCount = 0;
            while (i > 0) {
                if (i & 1ULL) {
                    errCount++;
                }
                i = (i >> 1);
            }
            return errCount;
        }

        inline int popcount64_01(uint64 i) {
            return i != 0;
        }

        uint64 getNSites() const;

        const std::vector<std::string> &getAllHaps() const;

        void setAllHaps(const std::vector<std::string> &allHaps);

        const std::vector<std::string> &getAllHapsWithCompressedHom() const;

        void setAllHapsWithCompressedHom(const std::vector<std::string> &allHapsWithCompressedHom);

    private:
        uint64 *haploBitsT;/// line: NHaps, col: NSite64, therefore, we could separate the N allele sites into 64-bits blocks
        uint64 NHaps, NSites, NSite64, Nref, M;
        std::vector<double> cms;
        bool *alleleIs0Array; /// lines: NHaps, columns: NSites-Used in weight graph
        int blkStart, blkEnd, snpCnt;
        std::vector<uint> bestK;
        std::vector<bool> hapsRef;     // M*2*Nref
        std::map<uint, std::vector<int>> haploBitsTWithHom; /// each ref size = 2 * (split64j.size + 2), would contain the hom seq before het site at 2*het_id + 1, and het site at 2*(het_id+1)
        std::vector<double> cMcoords;
        std::vector<std::string> all_haps;
        std::vector<std::string> all_haps_with_compressed_hom;

        void setBit(uint64 n, uint64 m);
    };
}
#endif //SPECHAP_HAPLOBITSARRAYS_H
