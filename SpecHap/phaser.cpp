//
// Created by wangxuedong on 10/6/21.
//

//
// Created by yyh on 3/4/2019.
//

#include "phaser.h"
#include "htslib/vcf.h"
#include "type.h"
#include "util.h"
#include <iostream>
#include <cstdlib>
#include <set>
#include <unordered_map>
#include "vcfHandler.h"
#include "genoBits.h"
#include <boost/algorithm/string.hpp>

// TODO clarify between variant count and block count

Phaser::Phaser(const std::string &ref, const std::string &target, const std::string &fnout, int &chrom,
               const std::string &geneticMapFile, const std::string &tmpFile, const std::string &writeMode,
               double cMmax) {
    runTime_log.open("run_time.log", std::ios::out);
    // load ref as eagle
    tarVcf = new VCFReader(target.data());
//    validVcf = new VCFReader("/run/media/wangxuedong/TCGA-SKCM/tibet_pjt/chr22/test.target.dedup.vcf.gz");
    fwvcf = new VCFWriter(tarVcf->header, fnout.data());
//    frfrag = new FragmentReader(fnfrag.data());
    frbed = nullptr;
    coverage = 30;  //deprecated

//    if (OPERATION == MODE_10X)
//        frbed = new BEDReader(fnbed.data());

    bool use_secondary = false;
    recursive_complete_empty_blk_flag = true;
    threshold = 1e-20;

    spectral = new Spectral(frfrag, frbed, threshold, coverage, use_secondary);
    clock_t start, end;
    start = clock();

    VcfHandler parse_all_files(ref, target, chrom,
                               geneticMapFile,
                               tmpFile, "wz");

    GenoBitsbuilder to_bits(parse_all_files.hapsRef, parse_all_files.genosTarget, parse_all_files.cMs,
                            int(cMmax) == 0 ? 1 : int(cMmax),
                            parse_all_files.M, parse_all_files.Nref, parse_all_files.Ntarget);
    hapsRef = parse_all_files.hapsRef;
    refCohortFlag = parse_all_files.refCohortFlag;
    haploBitsT = new HaploBitsArrays::HaploBitsT(to_bits.getHaploBits(), to_bits.getGenoBits(),
                                                 to_bits.getEffectiveSNPSites(), parse_all_files.Nref,
                                                 parse_all_files.Ntarget, parse_all_files.M,
                                                 to_bits.getMseg64(), to_bits.getSeg64cMvecs(),
                                                 parse_all_files.cMs, to_bits.getBlockPaddingCount());
    end = clock();
    std::cout << "eagle load data time: " << (double) (end - start) / CLOCKS_PER_SEC << std::endl;
}

Phaser::~Phaser() {
    delete tarVcf;
    delete fwvcf;
    delete frfrag;
    delete spectral;
    runTime_log.close();
    if (frbed != nullptr)
        delete frbed;
}

void split_comma(char *str, std::vector<uint> &f) {
    char *numberstr = strtok(str, ",");
    while (numberstr != NULL) {
        f.push_back((uint) atoi(numberstr));
        numberstr = strtok(NULL, ",");
    }
}

std::vector<uint> split(const std::string &str, const std::string &pattern) {
    char *strc = new char[strlen(str.c_str()) + 1];
    strcpy(strc, str.c_str());   //string转换成C-string
    std::vector<uint> res;
    char *temp = strtok(strc, pattern.c_str());
    while (temp != NULL) {
        res.push_back(atoi(temp));
        temp = strtok(NULL, pattern.c_str());
    }
    delete[] strc;
    return res;
}

int Phaser::load_contig_records(ChromoPhaser *chromo_phaser) {
    int status = 0;
//    std::ofstream out1("het.txt", std::ios::out);
//    std::ofstream out2("snp.txt", std::ios::out);
    while (true) {
        ptr_ResultforSingleVariant result = std::make_shared<ResultforSingleVariant>();
        int flag = this->tarVcf->get_next_record_contig(result, true);
        // 2 for het
        if (flag == 2)
            continue;
        if (flag == 3)
            continue;
        if (flag == 4)
            continue;
        if (flag != 0)
            break;


        chromo_phaser->results_for_variant.push_back(result);
    }
//    for (auto &het : this->tarVcf->het_pos_container) {
//
//        out1 << het << std::endl;
//
//    }
//    out1 << this->tarVcf->het_pos_container.size() << std::endl;
//
//    for (auto &snp : this->tarVcf->snp_pos_container) {
//
//        out2 << snp << std::endl;
//
//    }
//    out2 << this->tarVcf->snp_pos_container.size() << std::endl;


    chromo_phaser->variant_count = chromo_phaser->results_for_variant.size();
    for (int i = 0; i < chromo_phaser->variant_count; i++) {
        chromo_phaser->variant_to_block_id[i] = i;
    }
    chromo_phaser->init_block_count = chromo_phaser->variant_count;
    return status;
}


int Phaser::load_contig_blocks(ChromoPhaser *chromo_phaser) {
    int status = 0;
    std::ofstream out1("het.txt", std::ios::out);
    std::ofstream out2("snp.txt", std::ios::out);
    while (true) {
        ptr_ResultforSingleVariant result = std::make_shared<ResultforSingleVariant>();
        int flag = this->tarVcf->get_next_record_contig(result, true);
        // 2 for het
        if (flag == 2)
            continue;
        if (flag == 3)
            continue;
        if (flag == 4)
            continue;
        if (flag != 0)
            break;


        chromo_phaser->results_for_variant.push_back(result);
    }
    for (auto &het : this->tarVcf->het_pos_container) {

        out1 << het << std::endl;

    }
    out1 << this->tarVcf->het_pos_container.size() << std::endl;

    for (auto &snp : this->tarVcf->snp_pos_container) {

        out2 << snp << std::endl;

    }
    out2 << this->tarVcf->snp_pos_container.size() << std::endl;
    chromo_phaser->variant_count = chromo_phaser->results_for_variant.size();


    std::unordered_map<uint, uint> ps2block_ids;
    uint block_count = 0;
    int count_win_size = 0;
    uint start_pos = 0;
    for (int i = 0; i < chromo_phaser->variant_count; i++) {
        auto result = chromo_phaser->results_for_variant[i];
        if (i == 0 || count_win_size > WINDOW_SIZE) {
            start_pos = result->pos;
            count_win_size = 0;
            block_count++;
        }

//        if (ps == 0) //not phased
//        {
//            chromo_phaser->variant_to_block_id[i] = i;
//            block_count++;
//        } else {      //phased
        if (ps2block_ids.count(start_pos) == 0) {   // not met before
            ps2block_ids[start_pos] = i;
            chromo_phaser->variant_to_block_id[i] = ps2block_ids[start_pos];

        } else {
            chromo_phaser->variant_to_block_id[i] = ps2block_ids[start_pos];
        }
        count_win_size++;
//        }
    }
    chromo_phaser->init_block_count = block_count;
    return status;
}


int Phaser::load_contig_blocks_cohort(uint &nt) {
    int status = 0;
//    std::ofstream out1("het.txt", std::ios::out);
//    std::ofstream out2("snp.txt", std::ios::out);
    while (true) {
        ptr_ResultforSingleVariant result = std::make_shared<ResultforSingleVariant>();
        int flag = this->tarVcf->get_next_record_contig_cohort(result, nt, true);
        // 2 for het
        if (flag == 2)
            continue;
        if (flag == 3)
            continue;
        if (flag == 4)
            continue;
        if (flag != 0)
            break;
//        chromo_phaser->results_for_variant.push_back(result);
    }
//    chromo_phaser->variant_count = chromo_phaser->results_for_variant.size();
//

    std::unordered_map<uint, uint> ps2block_ids;
    uint block_count = 0;
    int count_win_size = 0;
    uint start_pos = 0;
//    for (int i = 0; i < chromo_phaser->variant_count; i++) {
//        auto result = chromo_phaser->results_for_variant[i];
//        if (i == 0 || count_win_size > WINDOW_SIZE) {
//            start_pos = result->pos;
//            count_win_size = 0;
//            block_count++;
//        }
//
////        if (ps == 0) //not phased
////        {
////            chromo_phaser->variant_to_block_id[i] = i;
////            block_count++;
////        } else {      //phased
////        if (ps2block_ids.count(start_pos) == 0) {   // not met before
////            ps2block_ids[start_pos] = i;
////            chromo_phaser->variant_to_block_id[i] = ps2block_ids[start_pos];
////
////        } else {
////            chromo_phaser->variant_to_block_id[i] = ps2block_ids[start_pos];
////        }
//        count_win_size++;
////        }
//    }
//    chromo_phaser->init_block_count = block_count;
    return status;
}

void Phaser::phasing() {
    clock_t start_t, end_t;
    start_t = clock();
    uint prev_variant_count = 0;
//    std::ofstream file;
    for (uint rid = 0; rid < tarVcf->contigs_count; rid++) {
        if (tarVcf->jump_to_contig(rid) != 0)
            break;
        ChromoPhaser *chromo_phaser = new ChromoPhaser(rid, tarVcf->contigs[rid], WINDOW_OVERLAP, WINDOW_SIZE);
        std::string mess = "phasing haplotype for " + std::string(tarVcf->contigs[rid]);
        logging(std::cerr, mess);
        clock_t start1, end1;
        start1 = clock();
        load_contig_blocks(chromo_phaser);
        end1 = clock();
        std::cout << "spec load data time: " << (double) (end1 - start1) / CLOCKS_PER_SEC << std::endl;
        chromo_phaser->construct_phasing_window_initialize();
        spectral->set_chromo_phaser(chromo_phaser);
        clock_t start2, end2;
        start2 = clock();
        HaploBitsArrays::CompressedHaploBitsT *hapBitsT = constructHapBitsT(
                0, 200, false);
        end2 = clock();
        std::cout << "process bestk time: " << (double) (end2 - start2) / CLOCKS_PER_SEC << std::endl;
        clock_t start3, end3;
        start3 = clock();
        hapBitsT->setCms(haploBitsT->cms);
        hapBitsT->setHapsRef(hapsRef);
        hapBitsT->setM(haploBitsT->M);
        spectral->setHaploBitsT(hapBitsT);
        std::string runTimeMes = mess + ", data preprocessing finished";
        runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
        initialize_hap_map_list();
        runTimeMes = mess + ", hap_map initializing";
        runTimeLogging(runTime_log, start_t, end_t, runTimeMes);

        end3 = clock();
        std::cout << "init time: " << (double) (end3 - start3) / CLOCKS_PER_SEC << std::endl;
        phase_hap_map(false, haploBitsT->Nref);
        runTimeMes = mess + ", hap_map filling finished";
        runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
        runTimeMes = mess + "start generating spectral graph and cut";
        logging(runTime_log, runTimeMes);

        clock_t start4, end4;
        start4 = clock();
        Parameters parameters;
        std::map<std::string, int> significance_map_tmp;
        significance_map_tmp.emplace(std::make_pair("phased", 5));
        significance_map_tmp.emplace(std::make_pair("reduced", 0));
        significance_map_tmp.emplace(std::make_pair("empty", 0));
        significance_map_tmp.emplace(std::make_pair("empty_phased", 4));
        significance_map_tmp.emplace(std::make_pair("reduce_phased", 1));
        significance_map_tmp.emplace(std::make_pair("secondary_empty_phased", 6));
        parameters.comp_pair_count = 8;
        parameters.reduce_weight_rate = 0.5;
        parameters.significance_map.emplace(std::make_pair(32, significance_map_tmp));
        parameters.significance_map.emplace(std::make_pair(16, significance_map_tmp));
        parameters.significance_map.emplace(std::make_pair(8, significance_map_tmp));
        parameters.significance_map.emplace(std::make_pair(4, significance_map_tmp));
//        std::cout << "transed hap map size = " << transed_hap_map.size() << std::endl;
        for (auto i = hap_map_32.begin(); i != hap_map_32.end(); ++i) {
            if (i->second.tag == "empty") {
                for (int j = i->second.het_idx.front(); j < i->second.het_idx.front() + 32; j += 16) {
                    if (hap_map_16.count(j) == 0) continue;
                    if (hap_map_16[j].tag == "empty") {
                        for (int k = j; k < j + 16; k += 8) {
                            if (hap_map_8.count(k) == 0) continue;
                            if (hap_map_8[k].tag == "empty") {
                                for (int l = k; l < k + 8; l += 4) {
                                    if (hap_map_4.count(l) == 0) continue;
                                    if (hap_map_4[l].tag == "empty") {
                                        continue;
                                    } else {
                                        std::vector<std::string> pos_neg_idx;
                                        std::vector<std::string> blk_order;
                                        for (int m = l; m < l + hap_map_4[l].het_idx.size(); ++m) {
                                            pos_neg_idx.emplace_back(std::to_string(m) + "+");
                                            pos_neg_idx.emplace_back(std::to_string(m) + "-");
                                        }
                                        uint ni = pos_neg_idx.size();
                                        auto *rawGraph = new double[ni * ni];
                                        int *rawCount = new int[ni * ni];
                                        for (int i = 0; i < ni * ni; ++i) {
                                            rawGraph[i] = 0;
                                            rawCount[i] = 0;
                                        }
                                        ViewMap weighted_graph(rawGraph, ni, ni);
                                        CViewMap count_graph(rawCount, ni, ni);
                                        spectral->load_weight(weighted_graph, hap_map_4, l, parameters, 0, 4);
                                        blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_4,
                                                                                transed_hap_map, parameters);
                                    }
                                }
                            } else {
                                std::vector<std::string> pos_neg_idx;
                                std::vector<std::string> blk_order;
                                for (int m = k; m < k + hap_map_8[k].het_idx.size(); ++m) {
                                    pos_neg_idx.emplace_back(std::to_string(m) + "+");
                                    pos_neg_idx.emplace_back(std::to_string(m) + "-");
                                }
                                uint ni = pos_neg_idx.size();
                                auto *rawGraph = new double[ni * ni];
                                int *rawCount = new int[ni * ni];
                                for (int i = 0; i < ni * ni; ++i) {
                                    rawGraph[i] = 0;
                                    rawCount[i] = 0;
                                }
                                ViewMap weighted_graph(rawGraph, ni, ni);
                                CViewMap count_graph(rawCount, ni, ni);
                                spectral->load_weight_for_blocks(weighted_graph, hap_map_8[k], hap_map_4,
                                                                 parameters, 8,
                                                                 4);
                                spectral->load_weight(weighted_graph, hap_map_8, k, parameters, 0, 8);
                                blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_8,
                                                                        transed_hap_map,
                                                                        parameters);
                            }
                        }
                    } else {
                        std::vector<std::string> pos_neg_idx;
                        std::vector<std::string> blk_order;
                        for (int m = j; m < j + hap_map_16[j].het_idx.size(); ++m) {
                            pos_neg_idx.emplace_back(std::to_string(m) + "+");
                            pos_neg_idx.emplace_back(std::to_string(m) + "-");
                        }
                        uint ni = pos_neg_idx.size();
                        auto *rawGraph = new double[ni * ni];
                        int *rawCount = new int[ni * ni];
                        for (int i = 0; i < ni * ni; ++i) {
                            rawGraph[i] = 0;
                            rawCount[i] = 0;
                        }
                        ViewMap weighted_graph(rawGraph, ni, ni);
                        CViewMap count_graph(rawCount, ni, ni);
                        spectral->load_weight_for_blocks(weighted_graph, hap_map_16[j], hap_map_8, parameters, 16,
                                                         8);
                        spectral->load_weight_for_blocks(weighted_graph, hap_map_16[j], hap_map_4, parameters, 16,
                                                         4);
                        spectral->load_weight(weighted_graph, hap_map_16, j, parameters, 0, 16);
                        blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_16,
                                                                transed_hap_map,
                                                                parameters);
                    }
                }
            } else {
                std::vector<std::string> pos_neg_idx;
                std::vector<std::string> blk_order;
                for (int m = i->second.het_idx.front(); m < i->second.het_idx.front() + i->second.het_idx.size(); ++m) {
                    pos_neg_idx.emplace_back(std::to_string(m) + "+");
                    pos_neg_idx.emplace_back(std::to_string(m) + "-");
                }
                uint ni = pos_neg_idx.size();
                auto *rawGraph = new double[ni * ni];
                int *rawCount = new int[ni * ni];
                for (int i = 0; i < ni * ni; ++i) {
                    rawGraph[i] = 0;
                    rawCount[i] = 0;
                }
                ViewMap weighted_graph(rawGraph, ni, ni);
                CViewMap count_graph(rawCount, ni, ni);
                spectral->load_weight_for_blocks(weighted_graph, hap_map_32[i->second.het_idx.front()], hap_map_4,
                                                 parameters, 32, 4);
                spectral->load_weight_for_blocks(weighted_graph, hap_map_32[i->second.het_idx.front()], hap_map_8,
                                                 parameters, 32, 8);
                spectral->load_weight_for_blocks(weighted_graph, hap_map_32[i->second.het_idx.front()], hap_map_16,
                                                 parameters, 32, 16);
                spectral->load_weight(weighted_graph, hap_map_32, i->second.start, parameters, 0, 32);
                blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_32, transed_hap_map,
                                                        parameters);
            }
        }
        end4 = clock();
        std::cout << "spec graph: " << (double) (end4 - start4) / CLOCKS_PER_SEC << std::endl;
        int idx = 0;
//        transed_hap_map = test_hap_map;

        std::ofstream fout1;
        fout1.open("check_trans_mp.txt");
//
//        for (auto &trans : transed_hap_map)
//
//            fout1 << trans.first << '\t' << vec_to_string(trans.second.pseudo_hap1) << std::endl;
//
//        fout1.close();
        clock_t start5, end5;
        start5 = clock();
        runTimeMes = mess + ", in-block phase finished";
        runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
        runTimeMes = mess + ", start linking the blocks";
        logging(runTime_log, runTimeMes);

//        std::ofstream hapMap_4("hapMap_4.txt", std::ios::out);
//        for (auto i = hap_map_4.begin(); i != hap_map_4.end(); ++i) {
//            hapMap_4 << i->second.start << "\t" << i->second.end << std::endl;
//            for (auto j = i->second.cur_comp_res.begin(); j != i->second.cur_comp_res.end(); ++j) {
//                hapMap_4 << j->second.cur << "\t" << j->second.min_count << "\t" << j->second.total_count << std::endl;
//            }
//        }
//        std::ofstream hapMap_8("hapMap_8.txt", std::ios::out);
//        for (auto i = hap_map_8.begin(); i != hap_map_8.end(); ++i) {
//            hapMap_8 << i->second.start << "\t" << i->second.end << std::endl;
//            for (auto j = i->second.cur_comp_res.begin(); j != i->second.cur_comp_res.end(); ++j) {
//                hapMap_8 << j->second.cur << "\t" << j->second.min_count << "\t" << j->second.total_count << std::endl;
//            }
//        }
//        std::ofstream hapMap_16("hapMap_16.txt", std::ios::out);
//        for (auto i = hap_map_16.begin(); i != hap_map_16.end(); ++i) {
//            hapMap_16 << i->second.start << "\t" << i->second.end << std::endl;
//            for (auto j = i->second.cur_comp_res.begin(); j != i->second.cur_comp_res.end(); ++j) {
//                hapMap_16 << j->second.cur << "\t" << j->second.min_count << "\t" << j->second.total_count << std::endl;
//            }
//        }
//        std::ofstream hapMap_32("hapMap_32.txt", std::ios::out);
//        for (auto i = hap_map_32.begin(); i != hap_map_32.end(); ++i) {
//            hapMap_32 << i->second.start << "\t" << i->second.end << std::endl;
//            for (auto j = i->second.cur_comp_res.begin(); j != i->second.cur_comp_res.end(); ++j) {
//                hapMap_32 << j->second.cur << "\t" << j->second.min_count << "\t" << j->second.total_count << std::endl;
//            }
//        }

        link_blk();
        runTimeMes = mess + ", link block finished";
        runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
        end5 = clock();
        std::cout << "link block time: " << (double) (end5 - start5) / CLOCKS_PER_SEC << std::endl;
        //todo: imputation
//        hapBitsT->phasingInBlockV2(phased_hap_map, WINDOW_SIZE);

        std::ofstream out8("final_hap.txt", std::ios::out);
        for (auto &h : transed_hap_map) {

            for (uint i = 0; i < h.second.phased_het_idx.size(); i++) {
                out8 << h.first << '\t' << h.second.hap_range_idx[h.second.phased_het_idx[i]] << '\t'
                     << h.second.pseudo_hap1[i] << h.second.pseudo_hap2[i]
                     << std::endl;
            }
        }
        out8.close();
//        fwvcf->write_nxt_contigs(tarVcf->contigs[rid].data(), chromo_phaser, *tarVcf, out1);
        //write vcf
        prev_variant_count += chromo_phaser->variant_count;
        spectral->release_chromo_phaser();
        delete chromo_phaser;
    }
}


void Phaser::phasing_by_chrom(uint var_count, ChromoPhaser *chromo_phaser) {
//    frfrag->set_curr_chr_var_count(var_count);

    while (chromo_phaser->phased->rest_blk_count > 0) {
        if (chromo_phaser->phased->rest_blk_count > chromo_phaser->init_block_count)
            break;
        if (OPERATION == MODE_10X)
            chromo_phaser->phased->update_phasing_info(MAX_BARCODE_SPANNING);
        else {
            if (KEEP_PS)
                chromo_phaser->phased->update_phasing_info_keep_phased();
            else
                chromo_phaser->phased->update_phasing_info();
        }
        spectral->solver(tarVcf->snp_pos_container, tarVcf->het_pos_container);
    }
    //std::cout << chromo_phaser->phased->phased_blk_count << std::endl;
    if (OPERATION == MODE_HIC) {
        phase_HiC_poss(chromo_phaser);
    }
}


void Phaser::phase_HiC_poss(ChromoPhaser *chromo_phaser) {
    std::unordered_map<uint, std::set<uint>> connected_comps = spectral->load_hic_poss_info();
    for (auto i : connected_comps) {
        std::set<uint> &connected_comp = i.second;
        int nblocks = connected_comp.size();
        if (nblocks == 1)
            continue;

        int count = 0;

        //split into window again
        int HiC_poss_block = WINDOW_SIZE, overlap = WINDOW_OVERLAP;
        if (nblocks > HiC_poss_block + overlap) {
            this->phase_HiC_recursive(chromo_phaser, connected_comp);
        }
            //direct phase
        else {
            chromo_phaser->phased->clear();
            count = 0;
            //update indexing scheme
            for (auto blk_start_id: connected_comp) {
                chromo_phaser->phased->current_window_idxes.push_back(blk_start_id);
                chromo_phaser->phased->mat2variant_index[count] = blk_start_id;
                ptr_ResultforSingleVariant variant = chromo_phaser->results_for_variant[blk_start_id];
                if (is_uninitialized(variant->block)) {
                    chromo_phaser->phased->variant2mat_index[blk_start_id] = count;
                } else {
                    auto blk = variant->block.lock();
                    for (auto _var_id : blk->variant_idxes) {
                        chromo_phaser->phased->variant2mat_index[_var_id] = count;
                    }
                }
                count++;
            }

            spectral->hic_poss_solver(nblocks);
        }
    }

}

void Phaser::phase_HiC_recursive(ChromoPhaser *chromo_phaser, std::set<uint> &connected_comp) {
    int count = 0;
    std::unordered_map<uint, uint> var2id;
    std::unordered_map<uint, uint> id2var;
    std::vector<uint> prev_block_idxes;
    std::map<uint, uint> block_idxes;
    int HiC_poss_block = WINDOW_SIZE, overlap = WINDOW_OVERLAP;
    int n_recursion = 0;

    for (auto blk_start_id: connected_comp) {
        var2id[blk_start_id] = count;
        id2var[count] = blk_start_id;
        block_idxes[count] = count;
        prev_block_idxes.push_back(count);
        count++;
    }
    int prev_blk_count = block_idxes.size();
    while (n_recursion < RECURSIVE_LIMIT || block_idxes.size() != prev_blk_count) {
        //now for each recurssion

        int nblocks = prev_block_idxes.size();

        int phased = 0, rest = nblocks;
        uint start = 0, end = 0;
        uint intend_start = 0;
        uint tt = 0;
        int current_window_size = 0;

        while (rest > 0) {
            chromo_phaser->phased->clear();
            auto i = block_idxes.find(end);
            tt = intend_start;
            uint count = 0;
            //the first window
            current_window_size = 0;

            if (i == block_idxes.begin()) {
                phased = 0;

                if (phased + HiC_poss_block >= prev_block_idxes.size())
                    end = prev_block_idxes.back() + 1;
                else {
                    if (HiC_poss_block + phased + overlap >= prev_block_idxes.size())
                        end = prev_block_idxes.back() + 1;
                    else
                        end = prev_block_idxes[phased + HiC_poss_block + overlap];
                }
                intend_start = intend_start + HiC_poss_block + overlap;
            } else {
                //determine true start

                while (count < overlap) {
                    count++;

                    if (i == block_idxes.begin())
                        break;
                    i = prev(i);
                }
                start = i->first;
                if (phased + HiC_poss_block - overlap >= prev_block_idxes.size())
                    end = prev_block_idxes.back() + 1;
                else {
                    if (phased + HiC_poss_block >= prev_block_idxes.size())
                        end = prev_block_idxes.back() + 1;
                    else
                        end = prev_block_idxes[HiC_poss_block + phased];
                }
                intend_start = intend_start + HiC_poss_block;
            }

            auto it = i;

            for (; it != block_idxes.end() && it->first != end; it++, current_window_size++) {

                uint blk_idx = id2var[it->first];
                ptr_PhasedBlock blk = chromo_phaser->phased->blocks[blk_idx];
                if (blk->size() == 1)
                    if (blk->results.begin()->second->get_filter() == filter_type::POOLRESULT)
                        continue;
                for (auto idx : blk->variant_idxes)
                    chromo_phaser->phased->variant2mat_index[idx] = current_window_size;
                chromo_phaser->phased->mat2variant_index[current_window_size] = blk_idx;
                chromo_phaser->phased->current_window_idxes.push_back(blk_idx);
            }

            phased += (end - tt);
            rest = nblocks - phased;

            //now do poss phasing
            spectral->hic_poss_solver(chromo_phaser->phased->current_window_idxes.size());

            //now update the index
            for (auto it : chromo_phaser->phased->current_window_idxes) {
                //its been phased! update index accordingly
                if (chromo_phaser->phased->block_idxes.count(it) == 0) {
                    block_idxes.erase(var2id[it]);
                }
            }
        }

        //update index after the recurssion
        std::unordered_map<uint, uint> _var2id;
        std::unordered_map<uint, uint> _id2var;
        std::vector<uint> _prev_block_idxes;
        std::map<uint, uint> _block_idxes;

        count = 0;
        for (auto i : block_idxes) {
            _block_idxes[count] = count;
            _prev_block_idxes.push_back(count);
            uint var = id2var[i.first];
            _var2id[var] = count;
            _id2var[count] = var;
            count++;
        }
        prev_blk_count = block_idxes.size();
        prev_block_idxes = _prev_block_idxes;
        block_idxes = _block_idxes;
        var2id = _var2id;
        id2var = _id2var;

        n_recursion++;
    }
}

inline int popcount64_012(uint64 i) {
    if (i == 0) return 0;
    else if ((i & (i - 1ULL)) == 0) return 1;
    else return 2;
}

uint Phaser::getGenoType0123(uint64 m64j, uint64 n) const {
    uint64 j = m64j & 63;
    uint64_masks bits = haploBitsT->tar_bits[m64j / 64 * haploBitsT->Ntarget + n];
    if (bits.is0 & (1ULL << j)) return 0;
    if (bits.is2 & (1ULL << j)) return 2;
    if (bits.is9 & (1ULL << j)) return 3;
    return 1;
}

std::vector<uint> Phaser::findBestHaps(uint n0, uint K, bool useTargetHaps) {
//    std::ofstream bestK_with_score;
//    std::ofstream bestK_with_hom_seq;
//    std::ofstream bestK_with_block_scores;
//    bestK_with_score.open("bestK_with_score.txt", std::ios::out);
//    bestK_with_hom_seq.open("bestK_with_hom_seq.txt", std::ios::out);
//    bestK_with_block_scores.open("bestK_with_block_scores.txt", std::ios::out);
    uint ignored_n0 = refCohortFlag == 1 ? haploBitsT->Nref : n0;
    std::map<uint, std::map<uint, int>> nHapsIdScoreMap;
    uint64 Nhaps =
            2 * ((haploBitsT->Nref == 0 || useTargetHaps) ? haploBitsT->Nref + haploBitsT->Ntarget : haploBitsT->Nref);
    if (K > Nhaps) K = Nhaps;
    /**
     * in bestHaps we store the index of hap upper to Nhaps
     */
    std::vector<uint> bestHaps;
    bestHaps.reserve(K);
    if (K == Nhaps) {
        for (uint64 nHap = 0; nHap < Nhaps; nHap++)
            if (nHap / 2 != ignored_n0)
                bestHaps.push_back(nHap);
    } else {
        std::vector<std::pair<uint, uint> > hapErrInds(Nhaps);
        std::vector<uint64_masks> genoBitsT(haploBitsT->Mseg64);
        for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; m64++)
            genoBitsT[m64] = haploBitsT->tar_bits[m64 * haploBitsT->Ntarget + n0];
        for (uint64 nHap = 0; nHap < Nhaps; nHap++) {
            uint numErrs = (nHap / 2 == ignored_n0 ? 1000000 : 0);
            for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; m64++) {
                uint64 is1 = (nHap & 1) ? haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is2
                                        : haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is0;
                uint64 wrongBits = (genoBitsT[m64].is0 & is1) | (genoBitsT[m64].is2 & ~is1);
                numErrs += popcount64_012(wrongBits);
            }
//            bestK_with_hom_seq << "ref id = " << nHap << std::endl;
            int total_padding_count = 0;
            if (nHapsIdScoreMap.count(nHap) == 0) {
                std::map<uint, int> tmp_map;
                nHapsIdScoreMap.insert(std::make_pair(nHap, tmp_map));
            }
            for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; ++m64) {
                total_padding_count += haploBitsT->block_padding_counts[m64];
                int cur_block_start_snp = m64 * 64 - total_padding_count;
                uint64 is1 = (nHap & 1) ? haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is2
                                        : haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is0;
                uint64 wrongBits = (genoBitsT[m64].is0 & is1) | (genoBitsT[m64].is2 & ~is1);
                int cur_err = popcount64_012(wrongBits);
                if (nHapsIdScoreMap.find(nHap)->second.count(cur_block_start_snp) == 0) {
                    nHapsIdScoreMap.find(nHap)->second.insert(std::make_pair(cur_block_start_snp, cur_err));
                }
                for (uint64 m64j = 0; m64j < haploBitsT->effectiveSNPSites[m64].size(); ++m64j) {
                    if (!haploBitsT->effectiveSNPSites[m64][m64j]) {
                        break;
                    }
                    if (nHap & 1) {
                        int cur_bit = (haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is2 >> (m64j & 63));
                        cur_bit = cur_bit & 1;
//                        bestK_with_hom_seq << cur_bit;
                    } else {
                        int cur_bit = (haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is0 >> (m64j & 63));
                        cur_bit = cur_bit & 1;
//                        bestK_with_hom_seq << cur_bit;
                    }
                }
            }
//            bestK_with_hom_seq << std::endl;
            hapErrInds[nHap].first = numErrs;
            hapErrInds[nHap].second = nHap;
        }
        std::sort(hapErrInds.begin(), hapErrInds.end());
//        printf("find best %d haps for sampleNo %d\n", K, n0);
        for (uint k = 0; k < K; k++)
            if (hapErrInds[k].second / 2 != ignored_n0) {
                bestHaps.push_back(hapErrInds[k].second);
//                bestK_with_score << "id = " << hapErrInds[k].second << ", score = " << hapErrInds[k].first << std::endl;
//                bestK_with_block_scores << "ref id = " << hapErrInds[k].second << std::endl;
//                bestK_with_block_scores << "------------------------------" << std::endl;
                std::map<uint, int> map = nHapsIdScoreMap.find(hapErrInds[k].second)->second;
                for (auto i = map.begin(); i != map.end(); ++i) {
//                    bestK_with_block_scores << "block start snp id = " << i->first << ", block score = " << i->second << std::endl;
                }
//                printf("%d : %d\n", hapErrInds[k].second, hapErrInds[k].first);
            }
    }
    return bestHaps;
}

HaploBitsArrays::CompressedHaploBitsT *Phaser::constructHapBitsT(uint64 n0, uint K, bool useTargetHaps) {
    const std::vector<uint> &bestHaps = findBestHaps(n0, K, useTargetHaps);
//    std::ofstream best_k("bestK_ids.txt", std::ios::out);
//    for (int i = 0; i < bestHaps.size(); ++i) {
//        best_k << bestHaps[i] << std::endl;
//    }
    std::vector<uchar> genos64j(haploBitsT->Mseg64 * 64);
    std::vector<uint64> hets64j, refMonoHets64j;
    for (uint i = 0; i < haploBitsT->Mseg64 * 64; ++i) {
        genos64j[i] = getGenoType0123(i, n0);
        if (haploBitsT->effectiveSNPSites[i / 64][i & 63] && genos64j[i] == 1) {
            hets64j.push_back(i);
        }
    }
    std::vector<uint64_masks> tgtGenoBits(haploBitsT->Mseg64);
    for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; m64++)
        tgtGenoBits[m64] = haploBitsT->tar_bits[m64 * haploBitsT->Ntarget + n0];
    /// this might not be necessary
    std::vector<uint64> splits64j;
    /// storing the genotypes at the het sites (padded on left and right to match hapBitsT)
    std::vector<uchar> hetGenos;
    hetGenos.push_back(0);
    for (uint i = 0; i < hets64j.size(); ++i) {
        hetGenos.push_back(genos64j[hets64j[i]]);
    }
    hetGenos.push_back(0);

    std::vector<double> cMcoords(hets64j.size() + 2);
    for (uint i = 0; i <= hets64j.size(); ++i) {
        uint64 hetStart = (i == 0 ? 0 : hets64j[i - 1]);
        uint64 hetStop = (i == hets64j.size() ? haploBitsT->Mseg64 * 64 : hets64j[i]);
        cMcoords[i] = haploBitsT->cms64j[hetStart];
        cMcoords[i + 1] = haploBitsT->cms64j[hetStop];
    }
    std::cout << "n0 = " << n0 << "\t" << "hets64j.size = " << hets64j.size() << std::endl;
    std::cout << "n0 = " << n0 << "\t" << "cMcoords.size = " << cMcoords.size() << std::endl;

    /// build radixTree data structure
    return new HaploBitsArrays::CompressedHaploBitsT(haploBitsT->ref_bits, haploBitsT->Mseg64, haploBitsT->Nref,
                                                     hets64j, hetGenos, tgtGenoBits, bestHaps, cMcoords);
}

void Phaser::set_blk(const char *contig, ChromoPhaser *chromo_phaser, VCFReader &frvcf) {
    bcf1_t *record = bcf_init();
    uint blk_count = 0;
    int gap_count = 0;
    std::unordered_map<ptr_PhasedBlock, uint> encountered_phased_block;
    frvcf.jump_to_contig(frvcf.curr_contig);
    std::unordered_map<uint, uint> idx2pos;
    uint idxx = 0;
    for (uint idx = 0; idx < frvcf.snp_pos_container.size(); idx++) {
        frvcf.get_next_record(record);
        while (std::find(frvcf.snp_pos_container.begin(), frvcf.snp_pos_container.end(), record->pos) ==
               frvcf.snp_pos_container.end()) { frvcf.get_next_record(record); }

//        const char * filter = filter_map.find(resultforSingleVariant->get_filter())->second.data();
        int ref;
        int alt;
        int32_t *gt_arr = NULL, ngt_arr = 0;
        bcf_get_genotypes(frvcf.header, record, &gt_arr, &ngt_arr);
        int32_t *ptr = gt_arr;
        int allele0 = bcf_gt_allele(ptr[0]);
        int allele1 = bcf_gt_allele(ptr[1]);
        int temp = bcf_alleles2gt(allele0, allele1);
        bcf_gt2alleles(temp, &ref, &alt);
//        std::cout << ref << alt << std::endl;

        if (std::find(frvcf.snp_pos_container.begin(), frvcf.snp_pos_container.end(), record->pos) !=
            frvcf.snp_pos_container.end()) {

//            if (std::find(frvcf.het_pos_container.begin(), frvcf.het_pos_container.end(), record->pos) !=
//                frvcf.het_pos_container.end()) {
//                idx2pos[idxx] = record->pos;
//                ptr_ResultforSingleVariant resultforSingleVariant = chromo_phaser->results_for_variant[idxx];
            if (std::find(frvcf.het_pos_container.begin(), frvcf.het_pos_container.end(), record->pos) !=
                frvcf.het_pos_container.end()) {
                idx2pos[idxx] = record->pos;
                ptr_ResultforSingleVariant resultforSingleVariant = chromo_phaser->results_for_variant[idxx];
                bcf_translate(frvcf.header, frvcf.header, record);

                if (!is_uninitialized(resultforSingleVariant->block) && resultforSingleVariant->variant_phased() &&
                    resultforSingleVariant->get_filter() == filter_type::PASS) {
                    if (gap_count >= 30) {
                        blk_count++;
                    }
                    ptr_PhasedBlock block = resultforSingleVariant->block.lock();
                    if (block.get() == nullptr) {
                        set_het(record, resultforSingleVariant, 0);
                        continue;
                    }
                    //already met
                    if (encountered_phased_block.count(block) != 0) {
                        uint blk_no = encountered_phased_block[block];
                        set_het(record, resultforSingleVariant, idx2pos[block->start_variant_idx]);
                    } else {
                        encountered_phased_block.emplace(block, ++blk_count);
                        set_het(record, resultforSingleVariant, idx2pos[block->start_variant_idx]);
                    }

                    gap_count = 0;
                } else {
                    gap_count++;
                    set_het(record, resultforSingleVariant, 0);
                }
                idxx++;
//                }

            } else {
//                std::map<uint, std::vector<uint>> snp_tmp;
//                std::vector<uint> hap_info;
//                hap_info.emplace_back(record->pos);
//                hap_info.emplace_back(allele0);
//                hap_info.emplace_back(allele1);
//                snp_tmp[0] = hap_info;
//                block_id_map.emplace_back(snp_tmp);
            }
        }

    }
}

void Phaser::set_het(bcf1_t *record, ptr_ResultforSingleVariant resultforSingleVariant, const unsigned int blk_no) {
    int ref;
    int alt;
    int32_t *gt_arr = NULL, ngt_arr = 0;
    bcf_get_genotypes(tarVcf->header, record, &gt_arr, &ngt_arr);
    int32_t *ptr = gt_arr;
    int allele0 = bcf_gt_allele(ptr[0]);
    int allele1 = bcf_gt_allele(ptr[1]);
    int temp = bcf_alleles2gt(allele0, allele1);;
    bcf_gt2alleles(temp, &ref, &alt);

    std::map<uint, std::vector<uint>> snp_tmp;
    std::vector<uint> hap_info;
    hap_info.emplace_back(record->pos);
    hap_info.emplace_back(allele0);
    hap_info.emplace_back(allele1);
    snp_tmp[blk_no] = hap_info;
    block_id_map.emplace_back(snp_tmp);
}

void Phaser::get_blk() {

    std::vector<uint> all_snp;
    std::vector<uint> het_snp;
//    std::ofstream out7("blk_id_map.txt", std::ios::out);
    for (uint b = 0; b < block_id_map.size(); b++) {
//        out7 << block_id_map[b].begin()->first << '\t' << block_id_map[b].begin()->second[0] << '\t'
//             << block_id_map[b].begin()->second[1] << block_id_map[b].begin()->second[2] << std::endl;
        all_snp.emplace_back(block_id_map[b].begin()->second[0]);
    }
    het_snp = this->tarVcf->het_pos_container;
    all_snp = this->tarVcf->snp_pos_container;

    uint pre_idx = -1;
    for (uint id = 0; id < block_id_map.size(); id++) {
        if (block_id_map[id].begin()->first == 0)
            continue;
        if (hap_map.find(block_id_map[id].begin()->first) == hap_map.end()) {
            Block blk; // first blk
            hap_map[block_id_map[id].begin()->first] = blk;
        }
    }

    for (uint idx = 0; idx < block_id_map.size(); idx++) {
        uint cur_idx = block_id_map[idx].begin()->first;
        if (cur_idx != pre_idx && cur_idx != 0) {
            hap_map[cur_idx].hap1.emplace_back(block_id_map[idx].begin()->second[1]);
            hap_map[cur_idx].hap2.emplace_back(block_id_map[idx].begin()->second[2]);
            hap_map[cur_idx].hap_range_idx.emplace_back(block_id_map[idx].begin()->second[0]);
            auto res_snp = find(all_snp.begin(), all_snp.end(), block_id_map[idx].begin()->second[0]);
            int index_snp = distance(all_snp.begin(), res_snp);
            hap_map[cur_idx].snp_idx.emplace_back(index_snp);
            auto res_het = find(het_snp.begin(), het_snp.end(), block_id_map[idx].begin()->second[0]);
            int index_het = distance(het_snp.begin(), res_het);
            hap_map[cur_idx].het_idx.emplace_back(index_het);
            hap_map[cur_idx].blk_idx = cur_idx;
        } else if (cur_idx != pre_idx && cur_idx == 0 && idx != 0) {
            hap_map[pre_idx].hap1.emplace_back(block_id_map[idx].begin()->second[1]);
            hap_map[pre_idx].hap2.emplace_back(block_id_map[idx].begin()->second[2]);
            hap_map[pre_idx].hap_range_idx.emplace_back(block_id_map[idx].begin()->second[0]);
            auto res_snp = find(all_snp.begin(), all_snp.end(), block_id_map[idx].begin()->second[0]);
            int index_snp = distance(all_snp.begin(), res_snp);
            hap_map[pre_idx].snp_idx.emplace_back(index_snp);
            auto res_het = find(het_snp.begin(), het_snp.end(), block_id_map[idx].begin()->second[0]);
            int index_het = distance(het_snp.begin(), res_het);
            hap_map[pre_idx].het_idx.emplace_back(index_het);
            hap_map[pre_idx].blk_idx = pre_idx;

        } else if (cur_idx == pre_idx && cur_idx != 0) {
            hap_map[cur_idx].hap1.emplace_back(block_id_map[idx].begin()->second[1]);
            hap_map[cur_idx].hap2.emplace_back(block_id_map[idx].begin()->second[2]);
            hap_map[cur_idx].hap_range_idx.emplace_back(block_id_map[idx].begin()->second[0]);
            auto res_snp = find(all_snp.begin(), all_snp.end(), block_id_map[idx].begin()->second[0]);
            int index_snp = distance(all_snp.begin(), res_snp);
            hap_map[cur_idx].snp_idx.emplace_back(index_snp);
            auto res_het = find(het_snp.begin(), het_snp.end(), block_id_map[idx].begin()->second[0]);
            int index_het = distance(het_snp.begin(), res_het);
            hap_map[cur_idx].het_idx.emplace_back(index_het);
            hap_map[cur_idx].blk_idx = cur_idx;

        } else {
            continue;
        }
        if (cur_idx != 0) {
            pre_idx = cur_idx;
        }
    }


}

void Phaser::link_blk() {
//    std::ofstream out6("VALID_SNP.txt", std::ios::out);
//    for (auto &h : transed_hap_map) {
//        // check
//        for (uint i = 0; i < h.second.phased_het_idx.size(); i++) {
//            out6 << h.first << '\t' << h.second.hap_range_idx[h.second.phased_het_idx[i]] << '\t' << h.second.pseudo_hap1[i] << h.second.pseudo_hap2[i]
//                 << std::endl;
//        }
//    }
    uint phase_win = 3;
    recursion();
}

void Phaser::recursion() {
    uint phase_win = 4;
    std::vector<uint> blk_keys;
    // get all blk idx
    for (auto &hp : transed_hap_map) {
//        if (hp.first == 0) { continue; }
        blk_keys.emplace_back(hp.first);
    }
    // divide into sub blk
    std::vector<std::vector<uint>> group_keys;
    std::vector<uint> group_key_tmp;
    uint count = 0;
    for (uint i = 0; i < blk_keys.size(); i++) {
        group_key_tmp.emplace_back(blk_keys[i]);
        count++;
        if (count >= phase_win || (group_key_tmp.back() == transed_hap_map.end()->first)) {
            group_keys.emplace_back(group_key_tmp);
            group_key_tmp.clear();
            count = 0;
        }
    }
    for (uint b = 0; b < group_keys.size(); b++) {
//        std::cout << "group key b size = " << group_keys[b].size() << std::endl;
        if (group_keys[b].size() <= 1) {
            continue;
        }
        // init pos_neg_idx
        std::vector<std::string> pos_neg_idx;
        std::vector<std::string> blk_order;
        for (uint k = 0; k < group_keys[b].size(); k++) {

            pos_neg_idx.emplace_back(std::to_string(group_keys[b][k]) + "+");
            pos_neg_idx.emplace_back(std::to_string(group_keys[b][k]) + "-");
        }
        // cons graph
//        std::cout << "hapsRef size = " << pos_neg_idx.size() << std::endl;

        blk_order = spectral->call_haplotype_v3(pos_neg_idx, group_keys[b], transed_hap_map);
    }
    if (transed_hap_map.size() > 4) {
        recursion();
    }
}

void Phaser::recursion_v2() {

    uint phase_win = 4;
    std::vector<uint> blk_keys;
    // get all blk idx
    for (auto &hp : transed_hap_map) {
        if (hp.first == 0) { continue; }
        blk_keys.emplace_back(hp.first);
    }
//    // get adjacent longest n blks
//    uint max_length = 0;
//    for (int i=0; i< blk_keys.size(); i=i+phase_win){
//        uint tmp_length = 0;
//        for (int l=0; l<phase_win; l++)
//        {
//            tmp_length=tmp_length + transed_hap_map[blk_keys[i+l]].hap1.size();
//        }
//        if (tmp_length> max_length){
//            max_length = tmp_length;
//        }
//    }
    // divide into sub blk
    std::vector<std::vector<uint>> group_keys;
    std::vector<uint> group_key_tmp;
    uint count = 0;
    for (uint i = 0; i < blk_keys.size(); i++) {
        group_key_tmp.emplace_back(blk_keys[i]);
        count++;
        if (count >= phase_win || (group_key_tmp.back() == transed_hap_map.end()->first)) {
            group_keys.emplace_back(group_key_tmp);
            group_key_tmp.clear();
            count = 0;
        }
    }
    for (uint b = 0; b < group_keys.size(); b++) {
        // init pos_neg_idx
        std::vector<std::string> pos_neg_idx;
        std::vector<std::string> blk_order;
        for (uint k = 0; k < group_keys[b].size(); k++) {

            pos_neg_idx.emplace_back(std::to_string(group_keys[b][k]) + "+");
            pos_neg_idx.emplace_back(std::to_string(group_keys[b][k]) + "-");
        }
        // cons graph
//        std::cout << "hapsRef size = " << hapsRef.size() << std::endl;
        blk_order = spectral->call_haplotype_v3(pos_neg_idx, group_keys[b], transed_hap_map);
    }
    if (transed_hap_map.size() > 4) {
        recursion_v2();
    }


}

void Phaser::transform_hapmap(int size) {
    for (auto &hp : hap_map) {
        Block blk;
        uint count = 0;
        for (uint i = 0; i < hp.second.hap_range_idx.size(); i++) {
            blk.hap_range_idx.emplace_back(hp.second.hap_range_idx[i]);
            blk.het_idx.emplace_back(hp.second.het_idx[i]);
            blk.snp_idx.emplace_back(hp.second.snp_idx[i]);
            blk.hap1.emplace_back(hp.second.hap1[i]);
            blk.hap2.emplace_back(hp.second.hap2[i]);
            count++;
            if (count == 1) {
                blk.blk_idx = blk.hap_range_idx.front();
            }
            if (count == size || blk.hap_range_idx.back() == hp.second.hap_range_idx.back()) {
                transed_hap_map[blk.blk_idx] = blk;
                blk.hap_range_idx.clear();
                blk.het_idx.clear();
                blk.snp_idx.clear();
                blk.hap1.clear();
                blk.hap2.clear();
                count = 0;
            }
            // fix: if last one ele just has 1 snp;
        }
    }

}

void Phaser::correct_hap() {

}

void Phaser::check_phased() {
    bcf1_t *record = bcf_init();
    uint blk_count = 0;
    int gap_count = 0;
    std::unordered_map<ptr_PhasedBlock, uint> encountered_phased_block;
    tarVcf->jump_to_contig(tarVcf->curr_contig);
    std::unordered_map<uint, uint> idx2pos;
    uint idxx = 0;
    std::ofstream out20("check_phase.txt", std::ios::out);
    for (uint idx = 0; idx < tarVcf->snp_pos_container.size(); idx++) {
        tarVcf->get_next_record(record);
        while (std::find(tarVcf->snp_pos_container.begin(), tarVcf->snp_pos_container.end(), record->pos) ==
               tarVcf->snp_pos_container.end()) { tarVcf->get_next_record(record); }
        if (std::find(tarVcf->het_pos_container.begin(), tarVcf->het_pos_container.end(), record->pos) !=
            tarVcf->het_pos_container.end()) {
            int ref;
            int alt;
            int32_t *gt_arr = NULL, ngt_arr = 0;
            bcf_get_genotypes(tarVcf->header, record, &gt_arr, &ngt_arr);
            int32_t *ptr = gt_arr;
            int allele0 = bcf_gt_allele(ptr[0]);
            int allele1 = bcf_gt_allele(ptr[1]);
            int temp = bcf_alleles2gt(allele0, allele1);;
            bcf_gt2alleles(temp, &ref, &alt);
            out20 << record->pos << "\t" << allele0 << "\t" << allele1 << std::endl;
        }
    }
}

void Phaser::resize_blk_id_map() {

    std::vector<uint> all_snp;
    std::vector<uint> het_snp;
    std::ofstream out7("blk_id_map.txt", std::ios::out);
    for (uint b = 0; b < block_id_map.size(); b++) {
        out7 << block_id_map[b].begin()->first << '\t' << block_id_map[b].begin()->second[0] << '\t'
             << block_id_map[b].begin()->second[1] << block_id_map[b].begin()->second[2] << std::endl;
        all_snp.emplace_back(block_id_map[b].begin()->second[0]);
    }
    het_snp = this->tarVcf->het_pos_container;
    all_snp = this->tarVcf->snp_pos_container;

    uint pre_idx = -1;
    for (uint id = 0; id < block_id_map.size(); id++) {
        if (block_id_map[id].begin()->first == 0)
            continue;
        if (hap_map.find(block_id_map[id].begin()->first) == hap_map.end()) {
            Block blk; // first blk
            hap_map[block_id_map[id].begin()->first] = blk;
        }
    }
    uint var_count = WINDOW_SIZE;
    Block blk;
    uint cur_blk_start;
    for (int i = 0; i < block_id_map.size(); i++) {
        if (var_count == WINDOW_SIZE) {
            hap_map[block_id_map[i].begin()->second[0]] = blk;
            var_count = 0;
            hap_map[block_id_map[i].begin()->second[0]].hap_range_idx.clear();
            hap_map[block_id_map[i].begin()->second[0]].hap1.clear();
            hap_map[block_id_map[i].begin()->second[0]].hap2.clear();
            hap_map[block_id_map[i].begin()->second[0]].het_idx.clear();
            hap_map[block_id_map[i].begin()->second[0]].snp_idx.clear();
            hap_map[block_id_map[i].begin()->second[0]].blk_idx = block_id_map[i].begin()->second[0];
            cur_blk_start = block_id_map[i].begin()->second[0];
        }

        if (cur_blk_start % 2 == 0) {
            hap_map[cur_blk_start].hap_range_idx.emplace_back(block_id_map[i].begin()->second[0]);
            hap_map[cur_blk_start].hap2.emplace_back(block_id_map[i].begin()->second[1]);
            hap_map[cur_blk_start].hap1.emplace_back(block_id_map[i].begin()->second[2]);
            auto res_snp = find(all_snp.begin(), all_snp.end(), block_id_map[i].begin()->second[0]);
            int index_snp = distance(all_snp.begin(), res_snp);
            hap_map[cur_blk_start].snp_idx.emplace_back(index_snp);
            auto res_het = find(het_snp.begin(), het_snp.end(), block_id_map[i].begin()->second[0]);
            int index_het = distance(het_snp.begin(), res_het);
            hap_map[cur_blk_start].het_idx.emplace_back(index_het);
            var_count++;
        } else {
            hap_map[cur_blk_start].hap_range_idx.emplace_back(block_id_map[i].begin()->second[0]);
            hap_map[cur_blk_start].hap1.emplace_back(block_id_map[i].begin()->second[1]);
            hap_map[cur_blk_start].hap2.emplace_back(block_id_map[i].begin()->second[2]);
            auto res_snp = find(all_snp.begin(), all_snp.end(), block_id_map[i].begin()->second[0]);
            int index_snp = distance(all_snp.begin(), res_snp);
            hap_map[cur_blk_start].snp_idx.emplace_back(index_snp);
            auto res_het = find(het_snp.begin(), het_snp.end(), block_id_map[i].begin()->second[0]);
            int index_het = distance(het_snp.begin(), res_het);
            hap_map[cur_blk_start].het_idx.emplace_back(index_het);
            var_count++;
        }
    }

    std::ofstream out6("VALID_SNP.txt", std::ios::out);
    for (auto &h : hap_map) {
        // check
        for (uint i = 0; i < h.second.hap_range_idx.size(); i++) {
            out6 << h.first << '\t' << h.second.hap_range_idx[i] << '\t' << h.second.hap1[i] << h.second.hap2[i]
                 << std::endl;
        }
    }
}

void Phaser::transform_hapmap_v2() {
    // remove empty blocks and one snp blocks
    for (auto iter1 = transed_hap_map.begin(); iter1 != transed_hap_map.end();) {
        if (iter1->second.phased_het_idx.size() <= 1) {
            transed_hap_map.erase(iter1++); //#1
        } else {
            ++iter1;
        }
    }
}

void Phaser::recursive_set_hap_map(HaploBitsArrays::CompressedHaploBitsT &hapBitsT, std::map<uint, Block> &empty_blks) {


    std::map<uint, Block> tmp_empty_hap_map;
    hapBitsT.phasingInBlock(empty_blks, WINDOW_SIZE);
    get_empty_split_blks(empty_blks, tmp_empty_hap_map);
//        split_blk(tmp_empty_hap_map);
    add_phased_to_hap_map(empty_blks);
    if (tmp_empty_hap_map.begin()->second.hap1.size() == 1) {
        int total_blank_site = 0;
        for (auto i = tmp_empty_hap_map.begin(); i != tmp_empty_hap_map.end(); ++i) {
            total_blank_site += i->second.het_idx.size();
        }
        std::cout << "total blank blk count = " << tmp_empty_hap_map.size() << ", total blank size_2 empty snp count = "
                  << total_blank_site << std::endl;
        recursive_complete_empty_blk_flag = false;
    }
    while (recursive_complete_empty_blk_flag) {
        recursive_set_hap_map(hapBitsT, tmp_empty_hap_map);
    }

}


void
Phaser::get_empty_split_blks(std::map<uint, Block> &partial_phased_blks, std::map<uint, Block> &tmp_empty_hap_map) {

    for (auto &partial_phased_blk : partial_phased_blks) {
        if (partial_phased_blk.second.pseudo_hap1.empty() || partial_phased_blk.second.pseudo_hap1.size() == 1) {
            // right
            uint const half_size = partial_phased_blk.second.hap_range_idx.size() / 2;
            Block right_blk;
            right_blk.blk_idx = partial_phased_blk.second.hap_range_idx[half_size - 1];
            std::vector<uint> right_hap1(partial_phased_blk.second.hap1.begin() + half_size,
                                         partial_phased_blk.second.hap1.end());
            right_blk.hap1 = right_hap1;
            std::vector<uint> right_hap2(partial_phased_blk.second.hap2.begin() + half_size,
                                         partial_phased_blk.second.hap2.end());
            right_blk.hap2 = right_hap2;
            std::vector<uint> right_hap_range_idx(partial_phased_blk.second.hap_range_idx.begin() + half_size,
                                                  partial_phased_blk.second.hap_range_idx.end());
            right_blk.hap_range_idx = right_hap_range_idx;
            std::vector<uint> right_snp_idx(partial_phased_blk.second.snp_idx.begin() + half_size,
                                            partial_phased_blk.second.snp_idx.end());
            right_blk.snp_idx = right_snp_idx;
            std::vector<uint> right_het_idx(partial_phased_blk.second.het_idx.begin() + half_size,
                                            partial_phased_blk.second.het_idx.end());
            right_blk.het_idx = right_het_idx;
            tmp_empty_hap_map[partial_phased_blk.second.hap_range_idx[half_size - 1]] = right_blk;
            // left
            tmp_empty_hap_map[partial_phased_blk.first] = partial_phased_blk.second;
            tmp_empty_hap_map[partial_phased_blk.first].hap_range_idx.erase(
                    tmp_empty_hap_map[partial_phased_blk.first].hap_range_idx.begin() + half_size,
                    tmp_empty_hap_map[partial_phased_blk.first].hap_range_idx.begin() + half_size * 2);
            tmp_empty_hap_map[partial_phased_blk.first].hap1.erase(
                    tmp_empty_hap_map[partial_phased_blk.first].hap1.begin() + half_size,
                    tmp_empty_hap_map[partial_phased_blk.first].hap1.begin() + half_size * 2);
            tmp_empty_hap_map[partial_phased_blk.first].hap2.erase(
                    tmp_empty_hap_map[partial_phased_blk.first].hap2.begin() + half_size,
                    tmp_empty_hap_map[partial_phased_blk.first].hap2.begin() + half_size * 2);
            tmp_empty_hap_map[partial_phased_blk.first].snp_idx.erase(
                    tmp_empty_hap_map[partial_phased_blk.first].snp_idx.begin() + half_size,
                    tmp_empty_hap_map[partial_phased_blk.first].snp_idx.begin() + half_size * 2);
            tmp_empty_hap_map[partial_phased_blk.first].het_idx.erase(
                    tmp_empty_hap_map[partial_phased_blk.first].het_idx.begin() + half_size,
                    tmp_empty_hap_map[partial_phased_blk.first].het_idx.begin() + half_size * 2);
        }
    }
}


void Phaser::add_phased_to_hap_map(std::map<uint, Block> &partial_phased_blks) {
    for (auto &partial_phased_blk : partial_phased_blks) {
        if (!partial_phased_blk.second.pseudo_hap1.empty()) {
            phased_hap_map[partial_phased_blk.first] = partial_phased_blk.second;
        }
    }
}

void Phaser::get_empty_phased_blks() {
    //
    for (auto &iter : hap_map) {
        if (iter.second.pseudo_hap1.empty() || iter.second.pseudo_hap1.size() == 1) {
            empty_hap_map[iter.first] = iter.second;
        } else {
            phased_hap_map[iter.first] = iter.second;
        }
    }
}

std::vector<uint> Phaser::findLocalBestHaps(uint n0, uint K, bool useTargetHaps, int startPos, int endPos) {
    uint64 Nhaps =
            2 * ((haploBitsT->Nref == 0 || useTargetHaps) ? haploBitsT->Nref + haploBitsT->Ntarget : haploBitsT->Nref);
    if (K > Nhaps) K = Nhaps;
    /**
     * in bestHaps we store the index of hap upper to Nhaps
     */
    std::vector<uint> bestHaps;
    bestHaps.reserve(K);
    if (K == Nhaps) {
        for (uint64 nHap = 0; nHap < Nhaps; nHap++)
            if (nHap / 2 != n0)
                bestHaps.push_back(nHap);
    } else {
        std::vector<std::pair<uint, uint> > hapErrInds(Nhaps);
        std::vector<uint64_masks> genoBitsT(haploBitsT->Mseg64);
        for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; m64++)
            genoBitsT[m64] = haploBitsT->tar_bits[m64 * haploBitsT->Ntarget + n0 - haploBitsT->Nref];
        for (uint64 nHap = 0; nHap < Nhaps; nHap++) {
            int total_padding = 0;
            uint numErrs = (nHap / 2 == n0 ? 1000000 : 0);
            for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; m64++) {
                total_padding += haploBitsT->block_padding_counts[m64];
                if (m64 < (startPos + total_padding) / 64) {
                    continue;
                } else if (m64 > (endPos + total_padding) / 64) {
                    break;
                }
                uint64 is1 = (nHap & 1) ? haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is2
                                        : haploBitsT->ref_bits[m64 * haploBitsT->Nref + (nHap / 2)].is0;
                uint64 wrongBits = (genoBitsT[m64].is0 & is1) | (genoBitsT[m64].is2 & ~is1);
                numErrs += popcount64_012(wrongBits);
            }
            hapErrInds[nHap].first = numErrs;
            hapErrInds[nHap].second = nHap;
        }
        std::sort(hapErrInds.begin(), hapErrInds.end());
//        printf("find best %d haps for sampleNo %d\n", K, n0);
        for (uint k = 0; k < K; k++)
            if (hapErrInds[k].second / 2 != n0) {
                bestHaps.push_back(hapErrInds[k].second);
//                printf("%d : %d\n", hapErrInds[k].second, hapErrInds[k].first);
            }
    }
    return bestHaps;
}

HaploBitsArrays::CompressedHaploBitsT *
Phaser::constructLocalHapBitsT(uint64 n0, uint K, bool useTargetHaps, int startPos, int endPos) {
    const std::vector<uint> &bestHaps = findLocalBestHaps(n0, K, useTargetHaps, startPos, endPos);
    std::vector<uchar> genos64j(haploBitsT->Mseg64 * 64);
    std::vector<uint64> hets64j, refMonoHets64j;
    for (uint i = 0; i < haploBitsT->Mseg64 * 64; ++i) {
        genos64j[i] = getGenoType0123(i, n0 - haploBitsT->Nref);
        if (haploBitsT->effectiveSNPSites[i / 64][i & 63] && genos64j[i] == 1) {
            hets64j.push_back(i);
        }
    }
    std::vector<uint64_masks> tgtGenoBits(haploBitsT->Mseg64);
    for (uint64 m64 = 0; m64 < haploBitsT->Mseg64; m64++)
        tgtGenoBits[m64] = haploBitsT->tar_bits[m64 * haploBitsT->Ntarget + n0 - haploBitsT->Nref];
    /// this might not be necessary
    std::vector<uint64> splits64j;
    /// storing the genotypes at the het sites (padded on left and right to match hapBitsT)
    std::vector<uchar> hetGenos;
    hetGenos.push_back(0);
    for (uint i = 0; i < hets64j.size(); ++i) {
        hetGenos.push_back(genos64j[hets64j[i]]);
    }
    hetGenos.push_back(0);

    std::vector<double> cMcoords(hets64j.size() + 2);
    for (uint i = 0; i <= hets64j.size(); ++i) {
        uint64 hetStart = (i == 0 ? 0 : hets64j[i - 1]);
        uint64 hetStop = (i == hets64j.size() ? haploBitsT->Mseg64 * 64 : hets64j[i]);
        cMcoords[i] = haploBitsT->cms64j[hetStart];
        cMcoords[i + 1] = haploBitsT->cms64j[hetStop];
    }
//    std::cout << "hets64j.size = " << hets64j.size() << std::endl;
//    std::cout << "cMcoords.size = " << cMcoords.size() << std::endl;

    /// build radixTree data structure
    return new HaploBitsArrays::CompressedHaploBitsT(haploBitsT->ref_bits, haploBitsT->Mseg64, haploBitsT->Nref,
                                                     hets64j, hetGenos, tgtGenoBits, bestHaps, cMcoords);
}


std::string Phaser::find_comp(std::string in_s) {
    std::string res_s;
    for (int i = 0; i < in_s.size(); ++i) {
        if (in_s[i] == '0') {
            res_s += '1';
        } else {
            res_s += '0';
        }
    }
    return res_s;
}


std::string Phaser::comp(std::string hap) {
    auto lambda = [](const char c) {
        switch (c) {
            case '1':
                return '0';
            case '0':
                return '1';
            default:
                return c;
        }
    };
    std::transform(hap.cbegin(), hap.cend(), hap.begin(), lambda);
    return hap;

}

std::map<uint, std::string> Phaser::get_local_ref_map(uint start, uint end, std::vector<std::string> &haps) {
    std::map<uint, std::string> res;
    for (auto &hap : haps) {
        auto index = &hap - &haps[0];
        res[index] = hap.substr(start, end - start);
    }
    return res;
}
//std::map<uint, std::string> Phaser::get_local_ref_map_v2(uint start, uint end, std::vector<std::string> &haps, std::vector<std::string> &best_hom_haps) {
    std::map<uint, std::string> Phaser::get_local_ref_map_v2(uint start, uint end, std::vector<std::string> &haps, std::vector<std::string> &best_hom_haps, std::vector<uint> &ids) {
    std::map<uint, std::string> res;
//    for (auto &id : ids) {
//        auto index = &id - &ids[0];
//        std::string local_seq = haps[id].substr(start, end - start);
//        std::string local_hom_seq = best_hom_haps[id].substr(start, end - start);
//        // check homo region, if it is the same chunk as target
//        if (check_hom(local_seq, index, local_hom_seq)){
//            res[id] = haps[id].substr(start, end - start);
//        }
//    }
    for (auto &hap : haps) {
        auto index = &hap - &haps[0];
        std::string local_seq = hap.substr(start, end - start);
        std::string local_hom_seq = best_hom_haps[index].substr(start, end - start);
        // check homo region, if it is the same chunk as target
        if (check_hom(local_seq, index, local_hom_seq)){
            res[ids[index]] = hap.substr(start, end - start);
        }
    }
    return res;
//    for (auto &hap : haps) {
//        auto index = &hap - &haps[0];
//        std::string local_seq = hap.substr(start, end - start);
//        std::string local_hom_seq = best_hom_haps[index].substr(start, end - start);
//        // check homo region, if it is the same chunk as target
//        if (check_hom(local_seq, index, local_hom_seq)){
//            res[index] = hap.substr(start, end - start);
//        }
//    }
//    return res;
}

bool Phaser::check_hom(std::string &local_seq, uint index, std::string &homo_hap) {
    int err_count = 0;
    for (int i = 0; i < homo_hap.size(); i++) {
        if (i % 2 == 0) {
            if (homo_hap[i] == '1') {
                err_count += 1;
            }
        }
    }
    if (err_count >= 1) {
        return false;
    } else {
        return true;
    }
}

std::vector<std::string> Phaser::get_local_refs(std::map<uint, std::string> &local_ref_map) {
    std::vector<std::string> local_haps;
    for (auto const &local_ref: local_ref_map)
        local_haps.push_back(local_ref.second);
    return local_haps;
}


void Phaser::first_phase_iter(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos) {
    std::vector<std::string> local_refs = get_local_refs(local_ref_map);
    std::set<std::string> unique_local_refs = unique_haps(local_refs);
    std::map<uint, std::string> local_shielded_ref_map;
    std::vector<std::string> local_shielded_refs;
    std::set<std::string> unique_local_shielded_refs;
    std::vector<std::string> visited;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;

//    std::ofstream fout1;
//    fout1.open("check_all_haps.txt");
//
//    for (const std::string &number : local_refs)
//        fout1<<number<<std::endl;
//
//    fout1.close();
//
//    std::ofstream fout;
//    fout.open("check_haps.txt");
//
//    for (const std::string &number : unique_local_refs)
//        fout<<number<<std::endl;
//
//    fout.close();
    if (is_shield) {
        local_shielded_ref_map = get_shielded_local_ref_map(shield_pos, blk.start, blk.end, local_refs);
        local_shielded_refs = get_local_refs(local_shielded_ref_map);
        unique_local_shielded_refs = unique_haps(local_shielded_refs);
//    }
//
//    if (is_shield) {
        uint index_2 = 0;
        for (auto &hap: unique_local_shielded_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
                //            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
                //            }
                cur_comp_res.emplace_back(std::make_pair(index_2, cur_comp));
            }
            visited.emplace_back(hap);
            index_2++;
        }
        visited.clear();
    } else {
        uint index = 0;
        for (auto &hap: unique_local_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
//            if (is_shield) {
//                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
//                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
//            } else {
                cur_comp.cur_count = std::count(local_refs.begin(), local_refs.end(), hap);
                cur_comp.comp_count = std::count(local_refs.begin(), local_refs.end(), cur_comp.comp);
//            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//            if (is_shield) {
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
//            } else {
                cur_comp.cur_id_list = get_ids(hap, local_refs);
                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_refs);
//            }
                cur_comp_res.emplace_back(std::make_pair(index, cur_comp));
            }
            visited.emplace_back(hap);
            index++;
        }
        visited.clear();
    }
    if (cur_comp_res.empty()) {
        blk.tag = "empty";
        blk.cur_comp_res = cur_comp_res;
        return;
    } else if (cur_comp_res.size() == 1) {
        blk.tag = "phased";
        blk.cur_comp_res = cur_comp_res;
//        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
//        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    } else if (cur_comp_res.size() > 1) {
        blk.tag = "reduce_phased";
        sort_comp_res(cur_comp_res);
        blk.cur_comp_res = cur_comp_res;
//        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
//        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    }
    if (is_shield) {
        std::string seq1 = blk.cur_comp_res.begin()->second.cur;
        std::string seq2 = blk.cur_comp_res.begin()->second.comp;
        std::string major_allele;
        if (blk.cur_comp_res.begin()->second.cur_count >= blk.cur_comp_res.begin()->second.comp_count) {
            major_allele = local_refs[blk.cur_comp_res.begin()->second.cur_id_list[0]][shield_pos];
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        } else {
            major_allele = local_refs[blk.cur_comp_res.begin()->second.comp_id_list[0]][shield_pos];
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        }
//        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
//        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
        blk.tag = "secondary_empty_phased";
    }
}

void Phaser::first_phase_iter_v2(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos, std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids, std::vector<std::string> &all_haps) {
    std::vector<std::string> local_refs = get_local_refs(local_ref_map);
    std::set<std::string> unique_local_refs = unique_haps(local_refs);
    std::map<uint, std::string> local_shielded_ref_map;
    std::vector<std::string> local_shielded_refs;
    std::set<std::string> unique_local_shielded_refs;
    std::vector<std::string> visited;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;

    if (is_shield) {
//        local_shielded_ref_map = get_shielded_local_ref_map(shield_pos, blk.start, blk.end, local_refs);
        local_shielded_ref_map = get_shielded_local_ref_map_v2(shield_pos, blk.start, blk.end, local_refs, local_ref_map, ids);
        local_shielded_refs = get_local_refs(local_shielded_ref_map);
        unique_local_shielded_refs = unique_haps(local_shielded_refs);
        uint index_2 = 0;
        for (auto &hap: unique_local_shielded_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
                //            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
                cur_comp.cur_id_list = get_hap_ids(hap, local_shielded_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_shielded_ref_map);
                bool ibd_flag = false;
//                int ibd_pair_count = 0;
                for(auto &ibd: IBD_pairs){
                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
                        ibd_flag = true;
//                        ibd_pair_count += 1;
                    }
                }
//                if (ibd_pair_count>0){
//                    cur_comp.cur_count = cur_comp.cur_count-ibd_pair_count;
//                    cur_comp.comp_count = cur_comp.comp_count-ibd_pair_count;
//                    cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
//                    cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
//                }


//                if (ibd_flag){
//                    continue;
//                }
                //            }
                cur_comp_res.emplace_back(std::make_pair(index_2, cur_comp));
            }
            visited.emplace_back(hap);
            index_2++;
        }
        visited.clear();
    } else {
        uint index = 0;
        for (auto &hap: unique_local_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);

                cur_comp.cur_count = std::count(local_refs.begin(), local_refs.end(), hap);
                cur_comp.comp_count = std::count(local_refs.begin(), local_refs.end(), cur_comp.comp);

                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);

//                cur_comp.cur_id_list = get_ids(hap, local_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_refs);
                cur_comp.cur_id_list = get_hap_ids(hap, local_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_ref_map);

                // if cur_ids combine comp_ids in adjust list continue
                // fix later, not good
                bool ibd_flag = false;
//                int ibd_pair_count = 0;
                for(auto &ibd: IBD_pairs){
                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
                        ibd_flag = true;
//                        ibd_pair_count += 1;
                    }
                }
//                if (ibd_pair_count>0){
//                    cur_comp.cur_count = cur_comp.cur_count-ibd_pair_count;
//                    cur_comp.comp_count = cur_comp.comp_count-ibd_pair_count;
//                    cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
//                    cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
//                }
//                if (ibd_flag){
//                    continue;
//                }
                cur_comp_res.emplace_back(std::make_pair(index, cur_comp));
            }
            visited.emplace_back(hap);
            index++;
        }
        visited.clear();
    }
    if (cur_comp_res.empty()) {
        blk.tag = "empty";
        blk.cur_comp_res = cur_comp_res;
        return;
    } else if (cur_comp_res.size() == 1) {
        blk.tag = "phased";
        blk.cur_comp_res = cur_comp_res;
    } else if (cur_comp_res.size() > 1) {
        blk.tag = "reduce_phased";
        sort_comp_res(cur_comp_res);
        blk.cur_comp_res = cur_comp_res;
    }
    if (is_shield) {
        std::string seq1 = blk.cur_comp_res.begin()->second.cur;
        std::string seq2 = blk.cur_comp_res.begin()->second.comp;
        std::string major_allele;
        if (blk.cur_comp_res.begin()->second.cur_count >= blk.cur_comp_res.begin()->second.comp_count) {
            major_allele = all_haps[blk.cur_comp_res.begin()->second.cur_id_list[0]][shield_pos];
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        } else {
            major_allele = all_haps[blk.cur_comp_res.begin()->second.comp_id_list[0]][shield_pos];
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        }
        blk.tag = "secondary_empty_phased";
    }
}

void
Phaser::second_phase_iter(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos) {
    std::vector<std::string> local_refs = get_local_refs(local_ref_map);
    std::set<std::string> unique_local_refs = unique_haps(local_refs);
    std::map<uint, std::string> local_shielded_ref_map;
    std::vector<std::string> local_shielded_refs;
    std::set<std::string> unique_local_shielded_refs;
    std::vector<std::string> visited;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;
    if (is_shield) {
        local_shielded_ref_map = get_shielded_local_ref_map(shield_pos, blk.start, blk.end, local_refs);
        local_shielded_refs = get_local_refs(local_shielded_ref_map);
        unique_local_shielded_refs = unique_haps(local_shielded_refs);
//    }
//
//    if (is_shield) {
        uint index_2 = 0;
        for (auto &hap: unique_local_shielded_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
                //            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
                //            }
                cur_comp_res.emplace_back(std::make_pair(index_2, cur_comp));
            }
            visited.emplace_back(hap);
            index_2++;
        }
        visited.clear();
    } else {
        uint index = 0;
        for (auto &hap: unique_local_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
//            if (is_shield) {
//                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
//                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
//            } else {
                cur_comp.cur_count = std::count(local_refs.begin(), local_refs.end(), hap);
                cur_comp.comp_count = std::count(local_refs.begin(), local_refs.end(), cur_comp.comp);
//            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//            if (is_shield) {
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
//            } else {
                cur_comp.cur_id_list = get_ids(hap, local_refs);
                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_refs);
//            }
                cur_comp_res.emplace_back(std::make_pair(index, cur_comp));
            }
            visited.emplace_back(hap);
            index++;
        }
        visited.clear();
    }

    if (cur_comp_res.empty()) {
        blk.tag = "empty";
        blk.cur_comp_res = cur_comp_res;
    } else if (cur_comp_res.size() == 1) {
        blk.tag = "empty_phased";
        blk.cur_comp_res = cur_comp_res;
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    } else if (cur_comp_res.size() > 1) {
        blk.tag = "empty_phased";
        sort_comp_res(cur_comp_res);
        blk.cur_comp_res = cur_comp_res;
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    }
    if (is_shield) {
        std::string seq1 = blk.cur_comp_res.begin()->second.cur;
        std::string seq2 = blk.cur_comp_res.begin()->second.comp;
        std::string major_allele;
        major_allele = local_refs[blk.cur_comp_res.begin()->second.cur_id_list[0]][shield_pos];
        if (blk.cur_comp_res.begin()->second.cur_count >= blk.cur_comp_res.begin()->second.comp_count) {
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        } else {
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        }
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
        blk.tag = "secondary_empty_phased";
    }
}

void
Phaser::second_phase_iter_v2(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos, std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids, std::vector<std::string> &all_haps) {
    std::vector<std::string> local_refs = get_local_refs(local_ref_map);
    std::set<std::string> unique_local_refs = unique_haps(local_refs);
    std::map<uint, std::string> local_shielded_ref_map;
    std::vector<std::string> local_shielded_refs;
    std::set<std::string> unique_local_shielded_refs;
    std::vector<std::string> visited;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;
    if (is_shield) {
        local_shielded_ref_map = get_shielded_local_ref_map(shield_pos, blk.start, blk.end, local_refs);
        local_shielded_refs = get_local_refs(local_shielded_ref_map);
        unique_local_shielded_refs = unique_haps(local_shielded_refs);
//    }
//
//    if (is_shield) {
        uint index_2 = 0;
        for (auto &hap: unique_local_shielded_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
                //            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);

                cur_comp.cur_id_list = get_hap_ids(hap, local_shielded_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_shielded_ref_map);
                bool ibd_flag = false;
                for(auto &ibd: IBD_pairs){
                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
                        ibd_flag = true;
                    }
                }
                if (ibd_flag){
                    continue;
                }
                //            }
                cur_comp_res.emplace_back(std::make_pair(index_2, cur_comp));
            }
            visited.emplace_back(hap);
            index_2++;
        }
        visited.clear();
    } else {
        uint index = 0;
        for (auto &hap: unique_local_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
//            if (is_shield) {
//                cur_comp.cur_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
//                cur_comp.comp_count = std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
//            } else {
                cur_comp.cur_count = std::count(local_refs.begin(), local_refs.end(), hap);
                cur_comp.comp_count = std::count(local_refs.begin(), local_refs.end(), cur_comp.comp);
//            }
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//            if (is_shield) {
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
//            } else {
//                cur_comp.cur_id_list = get_ids(hap, local_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_refs);
                cur_comp.cur_id_list = get_hap_ids(hap, local_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_ref_map);

                // if cur_ids combine comp_ids in adjust list continue
                // fix later, not good
                bool ibd_flag = false;
                for(auto &ibd: IBD_pairs){
                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
                        ibd_flag = true;
                    }
                }
                if (ibd_flag){
                    continue;
                }
//            }
                cur_comp_res.emplace_back(std::make_pair(index, cur_comp));
            }
            visited.emplace_back(hap);
            index++;
        }
        visited.clear();
    }

    if (cur_comp_res.empty()) {
        blk.tag = "empty";
        blk.cur_comp_res = cur_comp_res;
    } else if (cur_comp_res.size() == 1) {
        blk.tag = "empty_phased";
        blk.cur_comp_res = cur_comp_res;
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    } else if (cur_comp_res.size() > 1) {
        blk.tag = "empty_phased";
        sort_comp_res(cur_comp_res);
        blk.cur_comp_res = cur_comp_res;
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    }
    if (is_shield) {
        std::string seq1 = blk.cur_comp_res.begin()->second.cur;
        std::string seq2 = blk.cur_comp_res.begin()->second.comp;
        std::string major_allele;
        major_allele = all_haps[blk.cur_comp_res.begin()->second.cur_id_list[0]][shield_pos];
        if (blk.cur_comp_res.begin()->second.cur_count >= blk.cur_comp_res.begin()->second.comp_count) {
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        } else {
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        }
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
        blk.tag = "secondary_empty_phased";
    }
}

std::map<uint, std::string>
Phaser::get_shielded_local_ref_map(uint shield_pos, uint start, uint end, std::vector<std::string> &haps) {
    std::map<uint, std::string> res;
    for (auto &hap : haps) {
        auto index = &hap - &haps[0];
        res[index] = hap.substr(0, shield_pos) + "*" + hap.substr(shield_pos + 1, end - start - shield_pos);
    }
    return res;
}
std::map<uint, std::string>
Phaser::get_shielded_local_ref_map_v2(uint shield_pos, uint start, uint end, std::vector<std::string> &haps, std::map<uint, std::string> &local_ref_map, std::vector<uint> ids) {
    std::map<uint, std::string> res;
    for (auto &hap : haps) {
        auto index = &hap - &haps[0];
        res[ids[index]] = hap.substr(0, shield_pos) + "*" + hap.substr(shield_pos + 1, end - start - shield_pos);
    }
    return res;
}

std::vector<uint> Phaser::get_ids(std::string hap, std::vector<std::string> &refs) {
    std::vector<uint> ids;
    for (auto &ref : refs) {
        auto index = &ref - &refs[0];
        if (hap == ref) {
            ids.emplace_back(index);
        }
    }
    return ids;
}

std::vector<uint> Phaser::get_hap_ids(std::string hap, std::map<uint, std::string> &haps_dict) {
    std::vector<uint> ids;
    for (auto &h : haps_dict) {
//        auto index = &ref - &refs[0];
        if (hap == h.second) {
            ids.emplace_back(h.first);
        }
    }
    return ids;
}

bool cmp(const std::pair<uint, uint> &p1, const std::pair<uint, uint> &p2) {
    return p1.second > p2.second;
}

std::vector<uint> Phaser::convert_str_2_vec(std::string &string) {
    std::vector<uint> res;
    for (char i : string) {
        const char *a = &i;
        res.emplace_back(uint(atoi(a)));
    }
    return res;
}

void Phaser::sort_comp_res(std::vector<std::pair<uint, CurComp>> &cur_comp_res) {
    std::vector<std::pair<uint, uint> > arr;
    std::vector<std::pair<uint, uint> > arr2;
    std::vector<std::pair<uint, CurComp>> best_part_map;
    std::vector<std::pair<uint, CurComp>> bad_part_map;

    for (auto &cur_comp_re : cur_comp_res) {
        auto index = &cur_comp_re - &cur_comp_res[0];
        arr.emplace_back(index, cur_comp_re.second.min_count);
    }
    sort(arr.begin(), arr.end(), cmp);
    uint best_val = arr[0].second;
    std::vector<uint> best_map_ids;
    std::vector<uint> bad_map_ids;
    for (auto &ar : arr) {
        auto index = &ar - &arr[0];
        if (ar.second == best_val) {
            best_map_ids.emplace_back(ar.first);
        } else {
            bad_map_ids.emplace_back(ar.first);
        }
    }
//    for (auto& best_map_id: best_map_ids){
//        best_part_map.emplace_back(cur_comp_res[best_map_id]);
//    }
    // sort best part by total count
    for (auto &best_map_id : best_map_ids) {
        arr2.emplace_back(std::make_pair(best_map_id, cur_comp_res[best_map_id].second.total_count));
    }
    sort(arr2.begin(), arr2.end(), cmp);

    for (auto &ar2: arr2) {
        best_part_map.emplace_back(cur_comp_res[ar2.first]);
    }

    for (auto &bad_map_id: bad_map_ids) {
        bad_part_map.emplace_back(cur_comp_res[bad_map_id]);
    }
    best_part_map.insert(best_part_map.end(), bad_part_map.begin(), bad_part_map.end());
    cur_comp_res = best_part_map;
}

std::set<std::string> Phaser::unique_haps(std::vector<std::string> &haps) {
    std::set<std::string> s(haps.begin(), haps.end());
    return s;
}

void Phaser::initialize_hap_map_list() {
    for (int het_idx = 0; het_idx < vcfHandler->het_pos_map[cur_tgt_idx].size(); ++het_idx) {
        unsigned int het_pos = vcfHandler->het_pos_map[cur_tgt_idx][het_idx];
        const std::vector<uint>::iterator &snp_iter = std::find(vcfHandler->snp_pos_map[cur_tgt_idx].begin(),
                                                                vcfHandler->snp_pos_map[cur_tgt_idx].end(), het_pos);
        long snp_idx = std::distance(vcfHandler->snp_pos_map[cur_tgt_idx].begin(), snp_iter);
        set_block_in_hapMap_by_size(hap_map_32, het_idx, het_pos, snp_idx, 32);
        set_block_in_hapMap_by_size(hap_map_16, het_idx, het_pos, snp_idx, 16);
        set_block_in_hapMap_by_size(hap_map_8, het_idx, het_pos, snp_idx, 8);
        set_block_in_hapMap_by_size(hap_map_4, het_idx, het_pos, snp_idx, 4);
    }
}

void Phaser::set_block_in_hapMap_by_size(std::map<uint, Block> &hapMap, uint het_idx, uint het_pos, uint snp_idx,
                                         int block_size) {
    if (het_idx % block_size == 0) {
        Block block;
        block.start = het_idx;
        block.end = (het_idx + block_size) < vcfHandler->het_pos_map[cur_tgt_idx].size() ? (het_idx + block_size) :
                    vcfHandler->het_pos_map[cur_tgt_idx].size();
        block.hap_range_idx.emplace_back(het_pos);
        block.het_idx.emplace_back(het_idx);
        block.snp_idx.emplace_back(snp_idx);
        block.blk_idx = het_pos;
        block.blk_size = block.end - block.start;
        hapMap.emplace(std::make_pair(het_idx, block));
    } else {
        uint block_k = het_idx / block_size;
        const std::map<uint, Block>::iterator &iterator = hapMap.find(block_k * block_size);
        iterator->second.hap1.emplace_back(0);
        iterator->second.hap2.emplace_back(1);
        iterator->second.hap_range_idx.emplace_back(het_pos);
        iterator->second.het_idx.emplace_back(het_idx);
        iterator->second.snp_idx.emplace_back(snp_idx);
    }
}

void Phaser::phase_hap_map(bool isCohort, uint nt) {
    HaploBitsArrays::CompressedHaploBitsT *pt = spectral->getHaploBitsT();
    std::vector<std::string> all_haps = pt->getAllHaps();
    std::vector<std::string> best_haps = get_best_haps_map(all_haps);
    if (isCohort) {
        all_haps.erase(all_haps.begin() + nt * 2, all_haps.begin() + nt * 2 + 2);
    }
    clock_t start1, end1;
    start1 = clock();
    phase_pipeline(hap_map_32, best_haps, all_haps);
    end1 = clock();
    std::cout << "phase 32 time: " << (double) (end1 - start1) / CLOCKS_PER_SEC << std::endl;
    clock_t start2, end2;
    start2 = clock();
    phase_pipeline(hap_map_16, best_haps, all_haps);
    end2 = clock();
    std::cout << "phase 16 time: " << (double) (end2 - start2) / CLOCKS_PER_SEC << std::endl;
    clock_t start3, end3;
    start3 = clock();
    phase_pipeline(hap_map_8, best_haps, all_haps);
    end3 = clock();
    std::cout << "phase 8 time: " << (double) (end3 - start3) / CLOCKS_PER_SEC << std::endl;
    clock_t start4, end4;
    start4 = clock();
    phase_pipeline(hap_map_4, best_haps, all_haps);
    end4 = clock();
    std::cout << "phase 4 time: " << (double) (end4 - start4) / CLOCKS_PER_SEC << std::endl;
}

void Phaser::phase_hap_map_v2(bool isCohort, uint nt) {
    HaploBitsArrays::CompressedHaploBitsT *pt = spectral->getHaploBitsT();
    std::vector<std::string> all_haps = pt->getAllHaps();
    std::vector<std::string> all_hom_haps = pt->getAllHapsWithCompressedHom();
    std::vector<std::string> best_haps = get_best_haps_map(all_haps);
    std::vector<std::string> best_hom_haps = get_best_haps_map(all_hom_haps);
    std::vector<uint> bestk_ids = pt->getBestK();

    if (isCohort) {
        all_haps.erase(all_haps.begin() + nt * 2, all_haps.begin() + nt * 2 + 2);
        all_hom_haps.erase(all_hom_haps.begin() + nt * 2, all_hom_haps.begin() + nt * 2 + 2);
    }
    clock_t start1, end1;
    start1 = clock();
    phase_pipeline_v2(hap_map_32, best_haps, all_haps, all_hom_haps, best_hom_haps, bestk_ids);
    end1 = clock();
    std::cout << "phase 32 time: " << (double) (end1 - start1) / CLOCKS_PER_SEC << std::endl;
    clock_t start2, end2;
    start2 = clock();
    phase_pipeline_v2(hap_map_16, best_haps, all_haps, all_hom_haps, best_hom_haps, bestk_ids);
    end2 = clock();
    std::cout << "phase 16 time: " << (double) (end2 - start2) / CLOCKS_PER_SEC << std::endl;
    clock_t start3, end3;
    start3 = clock();
    phase_pipeline_v2(hap_map_8, best_haps, all_haps, all_hom_haps, best_hom_haps, bestk_ids);
    end3 = clock();
    std::cout << "phase 8 time: " << (double) (end3 - start3) / CLOCKS_PER_SEC << std::endl;
    clock_t start4, end4;
    start4 = clock();
    phase_pipeline_v2(hap_map_4, best_haps, all_haps, all_hom_haps, best_hom_haps, bestk_ids);
    end4 = clock();
    std::cout << "phase 4 time: " << (double) (end4 - start4) / CLOCKS_PER_SEC << std::endl;
}


void Phaser::phase_pipeline(std::map<uint, Block> &hap_map_n, std::vector<std::string> &best_haps,
                            std::vector<std::string> &all_haps) {
    for (auto &blk_map : hap_map_n) {
        std::map<uint, std::string> local_ref_map;
        local_ref_map = get_local_ref_map(blk_map.second.start, blk_map.second.end, best_haps);
        if (blk_map.second.blk_size <= 4 && blk_map.second.start == 36) {
            std::cout << "4444" << std::endl;
        }
        first_phase_iter(blk_map.second, local_ref_map, false, 0);
    }
    for (auto &blk_map : hap_map_n) {
        std::map<uint, std::string> local_ref_map;
        if (blk_map.second.tag == "empty") {
            local_ref_map = get_local_ref_map(blk_map.second.start, blk_map.second.end, all_haps);
            second_phase_iter(blk_map.second, local_ref_map, false, 0);
        }
    }
    if (hap_map_n.begin()->second.blk_size == 4 || hap_map_n.begin()->second.blk_size == 8 ||
        hap_map_n.begin()->second.blk_size == 16) {
        // secondary_empty_block
        for (auto &blk_map : hap_map_n) {
            std::map<uint, std::string> local_ref_map;
            if (blk_map.second.tag == "empty") {
                local_ref_map = get_local_ref_map(blk_map.second.start, blk_map.second.end, best_haps);
                for (int i = 0; i < blk_map.second.blk_size; i++) {
                    first_phase_iter(blk_map.second, local_ref_map, true, i);
                    if (blk_map.second.tag == "secondary_empty_phased")
                        break;
                }
            }
            if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
                std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
                std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
                blk_map.second.pseudo_hap1 = hap1_vec;
                blk_map.second.pseudo_hap2 = hap2_vec;
                CurComp cur_comp;
                std::string hap0(blk_map.second.blk_size, '0');
                cur_comp.cur = hap0;
                std::string hap1(blk_map.second.blk_size, '1');
                cur_comp.comp = hap1;
                cur_comp.comp_count = 1;
                cur_comp.cur_count = 1;
                cur_comp.cur_id_list = {0};
                cur_comp.comp_id_list = {1};
                std::vector<std::pair<uint, CurComp>> tmp_map;
                cur_comp;
                blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
                blk_map.second.tag = "secondary_empty_phased";

            }
        }
    }
}
// bit operation
//void Phaser::phase_pipeline_v3(std::map<uint, Block> &hap_map_n, std::vector<std::string> &best_haps,
//                               std::vector<std::string> &all_haps, std::vector<std::string> &compress_hom_haps,
//                               std::vector<std::string> &best_hom_haps, std::vector<uint> &bestk_ids) {
//
//}

void Phaser::phase_pipeline_v2(std::map<uint, Block> &hap_map_n, std::vector<std::string> &best_haps,
                            std::vector<std::string> &all_haps, std::vector<std::string> &compress_hom_haps, std::vector<std::string> &best_hom_haps, std::vector<uint> &bestk_ids) {
    std::vector<uint> all_ids;
    std::vector<std::pair<int, int>> IBD_pairs;
    std::map<uint, uint> ref_id_dif_count_map = std::map<uint, uint>();
    for (int i = 0; i < all_haps.size(); ++i) {
        all_ids.push_back(i);
        if (i%2==0){
            IBD_pairs.emplace_back(std::make_pair(i, i+1));
        }
    }
    for (auto &blk_map : hap_map_n) {
        std::map<uint, std::string> local_ref_map;
//        local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//        local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids);
        local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids, ref_id_dif_count_map);
//        first_phase_iter_v2(blk_map.second, local_ref_map, false, 0, IBD_pairs, bestk_ids, all_haps);
        first_phase_iter_v3(blk_map.second, local_ref_map, false, 0, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
//        first_phase_iter(blk_map.second, local_ref_map, false, 0);
    }
    for (auto &blk_map : hap_map_n) {
        std::map<uint, std::string> local_ref_map;
        if (blk_map.second.tag == "empty") {
            local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end, all_haps, compress_hom_haps, all_ids, ref_id_dif_count_map);
            second_phase_iter_v3(blk_map.second, local_ref_map, false, 0, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
        }
    }
    if (ITER_NUM==0){
        if (
            hap_map_n.begin()->second.blk_size == 16|| hap_map_n.begin()->second.blk_size == 32) {
            // secondary_empty_block
            for (auto &blk_map : hap_map_n) {
                std::map<uint, std::string> local_ref_map;
                if (blk_map.second.tag == "empty") {
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, all_haps, compress_hom_haps, bestk_ids);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids);
                    local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end,
                                                         best_haps, best_hom_haps, bestk_ids, ref_id_dif_count_map);
                    for (int i = 0; i < blk_map.second.blk_size; i++) {
//                        first_phase_iter_v2(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps);
                        first_phase_iter_v3(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
//                        first_phase_iter(blk_map.second, local_ref_map, true, i);
                        if (blk_map.second.tag == "secondary_empty_phased")
                            break;
                    }
                }
                if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
                    std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
                    std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
                    blk_map.second.pseudo_hap1 = hap1_vec;
                    blk_map.second.pseudo_hap2 = hap2_vec;
                    CurComp cur_comp;
                    std::string hap0(blk_map.second.blk_size, '0');
                    cur_comp.cur = hap0;
                    std::string hap1(blk_map.second.blk_size, '1');
                    cur_comp.comp = hap1;
                    cur_comp.comp_count = 1;
                    cur_comp.cur_count = 1;
                    cur_comp.cur_id_list = {0};
                    cur_comp.comp_id_list = {1};
                    std::vector<std::pair<uint, CurComp>> tmp_map;
                    cur_comp;
                    blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
                    blk_map.second.tag = "secondary_empty_phased";

                }
            }
        }
    }
    else if(ITER_NUM==1){
        if (
                 hap_map_n.begin()->second.blk_size == 16|| hap_map_n.begin()->second.blk_size == 32) {
            // secondary_empty_block
            for (auto &blk_map : hap_map_n) {
                std::map<uint, std::string> local_ref_map;
                if (blk_map.second.tag == "empty") {
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, all_haps, compress_hom_haps, bestk_ids);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids);
                    local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids, ref_id_dif_count_map);
                    for (int i = 0; i < blk_map.second.blk_size; i++) {
//                        first_phase_iter_v2(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps);
                        first_phase_iter_v3(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
//                        first_phase_iter(blk_map.second, local_ref_map, true, i);
                        if (blk_map.second.tag == "secondary_empty_phased")
                            break;
                    }
                }
                if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
                    std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
                    std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
                    blk_map.second.pseudo_hap1 = hap1_vec;
                    blk_map.second.pseudo_hap2 = hap2_vec;
                    CurComp cur_comp;
                    std::string hap0(blk_map.second.blk_size, '0');
                    cur_comp.cur = hap0;
                    std::string hap1(blk_map.second.blk_size, '1');
                    cur_comp.comp = hap1;
                    cur_comp.comp_count = 1;
                    cur_comp.cur_count = 1;
                    cur_comp.cur_id_list = {0};
                    cur_comp.comp_id_list = {1};
                    std::vector<std::pair<uint, CurComp>> tmp_map;
                    cur_comp;
                    blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
                    blk_map.second.tag = "secondary_empty_phased";

                }
            }
        }
    }
    else if(ITER_NUM==2){
        if (
                hap_map_n.begin()->second.blk_size == 16) {
            // secondary_empty_block
            for (auto &blk_map : hap_map_n) {
                std::map<uint, std::string> local_ref_map;
                if (blk_map.second.tag == "empty") {
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, all_haps, compress_hom_haps, bestk_ids);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids);
                    local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids, ref_id_dif_count_map);
                    for (int i = 0; i < blk_map.second.blk_size; i++) {
//                        first_phase_iter_v2(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps);
                        first_phase_iter_v3(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
//                        first_phase_iter(blk_map.second, local_ref_map, true, i);
                        if (blk_map.second.tag == "secondary_empty_phased")
                            break;
                    }
                }
                if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
                    std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
                    std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
                    blk_map.second.pseudo_hap1 = hap1_vec;
                    blk_map.second.pseudo_hap2 = hap2_vec;
                    CurComp cur_comp;
                    std::string hap0(blk_map.second.blk_size, '0');
                    cur_comp.cur = hap0;
                    std::string hap1(blk_map.second.blk_size, '1');
                    cur_comp.comp = hap1;
                    cur_comp.comp_count = 1;
                    cur_comp.cur_count = 1;
                    cur_comp.cur_id_list = {0};
                    cur_comp.comp_id_list = {1};
                    std::vector<std::pair<uint, CurComp>> tmp_map;
                    cur_comp;
                    blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
                    blk_map.second.tag = "secondary_empty_phased";

                }
            }
        }
    }
    else if(ITER_NUM==3){
        if (
               hap_map_n.begin()->second.blk_size == 16||  hap_map_n.begin()->second.blk_size == 32) {
            // secondary_empty_block
            for (auto &blk_map : hap_map_n) {
                std::map<uint, std::string> local_ref_map;
                if (blk_map.second.tag == "empty") {
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, all_haps, compress_hom_haps, bestk_ids);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids);
                    local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids, ref_id_dif_count_map);
                    for (int i = 0; i < blk_map.second.blk_size; i++) {
//                        first_phase_iter_v2(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps);
                        first_phase_iter_v3(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
//                        first_phase_iter(blk_map.second, local_ref_map, true, i);
                        if (blk_map.second.tag == "secondary_empty_phased")
                            break;
                    }
                }
                if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
                    std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
                    std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
                    blk_map.second.pseudo_hap1 = hap1_vec;
                    blk_map.second.pseudo_hap2 = hap2_vec;
                    CurComp cur_comp;
                    std::string hap0(blk_map.second.blk_size, '0');
                    cur_comp.cur = hap0;
                    std::string hap1(blk_map.second.blk_size, '1');
                    cur_comp.comp = hap1;
                    cur_comp.comp_count = 1;
                    cur_comp.cur_count = 1;
                    cur_comp.cur_id_list = {0};
                    cur_comp.comp_id_list = {1};
                    std::vector<std::pair<uint, CurComp>> tmp_map;
                    cur_comp;
                    blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
                    blk_map.second.tag = "secondary_empty_phased";

                }
            }
        }
    }
    else{
        if (hap_map_n.begin()->second.blk_size == 16|| hap_map_n.begin()->second.blk_size == 32|| hap_map_n.begin()->second.blk_size <= 4) {
            // secondary_empty_block
            for (auto &blk_map : hap_map_n) {
                std::map<uint, std::string> local_ref_map;
                if (blk_map.second.tag == "empty") {
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, all_haps, compress_hom_haps, bestk_ids);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//                    local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids);
                    local_ref_map = get_local_ref_map_v3(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps, bestk_ids, ref_id_dif_count_map);
                    for (int i = 0; i < blk_map.second.blk_size; i++) {
//                        first_phase_iter_v2(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps);
                        first_phase_iter_v3(blk_map.second, local_ref_map, true, i, IBD_pairs, bestk_ids, all_haps, ref_id_dif_count_map);
//                        first_phase_iter(blk_map.second, local_ref_map, true, i);
                        if (blk_map.second.tag == "secondary_empty_phased")
                            break;
                    }
                }
                if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
                    std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
                    std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
                    blk_map.second.pseudo_hap1 = hap1_vec;
                    blk_map.second.pseudo_hap2 = hap2_vec;
                    CurComp cur_comp;
                    std::string hap0(blk_map.second.blk_size, '0');
                    cur_comp.cur = hap0;
                    std::string hap1(blk_map.second.blk_size, '1');
                    cur_comp.comp = hap1;
                    cur_comp.comp_count = 1;
                    cur_comp.cur_count = 1;
                    cur_comp.cur_id_list = {0};
                    cur_comp.comp_id_list = {1};
                    std::vector<std::pair<uint, CurComp>> tmp_map;
                    cur_comp;
                    blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
                    blk_map.second.tag = "secondary_empty_phased";

                }
            }
        }
    }
//    if (hap_map_n.begin()->second.blk_size == 32||
//        hap_map_n.begin()->second.blk_size == 16) {
//        // secondary_empty_block
//        for (auto &blk_map : hap_map_n) {
//            std::map<uint, std::string> local_ref_map;
//            if (blk_map.second.tag == "empty") {
//                local_ref_map = get_local_ref_map_v2(blk_map.second.start, blk_map.second.end, best_haps, best_hom_haps);
//                for (int i = 0; i < blk_map.second.blk_size; i++) {
//                    first_phase_iter(blk_map.second, local_ref_map, true, i);
//                    if (blk_map.second.tag == "secondary_empty_phased")
//                        break;
//                }
//            }
//            if (blk_map.second.tag == "empty" && blk_map.second.blk_size <= 4) {
//                std::vector<uint> hap1_vec(blk_map.second.blk_size, 0);
//                std::vector<uint> hap2_vec(blk_map.second.blk_size, 1);
//                blk_map.second.pseudo_hap1 = hap1_vec;
//                blk_map.second.pseudo_hap2 = hap2_vec;
//                CurComp cur_comp;
//                std::string hap0(blk_map.second.blk_size, '0');
//                cur_comp.cur = hap0;
//                std::string hap1(blk_map.second.blk_size, '1');
//                cur_comp.comp = hap1;
//                cur_comp.comp_count = 1;
//                cur_comp.cur_count = 1;
//                cur_comp.cur_id_list = {0};
//                cur_comp.comp_id_list = {1};
//                std::vector<std::pair<uint, CurComp>> tmp_map;
//                cur_comp;
//                blk_map.second.cur_comp_res.emplace_back(std::make_pair(0, cur_comp));
//                blk_map.second.tag = "secondary_empty_phased";
//
//            }
//        }
//    }
}

std::vector<std::string> Phaser::get_best_haps_map(std::vector<std::string> &all_haps) {
    HaploBitsArrays::CompressedHaploBitsT *pt = spectral->getHaploBitsT();
    std::vector<uint> bestk_ids = pt->getBestK();
    std::vector<std::string> res;
    for (auto &bk : bestk_ids) {
        res.emplace_back(all_haps[bk]);
    }
    return res;
}

std::string Phaser::vec_to_string(std::vector<uint> vec) {
    std::string str_ans;
    for (auto &i : vec) {
        str_ans += std::to_string(i);
    }
    return str_ans;
}

Phaser::Phaser(const std::string &vcfFile, const std::string &fnout, int &chrom, const std::string &geneticMapFile,
               const std::string &tmpFile, const std::string &writeMode, double cMmax) {
    runTime_log.open("run_time.log", std::ios::out);
    // load ref as eagle
//    tarVcf = new VCFReader(vcfFile.data());
//
//    fwvcf = new VCFWriter(tarVcf->header, fnout.data());

    frbed = nullptr;
    coverage = 30;  //deprecated
    cur_tgt_idx = 0;


    bool use_secondary = false;
    recursive_complete_empty_blk_flag = true;
    threshold = 1e-20;

    spectral = new Spectral(frfrag, frbed, threshold, coverage, use_secondary);
    clock_t start, end;
    start = clock();

    vcfHandler = new VcfHandler(vcfFile, chrom, 23, geneticMapFile, tmpFile, "wz");

    GenoBitsbuilder to_bits(vcfHandler->hapsRef, vcfHandler->genosTarget, vcfHandler->cMs,
                            int(cMmax) == 0 ? 1 : int(cMmax),
                            vcfHandler->M, vcfHandler->Nref, vcfHandler->Ntarget);
//    hapsRef = vcfHandler->hapsRef;
    refCohortFlag = vcfHandler->refCohortFlag;
    haploBitsT = new HaploBitsArrays::HaploBitsT(to_bits.getHaploBits(), to_bits.getGenoBits(),
                                                 to_bits.getEffectiveSNPSites(), vcfHandler->Nref,
                                                 vcfHandler->Ntarget, vcfHandler->M,
                                                 to_bits.getMseg64(), to_bits.getSeg64cMvecs(),
                                                 vcfHandler->cMs, to_bits.getBlockPaddingCount());
    end = clock();
    std::cout << "eagle load data time: " << (double) (end - start) / CLOCKS_PER_SEC << std::endl;
}

void Phaser::phasing_cohort() {
    clock_t start_t, end_t;
    start_t = clock();
    uint prev_variant_count = 0;
    uint iter_num = ITER_NUM;
    uint k = 0;
//todo: this is a test of 10000 K
//    k=10000;
    k=200;
//    for (uint rid = 0; rid < tarVcf->contigs_count; rid++) {
        for (uint nt = 0; nt < haploBitsT->Ntarget; ++nt) {
//            ChromoPhaser *chromo_phaser = new ChromoPhaser(rid, tarVcf->contigs[rid], WINDOW_OVERLAP, WINDOW_SIZE);
            std::string mess = "phasing haplotype for " + std::string(vcfHandler->cur_chr) + ", sample for: " + vcfHandler->targetIDs[nt] + ", " + std::to_string(nt) + "of"+ std::to_string(haploBitsT->Ntarget);
            logging(std::cerr, mess);
            clock_t start1, end1;
            start1 = clock();
//            if (tarVcf->jump_to_contig(rid) != 0)
//                break;
//            load_contig_blocks_cohort(nt);
            end1 = clock();
            cur_tgt_idx = nt;
            std::cout << "spec load data time: " << (double) (end1 - start1) / CLOCKS_PER_SEC << std::endl;
//            chromo_phaser->construct_phasing_window_initialize();
            clock_t start2, end2;
            start2 = clock();
            HaploBitsArrays::CompressedHaploBitsT *hapBitsT = constructHapBitsT(
                    nt, k, false);
            end2 = clock();
            std::cout << "process bestk time: " << (double) (end2 - start2) / CLOCKS_PER_SEC << std::endl;
            clock_t start3, end3;
            start3 = clock();
            hapBitsT->setCms(haploBitsT->cms);
            hapBitsT->setHapsRef(vcfHandler->hapsRef);
            hapBitsT->setM(haploBitsT->M);
            spectral->setHaploBitsT(hapBitsT);
            std::string runTimeMes = mess + ", data preprocessing finished";
            runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
            initialize_hap_map_list();
            runTimeMes = mess + ", hap_map initializing";
            runTimeLogging(runTime_log, start_t, end_t, runTimeMes);

            end3 = clock();
            std::cout << "init time: " << (double) (end3 - start3) / CLOCKS_PER_SEC << std::endl;
            phase_hap_map_v2(true, nt);
            runTimeMes = mess + ", hap_map filling finished";
            runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
            runTimeMes = mess + "start generating spectral graph and cut";
            logging(runTime_log, runTimeMes);

            clock_t start4, end4;
            start4 = clock();
            Parameters parameters;
            std::map<std::string, int> significance_map_tmp;
            significance_map_tmp.emplace(std::make_pair("phased", 5));
            significance_map_tmp.emplace(std::make_pair("reduced", 0));
            significance_map_tmp.emplace(std::make_pair("empty", 0));
            significance_map_tmp.emplace(std::make_pair("empty_phased", 4));
            significance_map_tmp.emplace(std::make_pair("reduce_phased", 1));
            significance_map_tmp.emplace(std::make_pair("secondary_empty_phased", 6));
            parameters.comp_pair_count = 8;
            parameters.reduce_weight_rate = 0.5;
            parameters.significance_map.emplace(std::make_pair(32, significance_map_tmp));
            parameters.significance_map.emplace(std::make_pair(16, significance_map_tmp));
            parameters.significance_map.emplace(std::make_pair(8, significance_map_tmp));
            parameters.significance_map.emplace(std::make_pair(4, significance_map_tmp));
//        std::cout << "transed hap map size = " << transed_hap_map.size() << std::endl;
            for (auto i = hap_map_32.begin(); i != hap_map_32.end(); ++i) {
                if (i->second.tag == "empty") {
                    for (int j = i->second.het_idx.front(); j < i->second.het_idx.front() + 32; j += 16) {
                        if (hap_map_16.count(j) == 0) continue;
                        if (hap_map_16[j].tag == "empty") {
                            for (int k = j; k < j + 16; k += 8) {
                                if (hap_map_8.count(k) == 0) continue;
                                if (hap_map_8[k].tag == "empty") {
                                    for (int l = k; l < k + 8; l += 4) {
                                        if (hap_map_4.count(l) == 0) continue;
                                        if (hap_map_4[l].tag == "empty") {
                                            continue;
                                        } else {
                                            std::vector<std::string> pos_neg_idx;
                                            std::vector<std::string> blk_order;
                                            for (int m = l; m < l + hap_map_4[l].het_idx.size(); ++m) {
                                                pos_neg_idx.emplace_back(std::to_string(m) + "+");
                                                pos_neg_idx.emplace_back(std::to_string(m) + "-");
                                            }
                                            uint ni = pos_neg_idx.size();
                                            auto *rawGraph = new double[ni * ni];
                                            int *rawCount = new int[ni * ni];
                                            for (int i = 0; i < ni * ni; ++i) {
                                                rawGraph[i] = 0;
                                                rawCount[i] = 0;
                                            }
                                            ViewMap weighted_graph(rawGraph, ni, ni);
                                            CViewMap count_graph(rawCount, ni, ni);
                                            spectral->load_weight(weighted_graph, hap_map_4, l, parameters, 0, 4);
                                            blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph,
                                                                                    hap_map_4,
                                                                                    transed_hap_map, parameters);
                                        }
                                    }
                                } else {
                                    std::vector<std::string> pos_neg_idx;
                                    std::vector<std::string> blk_order;
                                    for (int m = k; m < k + hap_map_8[k].het_idx.size(); ++m) {
                                        pos_neg_idx.emplace_back(std::to_string(m) + "+");
                                        pos_neg_idx.emplace_back(std::to_string(m) + "-");
                                    }
                                    uint ni = pos_neg_idx.size();
                                    auto *rawGraph = new double[ni * ni];
                                    int *rawCount = new int[ni * ni];
                                    for (int i = 0; i < ni * ni; ++i) {
                                        rawGraph[i] = 0;
                                        rawCount[i] = 0;
                                    }
                                    ViewMap weighted_graph(rawGraph, ni, ni);
                                    CViewMap count_graph(rawCount, ni, ni);
                                    spectral->load_weight_for_blocks(weighted_graph, hap_map_8[k], hap_map_4,
                                                                     parameters, 8,
                                                                     4);
                                    spectral->load_weight(weighted_graph, hap_map_8, k, parameters, 0, 8);
                                    blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_8,
                                                                            transed_hap_map,
                                                                            parameters);
                                }
                            }
                        } else {
                            std::vector<std::string> pos_neg_idx;
                            std::vector<std::string> blk_order;
                            for (int m = j; m < j + hap_map_16[j].het_idx.size(); ++m) {
                                pos_neg_idx.emplace_back(std::to_string(m) + "+");
                                pos_neg_idx.emplace_back(std::to_string(m) + "-");
                            }
                            uint ni = pos_neg_idx.size();
                            auto *rawGraph = new double[ni * ni];
                            int *rawCount = new int[ni * ni];
                            for (int i = 0; i < ni * ni; ++i) {
                                rawGraph[i] = 0;
                                rawCount[i] = 0;
                            }
                            ViewMap weighted_graph(rawGraph, ni, ni);
                            CViewMap count_graph(rawCount, ni, ni);
                            spectral->load_weight_for_blocks(weighted_graph, hap_map_16[j], hap_map_8, parameters, 16,
                                                             8);
                            spectral->load_weight_for_blocks(weighted_graph, hap_map_16[j], hap_map_4, parameters, 16,
                                                             4);
                            spectral->load_weight(weighted_graph, hap_map_16, j, parameters, 0, 16);
                            blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_16,
                                                                    transed_hap_map,
                                                                    parameters);
                        }
                    }
                } else {
                    std::vector<std::string> pos_neg_idx;
                    std::vector<std::string> blk_order;
                    for (int m = i->second.het_idx.front();
                         m < i->second.het_idx.front() + i->second.het_idx.size(); ++m) {
                        pos_neg_idx.emplace_back(std::to_string(m) + "+");
                        pos_neg_idx.emplace_back(std::to_string(m) + "-");
                    }
                    uint ni = pos_neg_idx.size();
                    auto *rawGraph = new double[ni * ni];
                    int *rawCount = new int[ni * ni];
                    for (int i = 0; i < ni * ni; ++i) {
                        rawGraph[i] = 0;
                        rawCount[i] = 0;
                    }
                    ViewMap weighted_graph(rawGraph, ni, ni);
                    CViewMap count_graph(rawCount, ni, ni);
                    spectral->load_weight_for_blocks(weighted_graph, hap_map_32[i->second.het_idx.front()], hap_map_4,
                                                     parameters, 32, 4);
                    spectral->load_weight_for_blocks(weighted_graph, hap_map_32[i->second.het_idx.front()], hap_map_8,
                                                     parameters, 32, 8);
                    spectral->load_weight_for_blocks(weighted_graph, hap_map_32[i->second.het_idx.front()], hap_map_16,
                                                     parameters, 32, 16);
                    spectral->load_weight(weighted_graph, hap_map_32, i->second.start, parameters, 0, 32);
                    blk_order = spectral->call_haplotype_v4(pos_neg_idx, weighted_graph, hap_map_32, transed_hap_map,
                                                            parameters);
                }
            }
            end4 = clock();
            std::cout << "spec graph: " << (double) (end4 - start4) / CLOCKS_PER_SEC << std::endl;
            int idx = 0;
//        transed_hap_map = test_hap_map;

//            std::ofstream fout1;
//            fout1.open("check_trans_mp.txt");
//
//            for (auto &trans : transed_hap_map)
//
//                fout1 << trans.first << '\t' << vec_to_string(trans.second.pseudo_hap1) << std::endl;
//
//            fout1.close();
            clock_t start5, end5;
            start5 = clock();
            runTimeMes = mess + ", in-block phase finished";
            runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
            runTimeMes = mess + ", start linking the blocks";
            logging(runTime_log, runTimeMes);

            link_blk();
            runTimeMes = mess + ", link block finished";
            runTimeLogging(runTime_log, start_t, end_t, runTimeMes);
            end5 = clock();
            std::cout << "link block time: " << (double) (end5 - start5) / CLOCKS_PER_SEC << std::endl;
            //todo: imputation
//        hapBitsT->phasingInBlockV2(phased_hap_map, WINDOW_SIZE);
            std::string nt_str = std::to_string(nt);
            std::string final_hap_name = "final_hap" + nt_str + ".txt";
            std::ofstream out8(final_hap_name, std::ios::out);
            for (auto &h : transed_hap_map) {
                for (uint i = 0; i < h.second.phased_het_idx.size(); i++) {
                    out8 << h.first << '\t' << h.second.hap_range_idx[h.second.phased_het_idx[i]] << '\t'
                         << h.second.pseudo_hap1[i] << h.second.pseudo_hap2[i]
                         << std::endl;
                }
            }
            out8.close();
//        fwvcf->write_nxt_contigs(tarVcf->contigs[rid].data(), chromo_phaser, *tarVcf, out1);
            //write vcf
//            prev_variant_count += chromo_phaser->variant_count;
            spectral->release_chromo_phaser();
//            this->tarVcf->snp_pos_container.clear();
//            this->tarVcf->het_pos_container.clear();
            this->transed_hap_map.clear();
            this->recursive_complete_empty_blk_flag = false;
            this->block_id_map.clear();
            this->hap_map.clear();
            this->hap_map_32.clear();
            this->hap_map_16.clear();
            this->hap_map_8.clear();
            this->hap_map_4.clear();
//            delete chromo_phaser;
            delete hapBitsT;
        }
//    }
//    all_hap_file.close();
//    all_hap_with_zipped_hom_file.close();
//    bestK_ids_file.close();
}

int Phaser::count_hom_dif(std::string &local_seq, uint index, std::string &homo_hap) {
    int err_count = 0;
    for (int i = 0; i < homo_hap.size(); i++) {
        if (i % 2 == 0) {
            if (homo_hap[i] == '1') {
                err_count += 1;
            }
        }
    }
    return err_count;
}

std::map<uint, std::string> Phaser::get_local_ref_map_v3(uint start, uint end, std::vector<std::string> &haps,
                                                         std::vector<std::string> &best_hom_haps,
                                                         std::vector<uint> &ids,
                                                         std::map<uint, uint> &ref_id_hom_dif_map) {
    std::map<uint, std::string> res;
    for (auto &hap : haps) {
        auto index = &hap - &haps[0];
        std::string local_seq = hap.substr(start, end - start);
        std::string local_hom_seq = best_hom_haps[index].substr(start, end - start);
        // check homo region, if it is the same chunk as target
        int homDif = count_hom_dif(local_seq, index, local_hom_seq);
        res[ids[index]] = hap.substr(start, end-start);
        ref_id_hom_dif_map.insert(std::make_pair(ids[index], homDif));
    }
    return res;
}

void
Phaser::first_phase_iter_v3(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos,
                            std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids,
                            std::vector<std::string> &all_haps, std::map<uint, uint> &ref_id_dif_count_map) {
    std::vector<std::string> local_refs = get_local_refs(local_ref_map);
    std::set<std::string> unique_local_refs = unique_haps(local_refs);
    std::map<uint, std::string> local_shielded_ref_map;
    std::vector<std::string> local_shielded_refs;
    std::set<std::string> unique_local_shielded_refs;
    std::vector<std::string> visited;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;

    if (is_shield) {
//        local_shielded_ref_map = get_shielded_local_ref_map(shield_pos, blk.start, blk.end, local_refs);
        local_shielded_ref_map = get_shielded_local_ref_map_v2(shield_pos, blk.start, blk.end, local_refs, local_ref_map, ids);
        local_shielded_refs = get_local_refs(local_shielded_ref_map);
        unique_local_shielded_refs = unique_haps(local_shielded_refs);
        uint index_2 = 0;
        for (auto &hap: unique_local_shielded_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = SUPPORT_COUNT_SCALE*std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
                cur_comp.comp_count = SUPPORT_COUNT_SCALE*std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.cur_id_list = get_hap_ids(hap, local_shielded_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_shielded_ref_map);
                //todo: adding the decrement of hom dif count, testing different parameters
                for (auto cur_id : cur_comp.cur_id_list) {
                    cur_comp.cur_count -= decrease_support_count(ref_id_dif_count_map, cur_id);
                }
                for (auto comp_id : cur_comp.comp_id_list) {
                    cur_comp.comp_count -= decrease_support_count(ref_id_dif_count_map, comp_id);
                }
                // check 10 in most haps
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
//                int ibd_pair_count = 0;
//                for(auto &ibd: IBD_pairs){
//                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
//                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
//                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
//                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
//                        ibd_flag = true;
////                        ibd_pair_count += 1;
//                    }
//                }
//                if (ibd_pair_count>0){
//                    cur_comp.cur_count = cur_comp.cur_count-ibd_pair_count;
//                    cur_comp.comp_count = cur_comp.comp_count-ibd_pair_count;
//                    cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
//                    cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
//                }


//                if (ibd_flag){
//                    continue;
//                }
                //            }
                cur_comp_res.emplace_back(std::make_pair(index_2, cur_comp));
            }
            visited.emplace_back(hap);
            index_2++;
        }
        visited.clear();
    } else {
        uint index = 0;
        for (auto &hap: unique_local_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = SUPPORT_COUNT_SCALE*std::count(local_refs.begin(), local_refs.end(), hap);
                cur_comp.comp_count = SUPPORT_COUNT_SCALE*std::count(local_refs.begin(), local_refs.end(), cur_comp.comp);
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.cur_id_list = get_hap_ids(hap, local_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_ref_map);
                //todo: adding the decrement of hom dif count, testing different parameters
                for (auto cur_id : cur_comp.cur_id_list) {
                    cur_comp.cur_count -= decrease_support_count(ref_id_dif_count_map, cur_id);
                }
                for (auto comp_id : cur_comp.comp_id_list) {
                    cur_comp.comp_count -= decrease_support_count(ref_id_dif_count_map, comp_id);
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);

//                cur_comp.cur_id_list = get_ids(hap, local_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_refs);

                // if cur_ids combine comp_ids in adjust list continue
                // fix later, not good
//                int ibd_pair_count = 0;
//                for(auto &ibd: IBD_pairs){
//                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
//                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
//                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
//                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
//                        ibd_flag = true;
////                        ibd_pair_count += 1;
//                    }
//                }
//                if (ibd_pair_count>0){
//                    cur_comp.cur_count = cur_comp.cur_count-ibd_pair_count;
//                    cur_comp.comp_count = cur_comp.comp_count-ibd_pair_count;
//                    cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
//                    cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
//                }
//                if (ibd_flag){
//                    continue;
//                }
                cur_comp_res.emplace_back(std::make_pair(index, cur_comp));
            }
            visited.emplace_back(hap);
            index++;
        }
        visited.clear();
    }
    if (cur_comp_res.empty()) {
        blk.tag = "empty";
        blk.cur_comp_res = cur_comp_res;
        return;
    } else if (cur_comp_res.size() == 1) {
        blk.tag = "phased";
        blk.cur_comp_res = cur_comp_res;
    } else if (cur_comp_res.size() > 1) {
        blk.tag = "reduce_phased";
        sort_comp_res(cur_comp_res);
        blk.cur_comp_res = cur_comp_res;
    }
    if (is_shield) {
        std::string seq1 = blk.cur_comp_res.begin()->second.cur;
        std::string seq2 = blk.cur_comp_res.begin()->second.comp;
        std::string major_allele;
        if (blk.cur_comp_res.begin()->second.cur_count >= blk.cur_comp_res.begin()->second.comp_count) {
            major_allele = all_haps[blk.cur_comp_res.begin()->second.cur_id_list[0]][shield_pos];
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        } else {
            major_allele = all_haps[blk.cur_comp_res.begin()->second.comp_id_list[0]][shield_pos];
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        }
        blk.tag = "secondary_empty_phased";
    }
}

int Phaser::decrease_support_count(std::map<uint, uint> &ref_id_dif_count_map, uint cur_ref_id) {
    if (ref_id_dif_count_map.find(cur_ref_id) == ref_id_dif_count_map.end()) {
        return SUPPORT_COUNT_SCALE;
    }
    uint cur_dif_count = ref_id_dif_count_map[cur_ref_id];
    if (cur_dif_count == 0) {
        return 0;
    }
    if (cur_dif_count == 1) {
        return DECREMENT_1_HOM_DIF;
    }
    if (cur_dif_count == 2) {
        return DECREMENT_2_HOM_DIF;
    }
    return SUPPORT_COUNT_SCALE;
}

void
Phaser::second_phase_iter_v3(Block &blk, std::map<uint, std::string> &local_ref_map, bool is_shield, uint shield_pos,
                             std::vector<std::pair<int, int>> &IBD_pairs, std::vector<uint> ids,
                             std::vector<std::string> &all_haps, std::map<uint, uint> &ref_id_dif_count_map) {
    std::vector<std::string> local_refs = get_local_refs(local_ref_map);
    std::set<std::string> unique_local_refs = unique_haps(local_refs);
    std::map<uint, std::string> local_shielded_ref_map;
    std::vector<std::string> local_shielded_refs;
    std::set<std::string> unique_local_shielded_refs;
    std::vector<std::string> visited;
    std::vector<std::pair<uint, CurComp>> cur_comp_res;
    if (is_shield) {
        local_shielded_ref_map = get_shielded_local_ref_map(shield_pos, blk.start, blk.end, local_refs);
        local_shielded_refs = get_local_refs(local_shielded_ref_map);
        unique_local_shielded_refs = unique_haps(local_shielded_refs);
        uint index_2 = 0;
        for (auto &hap: unique_local_shielded_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = SUPPORT_COUNT_SCALE*std::count(local_shielded_refs.begin(), local_shielded_refs.end(), hap);
                cur_comp.comp_count = SUPPORT_COUNT_SCALE*std::count(local_shielded_refs.begin(), local_shielded_refs.end(), cur_comp.comp);
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.cur_id_list = get_hap_ids(hap, local_shielded_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_shielded_ref_map);
                //todo: adding the decrement of hom dif count, testing different parameters
                for (auto cur_id : cur_comp.cur_id_list) {
                    cur_comp.cur_count -= decrease_support_count(ref_id_dif_count_map, cur_id);
                }
                for (auto comp_id : cur_comp.comp_id_list) {
                    cur_comp.comp_count -= decrease_support_count(ref_id_dif_count_map, comp_id);
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);

                bool ibd_flag = false;
                for(auto &ibd: IBD_pairs){
                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
                        ibd_flag = true;
                    }
                }
                if (ibd_flag){
                    continue;
                }
                //            }
                cur_comp_res.emplace_back(std::make_pair(index_2, cur_comp));
            }
            visited.emplace_back(hap);
            index_2++;
        }
        visited.clear();
    } else {
        uint index = 0;
        for (auto &hap: unique_local_refs) {
            if (std::find(visited.begin(), visited.end(), hap) == visited.end() &&
                std::find(visited.begin(), visited.end(), comp(hap)) == visited.end()) {
                CurComp cur_comp;
                cur_comp.cur = hap;
                cur_comp.comp = comp(hap);
                cur_comp.cur_count = SUPPORT_COUNT_SCALE*std::count(local_refs.begin(), local_refs.end(), hap);
                cur_comp.comp_count = SUPPORT_COUNT_SCALE*std::count(local_refs.begin(), local_refs.end(), cur_comp.comp);
                if (cur_comp.cur_count == 0 || cur_comp.comp_count == 0) {
                    continue;
                }
                cur_comp.cur_id_list = get_hap_ids(hap, local_ref_map);
                cur_comp.comp_id_list = get_hap_ids(cur_comp.comp, local_ref_map);
                //todo: adding the decrement of hom dif count, testing different parameters
                for (auto cur_id : cur_comp.cur_id_list) {
                    cur_comp.cur_count -= decrease_support_count(ref_id_dif_count_map, cur_id);
                }
                for (auto comp_id : cur_comp.comp_id_list) {
                    cur_comp.comp_count -= decrease_support_count(ref_id_dif_count_map, comp_id);
                }
                cur_comp.total_count = cur_comp.cur_count + cur_comp.comp_count;
                cur_comp.min_count = std::min(cur_comp.cur_count, cur_comp.comp_count);
                // check 10 in most haps
//            if (is_shield) {
//                cur_comp.cur_id_list = get_ids(hap, local_shielded_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_shielded_refs);
//            } else {
//                cur_comp.cur_id_list = get_ids(hap, local_refs);
//                cur_comp.comp_id_list = get_ids(cur_comp.comp, local_refs);

                // if cur_ids combine comp_ids in adjust list continue
                // fix later, not good
                bool ibd_flag = false;
                for(auto &ibd: IBD_pairs){
                    if ((std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.first) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.second) != cur_comp.comp_id_list.end()) || \
                    (std::find(cur_comp.cur_id_list.begin(), cur_comp.cur_id_list.end(), ibd.second) != cur_comp.cur_id_list.end() && \
                    std::find(cur_comp.comp_id_list.begin(), cur_comp.comp_id_list.end(), ibd.first) != cur_comp.comp_id_list.end())){
                        ibd_flag = true;
                    }
                }
                if (ibd_flag){
                    continue;
                }
//            }
                cur_comp_res.emplace_back(std::make_pair(index, cur_comp));
            }
            visited.emplace_back(hap);
            index++;
        }
        visited.clear();
    }

    if (cur_comp_res.empty()) {
        blk.tag = "empty";
        blk.cur_comp_res = cur_comp_res;
    } else if (cur_comp_res.size() == 1) {
        blk.tag = "empty_phased";
        blk.cur_comp_res = cur_comp_res;
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    } else if (cur_comp_res.size() > 1) {
        blk.tag = "empty_phased";
        sort_comp_res(cur_comp_res);
        blk.cur_comp_res = cur_comp_res;
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
    }
    if (is_shield) {
        std::string seq1 = blk.cur_comp_res.begin()->second.cur;
        std::string seq2 = blk.cur_comp_res.begin()->second.comp;
        std::string major_allele;
        major_allele = all_haps[blk.cur_comp_res.begin()->second.cur_id_list[0]][shield_pos];
        if (blk.cur_comp_res.begin()->second.cur_count >= blk.cur_comp_res.begin()->second.comp_count) {
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        } else {
            blk.cur_comp_res.begin()->second.cur =
                    blk.cur_comp_res.begin()->second.cur.substr(0, shield_pos) + comp(major_allele) +
                    blk.cur_comp_res.begin()->second.cur.substr(shield_pos + 1, blk.end - blk.start);
            blk.cur_comp_res.begin()->second.comp =
                    blk.cur_comp_res.begin()->second.comp.substr(0, shield_pos) + major_allele +
                    blk.cur_comp_res.begin()->second.comp.substr(shield_pos + 1, blk.end - blk.start);
        }
        blk.pseudo_hap1 = convert_str_2_vec(blk.cur_comp_res.begin()->second.cur);
        blk.pseudo_hap2 = convert_str_2_vec(blk.cur_comp_res.begin()->second.comp);
        blk.tag = "secondary_empty_phased";
    }
}
